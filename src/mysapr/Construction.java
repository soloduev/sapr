package mysapr;

import javafx.collections.ObservableList;

/**
 *
 * @author Igor Soloduev
 */
public class Construction {

    private ObservableList<DataTableConstruction> tableView;
    private ObservableList<DataTableLoadsProp> dtlpView;
    private ObservableList<DataTableLongitudinalForces> dtlfView;
    private ObservableList<DataTableStaticLinearLoads> dtsllView;
    private double deltaVector[];
    private boolean isEdit;

    public Construction() {
        tableView = null;
        dtlpView = null;
        dtlfView = null;
        dtsllView = null;
        deltaVector = null;
        isEdit = false;
    }

    public ObservableList<DataTableConstruction> getTableView() {
        return tableView;
    }

    public void setTableView(ObservableList<DataTableConstruction> tableView) {
        this.tableView = tableView;
    }

    public ObservableList<DataTableLoadsProp> getDtlpView() {
        return dtlpView;
    }

    public void setDtlpView(ObservableList<DataTableLoadsProp> dtlpView) {
        this.dtlpView = dtlpView;
    }

    public ObservableList<DataTableLongitudinalForces> getDtlfView() {
        return dtlfView;
    }

    public void setDtlfView(ObservableList<DataTableLongitudinalForces> dtlfView) {
        this.dtlfView = dtlfView;
    }

    public ObservableList<DataTableStaticLinearLoads> getDtsllView() {
        return dtsllView;
    }

    public void setDtsllView(ObservableList<DataTableStaticLinearLoads> dtsllView) {
        this.dtsllView = dtsllView;
    }

    public double[] getDeltaVector() {
        return deltaVector;
    }

    public void setDeltaVector(double[] deltaVector) {
        this.deltaVector = deltaVector;
    }

    public boolean getIsEdit() {
        return isEdit;
    }

    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }
}
