/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysapr;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Soloduev Igor
 */

//Таблица хранящая номера узлов, в которых находится жесткая опора

public class DataTableLoadsProp {
    
    private IntegerProperty num_node; //Номер узла, в котором находиться опора
    
    public DataTableLoadsProp(int num_node)
    {
        this.num_node  = new SimpleIntegerProperty(num_node);
    }
    
    public IntegerProperty num_nodeProperty(){return num_node;}
    public Integer get_nodeNum(){return num_node.getValue();}
}
