
package mysapr;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Igor
 */
public class DataTableResult {
    
    //Класс представляет из себя стрктуру таблицы, в которую выводим результаты
    //рассчета метода перемещений
    
    private IntegerProperty num;
    private DoubleProperty x;
    private DoubleProperty Nx;
    private DoubleProperty ux;
    private DoubleProperty sigmaX;
    private DoubleProperty sigma;

    public DataTableResult(int num, double x, double Nx, double ux, double sigmaX, double sigma) {
        this.num = new SimpleIntegerProperty(num);
        this.x = new SimpleDoubleProperty(x);
        this.Nx = new SimpleDoubleProperty(Nx);
        this.ux = new SimpleDoubleProperty(ux);
        this.sigmaX = new SimpleDoubleProperty(sigmaX);
        this.sigma = new SimpleDoubleProperty(sigma);
    }
    
   public IntegerProperty numProperty(){return num;}
   public Integer getNum(){return num.getValue();}
   
   public DoubleProperty x_numProperty(){return x;}
   public double getX(){return x.getValue();}
   
   public DoubleProperty Nx_numProperty(){return Nx;}
   public double getNx(){return Nx.getValue();}
   
   public DoubleProperty ux_numProperty(){return ux;}
   public double getUx(){return ux.getValue();}
   
   public DoubleProperty sigmaX_numProperty(){return sigmaX;}
   public double getSigmaX(){return sigmaX.getValue();}
   
   public DoubleProperty sigma_numProperty(){return sigma;}
   public double getSigma(){return sigma.getValue();}
}
