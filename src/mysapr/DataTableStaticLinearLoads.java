/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysapr;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author SoloduevIgor
 */


//Таблица хранящая стержня, и статические погонные нагрузки приложенные к ним

public class DataTableStaticLinearLoads {
    
    private IntegerProperty num_rod; //Номер стержня
    private DoubleProperty sll; //Статическая Погонная Нагрузка

    public DataTableStaticLinearLoads(int num_rod, double sll) {
        this.num_rod = new SimpleIntegerProperty(num_rod);
        this.sll = new SimpleDoubleProperty(sll);
    }
    
    public IntegerProperty num_rodProperty(){return num_rod;}
    public Integer get_num_rodNum(){return num_rod.getValue();}
    
    
    public DoubleProperty sllProperty(){return sll;}
    public Double get_sllNum(){return sll.getValue();}
}
