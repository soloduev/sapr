/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysapr;

import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/**
 *
 * @author Igor Soloduev
 */
public class Processor {
    
    public static void calculate_delta(Construction construction,
                                       Label log_label,
                                       TextArea result_label)
    {
       if(construction!=null)
       { 
           if(construction.getTableView()!= null)
           {
               if(construction.getDtlpView()==null)
               {
                    log_label.setText("Конструкция не закреплена!"
                        + " Вы не можете произвести расчет!");
               }
               else if(construction.getDtlpView().size() == 0)
               {
                   log_label.setText("Конструкция не закреплена!"
                        + " Вы не можете произвести расчет!");
               }
               else if(construction.getTableView().size()>0)
               {
                   //Очищаем поле в котором потом выведем рассчеты
                   result_label.clear();
                   
                   //Сначала составляем матрицу А
                   //Получаем размер матрицы:(Количество узлов)
                   int sizeMatrix = construction.getTableView().size()+1;
                   double A[][] = new double[sizeMatrix][sizeMatrix];
                   
                   //Присваиваем ей значения
                   for (int i = 0; i < sizeMatrix; i++) {
                       for (int j = 0; j < sizeMatrix; j++) {
                           //Присваиваем ячейке 0, если разница между индексами > 1
                           if(Math.abs(i-j) > 1)
                               A[i][j] = 0;
                           else if(Math.abs(i-j) == 0 && i == 0)
                               A[i][j] = (construction.getTableView().get(i).getE()*construction.getTableView().get(i).getA())
                                          /
                                          (construction.getTableView().get(i).getL());
                           else if(Math.abs(i-j) == 0 && i == sizeMatrix-1)
                               A[i][j] = (construction.getTableView().get(sizeMatrix-2).getE()*construction.getTableView().get(sizeMatrix-2).getA())
                                          /
                                          (construction.getTableView().get(sizeMatrix-2).getL());
                           else if(Math.abs(i-j) == 0)
                           
                               A[i][j] = (construction.getTableView().get(i-1).getE()*construction.getTableView().get(i-1).getA())
                                          /
                                          (construction.getTableView().get(i-1).getL())
                                          +
                                          (construction.getTableView().get(i).getE()*construction.getTableView().get(i).getA())
                                          /
                                          (construction.getTableView().get(i).getL());
                           
                           else if(i-j == -1)
                               A[i][j] = - (construction.getTableView().get(j-1).getE()*construction.getTableView().get(j-1).getA())
                                           /
                                           (construction.getTableView().get(j-1).getL());
                            else if(i-j == 1)
                               A[i][j] = - (construction.getTableView().get(i-1).getE()*construction.getTableView().get(i-1).getA())
                                           /
                                           (construction.getTableView().get(i-1).getL());
                       }
                   }
                   
                   //Создаем вектор B
                   double B[] = new double[sizeMatrix];
                   
                   //Сначала заполняем сосредоточенные нагрузки
                   for (int i = 0; i < sizeMatrix; i++) {
                       for (int j = 0; j < construction.getDtsllView().size(); j++) {
                           if(construction.getDtsllView().get(j).get_num_rodNum() == i+1)
                               B[i] = construction.getDtsllView().get(j).get_sllNum();
                       }
                   }
                   
                   //Узнаем в каких узлах заделки
                   int prop_1 = -1;
                   int prop_2 = -1;
                   if(construction.getDtlpView().size()!=0)
                       if(construction.getDtlpView().size()!=1)
                       {
                           prop_1 = construction.getDtlpView().get(0).get_nodeNum();
                           prop_2 = construction.getDtlpView().get(1).get_nodeNum();
                       }
                       else
                           prop_1 = construction.getDtlpView().get(0).get_nodeNum();
                   
                   
                   for (int i = 0; i < construction.getDtlfView().size(); i++) {
                       for (int j = 0; j < construction.getTableView().size(); j++) {
                           if (construction.getTableView().get(j).getNum() == construction.getDtlfView().get(i).get_nodeNum()) {
                               
                               if(j == sizeMatrix-2)
                               {
                                   B[sizeMatrix - 1] += - (construction.getDtlfView().get(i).get_forceNum()*construction.getTableView().get(j).getL())/2;
                                   if(j == 0)
                                        B[0] += - (construction.getDtlfView().get(i).get_forceNum()*construction.getTableView().get(j).getL())/2;
                                   else
                                       B[sizeMatrix - 2] += (construction.getDtlfView().get(i).get_forceNum()*construction.getTableView().get(j).getL())/2;
                               }
                               else
                               {
                                   B[j+1] += (construction.getDtlfView().get(i).get_forceNum()*construction.getTableView().get(j).getL())/2;
                                   if(j == 0)
                                        B[0] += - (construction.getDtlfView().get(i).get_forceNum()*construction.getTableView().get(j).getL())/2;
                                   else
                                        B[j] += (construction.getDtlfView().get(i).get_forceNum()*construction.getTableView().get(j).getL())/2;
                               }
                           }
                       }
                   }
                   
                   //Теперь учтем заделки
                   if(prop_1 == 1 || prop_2 == 1)
                   {
                       A[0][0] = 1;
                       A[0][1] = 0;
                       B[0] = 0;
                   }
                   if(prop_1 == sizeMatrix || prop_2 == sizeMatrix)
                   {
                       A[sizeMatrix-1][sizeMatrix-1] = 1;
                       A[sizeMatrix-1][sizeMatrix-2] = 0;
                       B[sizeMatrix-1] = 0;
                   }
                   StringBuilder sb = new StringBuilder();
                   
                   //выводим матрицу A для проверки:
                   sb.append("[A] = [");
                   for (int i = 0; i < sizeMatrix; i++) {
                       if(i != 0)
                           sb.append("        ");
                       
                       for (int j = 0; j < sizeMatrix; j++) {
                           sb.append(Math.round(A[i][j]* 100.0) / 100.0).append(" ");
                       }
                       
                       if(i != sizeMatrix-1)
                           sb.append('\n');
                       else
                           sb.append("]\n\n");
                   }
                   
                   
                   //выводим вектор B для проверки:
                   sb.append("[B] = [ ");
                   for (int i = 0; i < sizeMatrix; i++) {
                       sb.append(Math.round(B[i]*100.0)/ 100.0).append(" ");
                   }
                   sb.append("]\n\n");
                   
                   //Отображаем систему уравнений
                   sb.append("Система уравнений: \n");
                   for (int i = 0; i < sizeMatrix; i++) {
                       for (int j = 0; j < sizeMatrix; j++) {
                           if(A[i][j]>=0)
                               sb.append("+");
                           
                           sb.append(Math.round(A[i][j]*100.0)/100.0).append("EA/L * Δ").append(j+1).append(" ");
                       }
                           sb.append("= ").append(Math.round(B[i]*100.0)/100.0).append("F\n");                   
                   }
                   
                   sb.append("\n\n");
                   
                   //Я решаю СЛАУ методом Крамера.
                   //Сначала подготавливаю нашу ОБЩУЮ матрицу
                   double mainMatrix[][] = new double[sizeMatrix][sizeMatrix+1];
                   
                   for (int i = 0; i < sizeMatrix; i++) {
                       System.arraycopy(A[i], 0, mainMatrix[i], 0, sizeMatrix);
                   }
                   
                   for (int i = 0; i < sizeMatrix; i++) {
                       mainMatrix[i][sizeMatrix] = B[i];
                   }
                   
                   
                   //Теперь для метода, нужно вычислить определители...
                   //Создаем вектор результатов. Сюда мы запишем ответы(Дельты...)
                   double mainResult[] = new double[sizeMatrix];
                   //Найдем определитель матрицы A
                   double determinant_A = MatrixCalculation.calculateMatrix(A);
                   
                   
                   for (int i = 0; i < sizeMatrix; i++) {
                       //В определителях столбец коэффициентов при соответствующей
                       //неизвестной заменяется столбцом свободных членов системы.
                       //Создаем необходимую матрицу для нахождения определителя
                       double new_matrix[][] = new double[sizeMatrix][sizeMatrix];
                       for (int j = 0; j < sizeMatrix; j++) {
                           System.arraycopy(A[j], 0, new_matrix[j], 0, sizeMatrix);
                       }
                       for (int j = 0; j < sizeMatrix; j++) {
                           new_matrix[j][i] = B[j];
                       }
                       //Теперь, находим определитель этой новой матрицы
                       double new_determinant = MatrixCalculation.calculateMatrix(new_matrix);
                       //Теперь определяем ответ
                       mainResult[i] = new_determinant / determinant_A;
                   }
                   
                   //Выводим на форму результат:
                   
                   sb.append("Полученный глобальный вектор перемещений:\n");
                   for (int i = 0; i < sizeMatrix; i++) {
                       sb.append("Δ").append(i+1).append(" = ").append(mainResult[i]).append(" * FL/EA").append("\n");
                   }
                   
                   sb.append("\n\n\n");
                   result_label.setText(sb.toString());
                   
                   //Ну и наконец записываем в конструкцию получившийся наш дельта вектор
                   construction.setDeltaVector(mainResult);
                   construction.setIsEdit(false);
                   
               }else
                log_label.setText("Конструкция не создана!"
                   + " Вы не можете произвести расчет!");
           }
           else
           log_label.setText("Конструкция не создана!"
                   + " Вы не можете произвести расчет!");
       }
       else
           log_label.setText("Конструкция не создана!"
                   + " Вы не можете произвести расчет!");
    }
    
    
    public static ArrayList<Double> calculateDisplacementMethod(double delta1,double delta2,
                                                   double x,double Lp,double qp,
                                                   double Ep,double Ap)
    {
        double Upx = delta1+(x/Lp)*(delta2-delta1)+((qp*Lp*Lp)/(2*Ep*Ap))*(x/Lp)*(1-(x/Lp));
        double Npx = ((Ep*Ap)/Lp)*(delta2-delta1)+((qp*Lp)/2)*(1-2*(x/Lp));
        //double Npx = -(((Ep*Ap)/Lp)*delta1)+(((Ep*Ap)/Lp)*delta2)+(((qp*Lp)/2)*(1-2*(x/Lp)));              
        double Spx = Npx/Ap;
        
        ArrayList<Double> values = new ArrayList<>();
        values.add(Upx);
        values.add(Npx);
        values.add(Spx);
        
        return values;
    } 
}
