/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysapr;

import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 *
 * @author Igor
 */
public class ValidatorManager {

    public static String validate_add_rod(Label lbl_log,
                                          TableView<DataTableConstruction> tableView,
                                          TextField input_num, TextField input_L,
                                          TextField input_A, TextField input_E,
                                          TextField input_B,
                                          TableView<DataTableLoadsProp> dtlpView,
                                          Label lbl_log2) throws InterruptedException {
        //Получаем все текстовые значения полей
        String str_num = input_num.getText();
        String str_l = input_L.getText();
        String str_a = input_A.getText();
        String str_e = input_E.getText();
        String str_b = input_B.getText();

        //Сначала, проверяем, не пусты ли поля
        if (str_num.isEmpty() || str_l.isEmpty() || str_a.isEmpty()
                || str_e.isEmpty() || str_b.isEmpty()) {
            System.out.println("Ни одно поле не должно быть пустым!");
            lbl_log.setText("Ни одно поле не должно быть пустым!");
            return "Ни одно поле не должно быть пустым!";
        }
        else {
            //Теперь проверям, все и поля численные?
            //Сначала тип INT
            try {
                Integer num = Integer.valueOf(str_num);
            }
            catch (Exception e) {
                System.out.println("Номер стержня должен быть целым числом!");
                lbl_log.setText("Номер стержня должен быть целым числом!");
                return "Номер стержня должен быть целым числом!";
            }

            try {
                //Теперь проверяем остальные поля DOUBLE
                Double l = Double.valueOf(str_l);
                Double a = Double.valueOf(str_a);
                Double e = Double.valueOf(str_e);
                Double b = Double.valueOf(str_b);
            }
            catch (Exception e) {
                System.out.println("Во всех полях, должны быть введены только числа!");
                lbl_log.setText("Во всех полях, должны быть введены только числа!");
                return "Во всех полях, должны быть введены только числа!";
            }

            //Теперь уже безопасно извлекаем все значения
            Integer num = Integer.valueOf(str_num);
            Double l = Double.valueOf(str_l);
            Double a = Double.valueOf(str_a);
            Double e = Double.valueOf(str_e);
            Double b = Double.valueOf(str_b);

            //Теперь проверяем на уникальность номера стержня
            long count = tableView.getItems().stream().filter(i -> i.getNum() == num).count();

            if (count == 0) {
                //Теперь проверяем, на неотрицательность значений
                if (num <= 0 || l <= 0 || a <= 0 || e <= 0 || b <= 0) {
                    System.out.println("Все значения должны быть положительны и не равняться 0");
                    lbl_log.setText("Все значения должны быть положительны и не равняться 0");
                    return "Все значения должны быть положительны и не равняться 0";
                }
                else {
                        //Проверка пройдена, добавляем стержень в таблицу
                        tableView.getItems().add(new DataTableConstruction(num, l, a, e, b));
                        lbl_log.setText("Стержень " + num + " успешно добавлен");
                        if(!dtlpView.getItems().isEmpty())
                        {
                            dtlpView.getItems().clear();
                            lbl_log2.setText("В целях безопасности, таблица заделок очищена, так как добавлен стержень");
                        }
                        return "true";
                }
            }
            else {
                System.out.println("Стержень под таким номером уже существует!");
                lbl_log.setText("Стержень под таким номером уже существует!");
                return "Стержень под таким номером уже существует!";
            }
        }
    }

    public static String validate_add_load(Label lbl_log,
                                           TableView<DataTableLoadsProp> tableView,
                                           TableView<DataTableConstruction> tableView2,
                                           TextField input_num) {

        String str_num = input_num.getText();
        //Сначала, проверяем, не пусто ли поле
        if (str_num.isEmpty()) {
            System.out.println("Поле не должно быть пустым!");
            lbl_log.setText("Поле не должно быть пустым!");
            return "Поле не должно быть пустым!";
        }
        else {
            //Теперь проверям, численное ли поле?
            //Сначала тип INT
            try {
                Integer num = Integer.valueOf(str_num);
            }
            catch (Exception e) {
                System.out.println("Номер узла должен быть целым числом!");
                lbl_log.setText("Номер узла должен быть целым числом!");
                return "Номер узла должен быть целым числом!";
            }

            //Безопасно создаем наше значение
            Integer num = Integer.valueOf(str_num);

            //Теперь проверяем, на отрицательность
            if (num <= 0) {
                System.out.println("Значение должно быть положительным и не равняться 0");
                lbl_log.setText("Значение должно быть положительным и не равняться 0");
                return "Значение должно быть положительным и не равняться 0";
            }
            else {
                //Теперь проверяем, существует ли такой узел?
                if (num > tableView2.getItems().size() + 1 || tableView2.getItems().isEmpty()) {
                    System.out.println("Такого узла не существует");
                    lbl_log.setText("Такого узла не существует");
                    return "Такого узла не существует";
                }
                else {
                    
                    //Теперь проверяем, крайний ли это узел?
                    if(num != 1 && num != tableView2.getItems().size()+1)
                    {
                        //Заделку можно установить, только в сводобных узлах
                        System.out.println("Заделку можно установить, только в сводобных узлах");
                        lbl_log.setText("Заделку можно установить, только в сводобных узлах");
                        return "Заделку можно установить, только в сводобных узлах";
                    }
                    else
                    {
                        //И наконец проверяем, не стоит ли там уже опора?
                        long count = tableView.getItems().stream().filter(i -> i.get_nodeNum() == num).count();
                        if (count != 0) {
                            //В данном узле, уже установлена заделка
                            System.out.println("В данном узле, уже установлена заделка");
                            lbl_log.setText("В данном узле, уже установлена заделка");
                            return "В данном узле, уже установлена заделка";
                        }
                        else {
                            //Проверка пройдена, добавляем опору
                            tableView.getItems().add(new DataTableLoadsProp(num));
                            System.out.println("В узле "+num+" добавлена заделка");
                            lbl_log.setText("В узле "+num+" добавлена заделка");
                            return "true";
                        }
                    }
                }
            }
        }
    }

    public static String validate_add_long_forces(Label lbl_log,
                                           TableView<DataTableLongitudinalForces> tableView,
                                           TableView<DataTableConstruction> tableView2,
                                           TextField input_num,
                                           TextField input_f)
    {
        //Получаем строковые значения объектов
        String str_num = input_num.getText();
        String str_f = input_f.getText();
        
        //Сначала, проверяем, не пусто ли поле
        if (str_num.isEmpty() || str_f.isEmpty()) {
            System.out.println("Ни одно поле не должно быть пустым!");
            lbl_log.setText("Ни одно поле не должно быть пустым!");
            return "Ни одно поле не должно быть пустым!";
        }
        else {
            //Теперь проверям, численное ли поле?
            //Сначала тип INT
            try {
                Integer num = Integer.valueOf(str_num);
            }
            catch (Exception e) {
                System.out.println("Номер стержня должен быть целым числом!");
                lbl_log.setText("Номер стержня должен быть целым числом!");
                return "Номер стержня должен быть целым числом!";
            }
            
            //Теперь проверям, численное ли поле?
            //Сначала тип DOUBLE
            try {
                Double f = Double.valueOf(str_f);
            }
            catch (Exception e) {
                System.out.println("Погонная нагрузка должна быть числом!");
                lbl_log.setText("Погонная нагрузка должна быть числом!");
                return "Погонная нагрузка должна быть числом!";
            }
            
            //Теперь можем безопасно создавать эти переменные
            Integer num = Integer.valueOf(str_num);
            Double f = Double.valueOf(str_f);
            
            //Теперь проверяем, на отрицательность целого значения стержня
            if (num <= 0) {
                System.out.println("Значение номера стержня должно быть положительным и не равняться 0");
                lbl_log.setText("Значение номера стержня должно быть положительным и не равняться 0");
                return "Значение номера стержня должно быть положительным и не равняться 0";
            }
            else 
            {
                //Теперь проверяем, существует ли такой стержень?
                long count = tableView2.getItems().stream().filter(i->i.getNum() == num).count();
                
                if(count==0)
                {
                    System.out.println("Такого стержня не существует!");
                    lbl_log.setText("Такого стержня не существует!");
                    return "Такого стержня не существует!";
                }
                else
                {
                    //И наконец проверяем, не стоит ли там уже сила?
                    long count1 = tableView.getItems().stream().filter(i->i.get_nodeNum() == num).count();
                    if (count1 != 0) {
                        //В данном стержне, уже установлена погонная нагрузка
                        System.out.println("В данном стержне, уже установлена погонная нагрузка");
                        lbl_log.setText("В данном стержне, уже установлена погонная нагрузка");
                        return "В данном стержне, уже установлена погонная нагрузка";
                    }
                    else {
                        
                        if(f == 0.0)
                        {
                            //Погонная нагрузка не может быть нулевой!
                            System.out.println("Погонная нагрузка не может быть нулевой!");
                            lbl_log.setText("Погонная нагрузка не может быть нулевой!");
                            return "Погонная нагрузка не может быть нулевой!";
                        }
                        else
                        {
                            //Проверка пройдена, добавляем силу
                            tableView.getItems().add(new DataTableLongitudinalForces(num, f));
                            System.out.println("В стержне "+num+" добавлена погонная нагрузка "+f);
                            lbl_log.setText("В стержне "+num+" добавлена погонная нагрузка "+f);
                            return "true";
                        }
                    }
                }
            }
        }
    }
    
    public static String validate_add_static_loads(Label lbl_log,
                                           TableView<DataTableStaticLinearLoads> tableView,
                                           TableView<DataTableConstruction> tableView2,
                                           TextField input_num,
                                           TextField input_f)
    {
        //Получаем строковые значения объектов
        String str_num = input_num.getText();
        String str_f = input_f.getText();
        
        //Сначала, проверяем, не пусто ли поле
        if (str_num.isEmpty() || str_f.isEmpty()) {
            System.out.println("Ни одно поле не должно быть пустым!");
            lbl_log.setText("Ни одно поле не должно быть пустым!");
            return "Ни одно поле не должно быть пустым!";
        }
        else {
            //Теперь проверям, численное ли поле?
            //Сначала тип INT
            try {
                Integer num = Integer.valueOf(str_num);
            }
            catch (Exception e) {
                System.out.println("Номер узла должен быть целым числом!");
                lbl_log.setText("Номер узла должен быть целым числом!");
                return "Номер узла должен быть целым числом!";
            }
            
            //Теперь проверям, численное ли поле?
            //Сначала тип DOUBLE
            try {
                Double f = Double.valueOf(str_f);
            }
            catch (Exception e) {
                System.out.println("Продольное усилие должно быть числом!");
                lbl_log.setText("Продольное усилие должно быть числом!");
                return "Продольное усилие должно быть числом!";
            }
            
            //Теперь можем безопасно создавать эти переменные
            Integer num = Integer.valueOf(str_num);
            Double f = Double.valueOf(str_f);
            
            //Теперь проверяем, на отрицательность целого значения узла
            if (num <= 0) {
                System.out.println("Значение номера узла должно быть положительным и не равняться 0");
                lbl_log.setText("Значение номера узла должно быть положительным и не равняться 0");
                return "Значение номера узла должно быть положительным и не равняться 0";
            }
            else 
            {
                //Теперь проверяем, существует ли такой узел?
                if(num > tableView2.getItems().size()+1 || tableView2.getItems().isEmpty())
                {
                    System.out.println("Такого узла не существует!");
                    lbl_log.setText("Такого узла не существует!");
                    return "Такого узла не существует!";
                }
                else
                {
                    //И наконец проверяем, не стоит ли там уже эта сила?
                    long count = tableView.getItems().stream().filter(i->i.get_num_rodNum() == num).count();
                    if (count != 0) {
                        //В данном узле, уже установлена сила
                        System.out.println("В данном узле, уже установлена сила");
                        lbl_log.setText("В данном узле, уже установлена сила");
                        return "В данном узле, уже установлена сила";
                    }
                    else {
                        //Проверка пройдена, добавляем силу
                        tableView.getItems().add(new DataTableStaticLinearLoads(num, f));
                        System.out.println("В узле "+num+" добавлена продольная сила "+f);
                        lbl_log.setText("В узле "+num+" добавлена продольная сила "+f);
                        return "true";
                    }
                }
            }
        }
    }

}
