package mysapr;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;

/**
 *
 * @author Igor Soloduev
 */
public class MySAPR extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        //Наш вспомогательный инструмент для создания интерфейса программы
        SceneManager sceneManager = new SceneManager(primaryStage, "САПР Солодуева Игоря");
        
        //Отображаем главную сцену
        sceneManager.create_main_scene();

    }

    
    public static void main(String[] args) {
        launch(args);
    }
    
}
