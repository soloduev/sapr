/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysapr;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Soloduev Igor
 */

//Таблица хранящая номера узлов, и продольные силы, которые в них приложены

public class DataTableLongitudinalForces {
    
    private IntegerProperty num_node; //Номер узла
    private DoubleProperty force; //Продольная сила в узле

    public DataTableLongitudinalForces(int num_node, double force) {
        this.num_node = new SimpleIntegerProperty(num_node);
        this.force = new SimpleDoubleProperty(force);
    }
    
    public IntegerProperty num_nodeProperty(){return num_node;}
    public Integer get_nodeNum(){return num_node.getValue();}
    
    
    public DoubleProperty forceProperty(){return force;}
    public Double get_forceNum(){return force.getValue();}
    
}
