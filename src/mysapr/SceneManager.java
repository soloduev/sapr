package mysapr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Igor Soloduev
 */
public class SceneManager {

    private Construction construction;
    private final Stage primaryStage; //Главный узел окна
    private final String windowName;  //Название окна
    private Scene scene;              //Текущая сцена

    private String project_name;      //Текущее имя проекта
    private Image im;                 //Лого программы
    private Image add_img;            //Картинка добавления
    private Image remove_img;         //Картинка удаления
    

    public SceneManager(Stage primaryStage, String windowName) {
        //Все что касается отрисовки интерфейса
        this.primaryStage = primaryStage;
        this.windowName = windowName;
        primaryStage.setMinHeight(650);
        primaryStage.setMinWidth(900);
        primaryStage.setMaxHeight(650);
        primaryStage.setMaxWidth(900);
        project_name = "";
        //Загружаем лого программы
        im=new Image(this.getClass().getResource("logo.png").toString());
        add_img=new Image(this.getClass().getResource("add.png").toString());
        remove_img=new Image(this.getClass().getResource("remove.png").toString());
        primaryStage.getIcons().add(im);

        //Все что касается рассчетов
        construction = new Construction();

    }

    public void create_main_scene() {
        AnchorPane root = new AnchorPane();

        
        //Размещаем лого и задаем его расположение
        ImageView logo = new ImageView(im);
        logo.setPreserveRatio(true);
        logo.setFitHeight(100);
        AnchorPane.setTopAnchor(logo, 50.0);
        AnchorPane.setRightAnchor(logo, 10.0);
        
        Button panel = new Button("Главное меню");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Начать работу");
        Button btn2 = new Button("О разработчике");
        Button btn3 = new Button("Выход из программы");
        Button btn4 = new Button("Справка");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(btn2, -5.0);
        AnchorPane.setLeftAnchor(btn3, -5.0);
        AnchorPane.setLeftAnchor(btn4, -5.0);

        AnchorPane.setTopAnchor(panel, 0.0);

        AnchorPane.setBottomAnchor(btn1, 90.0);
        AnchorPane.setBottomAnchor(btn4, 60.0);
        AnchorPane.setBottomAnchor(btn2, 30.0);
        AnchorPane.setBottomAnchor(btn3, 0.0);

        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(btn2, -5.0);
        AnchorPane.setRightAnchor(btn3, -5.0);
        AnchorPane.setRightAnchor(btn4, -5.0);

        //Настройка стиля и дизайна
        DesignManager.set_main_scene(root, btn1, btn2, btn3, btn4, panel);

        root.getChildren().addAll(panel, btn1, btn2, btn3, btn4,logo);

        if (scene == null) {
            scene = new Scene(root, 800, 600);
        }

        else {
            scene.setRoot(root);
        }

        primaryStage.setTitle(windowName);
        primaryStage.setScene(scene);
        primaryStage.show();

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            start_work();
        });

        btn2.setOnAction(ActionEvent -> {
            show_info();
        });

        btn3.setOnAction(ActionEvent -> {
            System.exit(0);
        });

        btn4.setOnAction(ActionEvent -> {
            show_faq();
        });
    }

    public void show_faq() {
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Справка");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Вернутся назад");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);

        AnchorPane.setBottomAnchor(btn1, 0.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);

        DesignManager.set_info(root, btn1, panel);

        root.getChildren().addAll(panel, btn1);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            create_main_scene();
        });
    }

    private void start_work() {
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Начало работы");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Создать проект");
        Button btn2 = new Button("Загрузить проект");
        Button btn3 = new Button("Вернутся назад");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(btn2, -5.0);
        AnchorPane.setLeftAnchor(btn3, -5.0);

        AnchorPane.setBottomAnchor(btn1, 60.0);
        AnchorPane.setBottomAnchor(btn2, 30.0);
        AnchorPane.setBottomAnchor(btn3, 0.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(btn2, -5.0);
        AnchorPane.setRightAnchor(btn3, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        
        //Размещаем лого и задаем его расположение
        ImageView logo = new ImageView(im);
        logo.setPreserveRatio(true);
        logo.setFitHeight(100);
        AnchorPane.setTopAnchor(logo, 50.0);
        AnchorPane.setRightAnchor(logo, 10.0);

        DesignManager.set_start_programm(root, btn1, btn2, btn3, panel);

        root.getChildren().addAll(panel, btn1, btn2, btn3, logo);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            create_project();
        });

        btn2.setOnAction(ActionEvent -> {
            load_project();
        });

        btn3.setOnAction(ActionEvent -> {
            create_main_scene();
        });
    }

    private void show_info() {
        AnchorPane root = new AnchorPane();

        String text = "Разработал студент 3 курса МГТУ \"Станкин\" - Солодуев И. А.\n"
                + "Факультет:\t \"Информационные технологии и ситемы управления\"\n"
                + "Направление:\t \"Прикладная информатика\"\n"
                + "Группа:\t \"ИДБ-13-15\"\n"
                + "Предмет:\t \"Компьютерная механика\"\n"
                + "Преподаватель:\t \"Чеканин В.А.\"\n\n"
                + "Работа позиционируется как \"Курсовой проект\"\n"
                + "Написано на языке Java\n"
                + "В качестве библиотеки графического интерфейса, использовалась библиотека JavaFX\n"
                + "Разрабатывался и собирался проект в NetBeans IDE\n\n"
                + "Исходный код проекта хранится в открытом доступе в git-репозитории:\n"
                + "\n"
                + "Почта и страница vk разработчика:\n\n\n\n 1 декабря 2015 год";
        
        Label info = new Label(text);
        info.setWrapText(true);
        info.setAlignment(Pos.CENTER_LEFT);
        info.setFont(new Font("Helvetica",14.0));
        
        Hyperlink hyperlink_git = new Hyperlink("https://github.com/LazyTroll/SAPR");
        hyperlink_git.setFont(new Font("Helvetica",14.0));
        
        Hyperlink hyperlink_mail = new Hyperlink("diahorver@gmail.com");
        hyperlink_mail.setFont(new Font("Helvetica",14.0));
        
        Hyperlink hyperlink_vk = new Hyperlink("https://vk.com/igorek_student");
        hyperlink_vk.setFont(new Font("Helvetica",14.0));
        
        Button panel = new Button("О разработчике");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Вернутся назад");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(info, 50.0);
        AnchorPane.setLeftAnchor(hyperlink_git, 46.0);
        AnchorPane.setLeftAnchor(hyperlink_mail, 46.0);
        AnchorPane.setLeftAnchor(hyperlink_vk, 46.0);

        AnchorPane.setBottomAnchor(btn1, 0.0);
        AnchorPane.setBottomAnchor(hyperlink_git, 248.0);
        AnchorPane.setBottomAnchor(hyperlink_mail, 210.0);
        AnchorPane.setBottomAnchor(hyperlink_vk, 190.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(info, 50.0);
        
        AnchorPane.setTopAnchor(info, 80.0);
        
        //Размещаем лого и задаем его расположение
        ImageView logo = new ImageView(im);
        logo.setPreserveRatio(true);
        logo.setFitHeight(100);
        AnchorPane.setTopAnchor(logo, 50.0);
        AnchorPane.setRightAnchor(logo, 10.0);

        DesignManager.set_info(root, btn1, panel);

        root.getChildren().addAll(panel, btn1, logo,info,hyperlink_git,
                                  hyperlink_mail,hyperlink_vk);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            create_main_scene();
        });
    }

    private void create_project() {
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Создание проекта");
        panel.setMinHeight(40.0);

        Button text_enter_name = new Button("Введите имя проекта");

        TextField name_task = new TextField();
        name_task.setMinWidth(100.0);

        Button btn_create_task = new Button("Создать проект");
        Button btn_cancel = new Button("Отмена");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn_cancel, -5.0);
        AnchorPane.setLeftAnchor(btn_create_task, -5.0);
        AnchorPane.setLeftAnchor(name_task, 250.0);
        AnchorPane.setLeftAnchor(text_enter_name, -5.0);

        AnchorPane.setBottomAnchor(btn_cancel, 0.0);
        AnchorPane.setBottomAnchor(btn_create_task, 30.0);
        AnchorPane.setBottomAnchor(name_task, 150.0);
        AnchorPane.setBottomAnchor(text_enter_name, 180.0);

        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(btn_cancel, -5.0);
        AnchorPane.setRightAnchor(btn_create_task, -5.0);
        AnchorPane.setRightAnchor(name_task, 250.0);
        AnchorPane.setRightAnchor(text_enter_name, -5.0);

        AnchorPane.setTopAnchor(panel, 0.0);
        
        //Размещаем лого и задаем его расположение
        ImageView logo = new ImageView(im);
        logo.setPreserveRatio(true);
        logo.setFitHeight(100);
        AnchorPane.setTopAnchor(logo, 50.0);
        AnchorPane.setRightAnchor(logo, 10.0);

        //Настройка стиля и дизайна
        DesignManager.set_create_project(root, btn_create_task, btn_cancel, text_enter_name,
                                         name_task, panel);

        root.getChildren().addAll(panel, text_enter_name, name_task,
                                  btn_create_task, btn_cancel, logo);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn_cancel.setOnAction(ActionEvent -> {
            start_work();
        });

        btn_create_task.setOnAction(ActionEvent -> {
            if (name_task.getText().isEmpty()) {
                System.out.println("Название проекта не должно быть пустым!");
                text_enter_name.setText("Введите название проекта (Название не должно быть пустым)");
            }
            else if (name_task.getText().contains(" ")) {
                System.out.println("Название проекта не должно содержать пробелы!");
                text_enter_name.setText("Введите название проекта (Название не должно содержать пробелы)");
            }
            else {
                project_name = name_task.getText();
                project_panel();
            }
        });
    }

    private void load_project() {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("."));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("kpr", "*.kpr"));
        try {
            File openFile = fc.showOpenDialog(primaryStage);
            String absolutePath = openFile.getAbsolutePath();
            String name = openFile.getName();
            String new_name = name.substring(0, name.length() - 4);
            String type_file = name.substring(name.length() - 4);
            System.out.println(absolutePath + "  ,  " + new_name + "   ,   " + type_file);
            if (new_name.length() > 0 && type_file.equals(".kpr")) {
                try {
                    
                    ObjectInputStream inputStream
                            = new ObjectInputStream(new FileInputStream(absolutePath));
                    DataSerialize dataSerialize = (DataSerialize) inputStream.readObject();
                    
                    if(construction.getTableView()==null)
                    {
                        ObservableList<DataTableConstruction> dtcs = FXCollections.observableArrayList();
                        ObservableList<DataTableLoadsProp> dtlp = FXCollections.observableArrayList();
                        ObservableList<DataTableLongitudinalForces> dtlf = FXCollections.observableArrayList();
                        ObservableList<DataTableStaticLinearLoads> dtsll = FXCollections.observableArrayList();
                        construction.setTableView(dtcs);
                        construction.setDtlpView(dtlp);
                        construction.setDtlfView(dtlf);
                        construction.setDtsllView(dtsll);
                        construction.setIsEdit(true);
                    }
                    else
                    {
                        construction.getTableView().clear();
                        construction.getDtlfView().clear();
                        construction.getDtlpView().clear();
                        construction.getDtsllView().clear();
                        construction.setIsEdit(true);
                    }
                   
                    dataSerialize.tableView.stream().forEach((al) -> { 
                        construction.getTableView()
                                .add(new DataTableConstruction(al.get(0).intValue(), al.get(1), al.get(2), al.get(3), al.get(4)));
                        System.out.println("true");
                        
                    });
                    dataSerialize.tableLoads.stream().forEach((al) -> {
                        construction.getDtlpView()
                                .add(new DataTableLoadsProp(al.get(0)));
                    });
                    dataSerialize.tableLongForces.stream().forEach((al) -> {
                        construction.getDtlfView()
                                .add(new DataTableLongitudinalForces(al.get(0).intValue(), al.get(1)));
                    });
                    dataSerialize.tableForces.stream().forEach((al) -> {
                        construction.getDtsllView()
                                .add(new DataTableStaticLinearLoads(al.get(0).intValue(), al.get(1)));
                    });
                    project_name = new_name;
                    project_panel();
                }
                catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(SceneManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else {
                System.out.println("К сожалению, вы выбрали не файл проекта");
            }
        }
        catch (Exception e) {
            System.out.println("Вы не выбрали файл проекта!");
        }
    }

    private void project_panel() {
        AnchorPane root = new AnchorPane();

        //Размещаем лого и задаем его расположение
        ImageView logo = new ImageView(im);
        logo.setPreserveRatio(true);
        logo.setFitHeight(100);
        AnchorPane.setTopAnchor(logo, 90.0);
        AnchorPane.setRightAnchor(logo, 10.0);
        
        Label info = new Label("");
        info.setAlignment(Pos.CENTER);
        info.setFont(new Font("Helvetica",14.0));
        
        Button panel = new Button("Панель управления проектом");
        panel.setMinHeight(40.0);
        Button btn_project_name = new Button("Имя проекта: " + project_name);

        Button btn_save_project = new Button("Сохранить проект");


        Button preprocessor = new Button("Препроцессор");

        Button btn_enter_data = new Button("Редактировать данные");
        Button btn_show_constuction = new Button("Показать конструкцию и нагрузки");

        Button processor = new Button("Процессор");

        Button btn_calculate = new Button("Рассчитать дельта-вектор");

        Button postprocessor = new Button("Постпроцессор");

        Button btn_show_result_in_table = new Button("Показать результаты расчета в табличном виде");
        Button btn_show_result_in_epure = new Button("Показать результаты расчета в виде эпюр");
        
        Button btn_exit = new Button("Вернуться в главное меню");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn_project_name, -5.0);
        AnchorPane.setLeftAnchor(preprocessor, -5.0);
        AnchorPane.setLeftAnchor(btn_enter_data, -5.0);
        AnchorPane.setLeftAnchor(btn_show_constuction, -5.0);
        AnchorPane.setLeftAnchor(processor, -5.0);
        AnchorPane.setLeftAnchor(postprocessor, -5.0);
        AnchorPane.setLeftAnchor(btn_exit, -5.0);
        AnchorPane.setLeftAnchor(btn_calculate, -5.0);
        AnchorPane.setLeftAnchor(btn_show_result_in_table, -5.0);
        AnchorPane.setLeftAnchor(btn_show_result_in_epure, -5.0);
        AnchorPane.setLeftAnchor(info, -5.0);

        AnchorPane.setBottomAnchor(btn_exit, 0.0);

        AnchorPane.setBottomAnchor(postprocessor, 180.0);
        AnchorPane.setBottomAnchor(btn_show_result_in_table, 150.0);
        AnchorPane.setBottomAnchor(btn_show_result_in_epure, 120.0);
        
        AnchorPane.setBottomAnchor(processor, 270.0);
        AnchorPane.setBottomAnchor(btn_calculate, 240.0);

        AnchorPane.setBottomAnchor(preprocessor, 390.0);
        AnchorPane.setBottomAnchor(btn_enter_data, 360.0);
        AnchorPane.setBottomAnchor(btn_show_constuction, 330.0);

        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(btn_project_name, -5.0);
        AnchorPane.setRightAnchor(preprocessor, -5.0);
        AnchorPane.setRightAnchor(btn_enter_data, -5.0);
        AnchorPane.setRightAnchor(btn_show_constuction, -5.0);
        AnchorPane.setRightAnchor(processor, -5.0);
        AnchorPane.setRightAnchor(postprocessor, -5.0);
        AnchorPane.setRightAnchor(btn_exit, -5.0);
        AnchorPane.setRightAnchor(btn_save_project, 5.0);
        AnchorPane.setRightAnchor(btn_calculate, -5.0);
        AnchorPane.setRightAnchor(btn_show_result_in_table, -5.0);
        AnchorPane.setRightAnchor(btn_show_result_in_epure, -5.0);
        AnchorPane.setRightAnchor(info, -5.0);

        AnchorPane.setTopAnchor(panel, 0.0);
        AnchorPane.setTopAnchor(btn_project_name, 42.0);
        AnchorPane.setTopAnchor(btn_save_project, 48.0);
        AnchorPane.setTopAnchor(info, 95.0);

        DesignManager.set_panel_project(root, panel, btn_project_name,
                                        preprocessor, btn_enter_data,
                                        btn_show_constuction, processor, postprocessor, btn_exit,
                                        btn_calculate,
                                        btn_show_result_in_table,
                                        btn_show_result_in_epure);

        root.getChildren().addAll(panel, btn_project_name,
                                  btn_save_project, preprocessor, btn_enter_data,
                                  btn_show_constuction, processor, postprocessor, btn_exit,
                                  btn_calculate,
                                  btn_show_result_in_table,
                                  btn_show_result_in_epure,
                                  info,logo);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn_exit.setOnAction(ActionEvent -> {
            show_acces_exit();
        });

        btn_enter_data.setOnAction(ActionEvent -> {
            create_constrution();
        });
       
        btn_show_constuction.setOnAction(ActionEvent -> {
            draw_construction();
        });
        
        btn_calculate.setOnAction(ActionEvent -> {
            calculate_construction();
        });
        
        btn_show_result_in_table.setOnAction(ActionEvent -> {
            if (construction.getIsEdit() == true) 
            {
                show_to_delta_scene();
            }
            else
            {
                show_result_table();
            }
        });
        
        btn_save_project.setOnAction(ActionEvent -> {
            save_file(info);
        });
        
        btn_show_result_in_epure.setOnAction(ActionEvent -> {
            if (construction.getIsEdit() == true) 
            {
                show_to_delta_scene();
            }
            else
            {
                draw_diagrams();
            }
        });
    }

    private void create_constrution() {
        //Данный метод отображает сцену ввода данных о конструкции
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Ввод данных о конструкции и нагрузках");
        panel.setMinHeight(40.0);

        //Расположение заголовка сцены
        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setTopAnchor(panel, 0.0);

        //Создаем пока пустую таблицу конструкций, которую впоследствии заполнит пользователь
        //---------------------------------------------------------------------
        ObservableList<DataTableConstruction> dtcs = FXCollections.observableArrayList();

        TableView<DataTableConstruction> tableView = new TableView<>();

        //Расположение таблицы конструкции
        AnchorPane.setLeftAnchor(tableView, 50.0);
        AnchorPane.setTopAnchor(tableView, 75.0);
        AnchorPane.setRightAnchor(tableView, 50.0);

        tableView.setMaxHeight(150.0);

        tableView.setTableMenuButtonVisible(true);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Далее задаем колонки
        TableColumn numCol = new TableColumn("Номер");
        TableColumn lCol = new TableColumn("L");
        TableColumn aCol = new TableColumn("A");
        TableColumn eCol = new TableColumn("E");
        TableColumn bCol = new TableColumn("[σ]");

        numCol.setCellValueFactory(new PropertyValueFactory("num"));
        lCol.setCellValueFactory(new PropertyValueFactory("l_num"));
        aCol.setCellValueFactory(new PropertyValueFactory("a_num"));
        eCol.setCellValueFactory(new PropertyValueFactory("e_num"));
        bCol.setCellValueFactory(new PropertyValueFactory("b_num"));

        numCol.setPrefWidth(50.0);
        lCol.setPrefWidth(120.0);
        aCol.setPrefWidth(120.0);
        eCol.setPrefWidth(120.0);
        bCol.setPrefWidth(120.0);

        numCol.setResizable(true);
        lCol.setResizable(true);
        aCol.setResizable(true);
        eCol.setResizable(true);
        bCol.setResizable(true);

        numCol.setEditable(true);

        //Делаем нашу таблицу изменяемой
        tableView.setEditable(true);

        tableView.getColumns().addAll(numCol, lCol, aCol, eCol, bCol);
        tableView.setItems(dtcs);
        //------------------------------------------------------------------ 

        //Создаем пока пустую таблицу опор, которую впоследствии заполнит пользователь
        //------------------------------------------------------------------ 
        ObservableList<DataTableLoadsProp> dtlp = FXCollections.observableArrayList();

        TableView<DataTableLoadsProp> dtlpView = new TableView<>();

        //Расположение таблицы
        AnchorPane.setLeftAnchor(dtlpView, 50.0);
        AnchorPane.setTopAnchor(dtlpView, 350.0);
        AnchorPane.setRightAnchor(dtlpView, 610.0);

        dtlpView.setMaxHeight(100.0);

        dtlpView.setTableMenuButtonVisible(true);
        dtlpView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Далее задаем колонки
        TableColumn num_nodeCol = new TableColumn("Узел c опорой");

        num_nodeCol.setCellValueFactory(new PropertyValueFactory("num_node"));

        num_nodeCol.setPrefWidth(110.0);

        num_nodeCol.setResizable(true);

        //Делаем нашу таблицу изменяемой
        dtlpView.setEditable(true);

        dtlpView.getColumns().addAll(num_nodeCol);
        dtlpView.setItems(dtlp);
        //------------------------------------------------------------------

        //Создаем пока пустую таблицу продольных сил
        //------------------------------------------------------------------ 
        ObservableList<DataTableLongitudinalForces> dtlf = FXCollections.observableArrayList();

        TableView<DataTableLongitudinalForces> dtlfView = new TableView<>();

        //Расположение таблицы 
        AnchorPane.setTopAnchor(dtlfView, 350.0);
        AnchorPane.setRightAnchor(dtlfView, 330.0);

        dtlfView.setMaxHeight(100.0);
        dtlfView.setMaxWidth(250.0);

        dtlfView.setTableMenuButtonVisible(true);
        dtlfView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Далее задаем колонки
        TableColumn num_node2Col = new TableColumn("Стержень");
        TableColumn forceCol = new TableColumn("q");

        num_node2Col.setCellValueFactory(new PropertyValueFactory("num_node"));
        forceCol.setCellValueFactory(new PropertyValueFactory("force"));

        num_node2Col.setPrefWidth(70.0);
        forceCol.setPrefWidth(100.0);

        num_node2Col.setResizable(true);
        forceCol.setResizable(true);

        //Делаем нашу таблицу изменяемой
        dtlfView.setEditable(true);

        dtlfView.getColumns().addAll(num_node2Col, forceCol);
        dtlfView.setItems(dtlf);
        //------------------------------------------------------------------

        //Создаем пока пустую таблицу погонных нагрузок
        //------------------------------------------------------------------ 
        ObservableList<DataTableStaticLinearLoads> dtsll = FXCollections.observableArrayList();

        TableView<DataTableStaticLinearLoads> dtsllView = new TableView<>();

        //Расположение таблицы
        AnchorPane.setTopAnchor(dtsllView, 350.0);
        AnchorPane.setRightAnchor(dtsllView, 50.0);

        dtsllView.setMaxHeight(100.0);
        dtsllView.setMaxWidth(250.0);

        dtsllView.setTableMenuButtonVisible(true);
        dtsllView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Далее задаем колонки
        TableColumn num_rodCol = new TableColumn("Узел");
        TableColumn sllCol = new TableColumn("F");

        num_rodCol.setCellValueFactory(new PropertyValueFactory("num_rod"));
        sllCol.setCellValueFactory(new PropertyValueFactory("sll"));

        num_rodCol.setPrefWidth(70.0);
        sllCol.setPrefWidth(100.0);

        num_rodCol.setResizable(true);
        sllCol.setResizable(true);

        //Делаем нашу таблицу изменяемой
        dtsllView.setEditable(true);

        dtsllView.getColumns().addAll(num_rodCol, sllCol);
        dtsllView.setItems(dtsll);
        //------------------------------------------------------------------

        //Указываем панели
        Button constuction = new Button("Данные конструкции");
        AnchorPane.setRightAnchor(constuction, -5.0);
        AnchorPane.setTopAnchor(constuction, 40.0);
        AnchorPane.setLeftAnchor(constuction, -5.0);

        Button loads = new Button("Данные нагрузок");
        AnchorPane.setRightAnchor(loads, -5.0);
        AnchorPane.setTopAnchor(loads, 300.0);
        AnchorPane.setLeftAnchor(loads, -5.0);

        //Устанавливаем кнопки управления
        //Картинка добавления
        ImageView img_add_1 = new ImageView(add_img);
        img_add_1.setPreserveRatio(true);
        img_add_1.setFitHeight(20);
        ImageView img_remove_1 = new ImageView(remove_img);
        img_remove_1.setPreserveRatio(true);
        img_remove_1.setFitHeight(20);
        ImageView img_add_2 = new ImageView(add_img);
        img_add_2.setPreserveRatio(true);
        img_add_2.setFitHeight(20);
        ImageView img_remove_2 = new ImageView(remove_img);
        img_remove_2.setPreserveRatio(true);
        img_remove_2.setFitHeight(20);
        ImageView img_add_3 = new ImageView(add_img);
        img_add_3.setPreserveRatio(true);
        img_add_3.setFitHeight(20);
        ImageView img_remove_3 = new ImageView(remove_img);
        img_remove_3.setPreserveRatio(true);
        img_remove_3.setFitHeight(20);
        ImageView img_add_4 = new ImageView(add_img);
        img_add_4.setPreserveRatio(true);
        img_add_4.setFitHeight(20);
        ImageView img_remove_4 = new ImageView(remove_img);
        img_remove_4.setPreserveRatio(true);
        img_remove_4.setFitHeight(20);
        
        
        Button btn_add_rod = new Button();
        btn_add_rod.setGraphic(img_add_1);
        AnchorPane.setRightAnchor(btn_add_rod, 50.0);
        AnchorPane.setTopAnchor(btn_add_rod, 230.0);
        Button btn_delete_rod = new Button();
        btn_delete_rod.setGraphic(img_remove_1);
        AnchorPane.setRightAnchor(btn_delete_rod, 92.0);
        AnchorPane.setTopAnchor(btn_delete_rod, 230.0);
        btn_delete_rod.setPrefWidth(25);
        Button btn_up_rod = new Button("Вверх");
        AnchorPane.setRightAnchor(btn_up_rod, 50.0);
        AnchorPane.setTopAnchor(btn_up_rod, 265.0);
        btn_up_rod.setPrefWidth(82);
        Button btn_down_rod = new Button("Вниз");
        AnchorPane.setRightAnchor(btn_down_rod, 134.0);
        AnchorPane.setTopAnchor(btn_down_rod, 265.0);
        btn_down_rod.setPrefWidth(82);
        Label lbl_log = new Label("");
        AnchorPane.setLeftAnchor(lbl_log, 50.0);
        AnchorPane.setTopAnchor(lbl_log, 270.0);
        lbl_log.setFont(new Font("Helvetica", 15));
        Label lbl_log2 = new Label("");
        AnchorPane.setLeftAnchor(lbl_log2, 50.0);
        AnchorPane.setTopAnchor(lbl_log2, 490.0);
        lbl_log2.setFont(new Font("Helvetica", 15));

        Button btn_add_prop = new Button(); //Добавить
        btn_add_prop.setGraphic(img_add_2);
        AnchorPane.setRightAnchor(btn_add_prop, 610.0);
        AnchorPane.setTopAnchor(btn_add_prop, 455.0);
        btn_add_prop.setPrefWidth(30);

        Button btn_remove_prop = new Button(); //Убрать
        AnchorPane.setRightAnchor(btn_remove_prop, 652.0);
        AnchorPane.setTopAnchor(btn_remove_prop, 455.0);
        btn_remove_prop.setPrefWidth(30);
        btn_remove_prop.setGraphic(img_remove_2);

        Button btn_add_node1 = new Button(); //Добавить
        btn_add_node1.setGraphic(img_add_3);
        AnchorPane.setRightAnchor(btn_add_node1, 330.0);
        AnchorPane.setTopAnchor(btn_add_node1, 455.0);
        btn_add_node1.setPrefWidth(30);

        Button btn_remove_node1 = new Button(); //Убрать
        AnchorPane.setRightAnchor(btn_remove_node1, 372.0);
        AnchorPane.setTopAnchor(btn_remove_node1, 455.0);
        btn_remove_node1.setPrefWidth(30);
        btn_remove_node1.setGraphic(img_remove_3);

        Button btn_add_node2 = new Button(); //Добавить
        btn_add_node2.setGraphic(img_add_4);
        AnchorPane.setRightAnchor(btn_add_node2, 50.0);
        AnchorPane.setTopAnchor(btn_add_node2, 455.0);
        btn_add_node2.setPrefWidth(30);

        Button btn_remove_node2 = new Button(); //Убрать
        AnchorPane.setRightAnchor(btn_remove_node2, 92.0);
        AnchorPane.setTopAnchor(btn_remove_node2, 455.0);
        btn_remove_node2.setPrefWidth(30);
        btn_remove_node2.setGraphic(img_remove_4);

        Button btn_show_rod = new Button("Показать конструкцию");
        AnchorPane.setBottomAnchor(btn_show_rod, 60.0);
        AnchorPane.setLeftAnchor(btn_show_rod, -5.0);
        AnchorPane.setRightAnchor(btn_show_rod, -5.0);

        Button btn_clear_all = new Button("Очистить все данные");
        AnchorPane.setBottomAnchor(btn_clear_all, 30.0);
        AnchorPane.setLeftAnchor(btn_clear_all, -5.0);
        AnchorPane.setRightAnchor(btn_clear_all, -5.0);

        Button btn_return = new Button("Вернуться на панель управления");
        AnchorPane.setBottomAnchor(btn_return, 0.0);
        AnchorPane.setLeftAnchor(btn_return, -5.0);
        AnchorPane.setRightAnchor(btn_return, -5.0);

        //Устанавливаем поля ввода
        TextField input_num = new TextField();
        AnchorPane.setTopAnchor(input_num, 230.0);
        AnchorPane.setLeftAnchor(input_num, 50.0);
        input_num.setPrefWidth(47);
        TextField input_L = new TextField();
        AnchorPane.setTopAnchor(input_L, 230.0);
        AnchorPane.setLeftAnchor(input_L, 100.0);
        input_L.setPrefWidth(117);
        TextField input_A = new TextField();
        AnchorPane.setTopAnchor(input_A, 230.0);
        AnchorPane.setLeftAnchor(input_A, 220.0);
        input_A.setPrefWidth(117);
        TextField input_E = new TextField();
        AnchorPane.setTopAnchor(input_E, 230.0);
        AnchorPane.setLeftAnchor(input_E, 340.0);
        input_E.setPrefWidth(117);
        TextField input_B = new TextField();
        AnchorPane.setTopAnchor(input_B, 230.0);
        AnchorPane.setLeftAnchor(input_B, 460.0);
        input_B.setPrefWidth(117);

        TextField input_prop = new TextField();
        AnchorPane.setTopAnchor(input_prop, 455.0);
        AnchorPane.setLeftAnchor(input_prop, 50.0);
        input_prop.setPrefWidth(140);

        TextField input_q_i = new TextField();
        AnchorPane.setTopAnchor(input_q_i, 455.0);
        AnchorPane.setRightAnchor(input_q_i, 415.0);
        input_q_i.setPrefWidth(95);
        TextField input_node1 = new TextField();
        AnchorPane.setTopAnchor(input_node1, 455.0);
        AnchorPane.setRightAnchor(input_node1, 513.0);
        input_node1.setPrefWidth(65);

        TextField input_f_i = new TextField();
        AnchorPane.setTopAnchor(input_f_i, 455.0);
        AnchorPane.setRightAnchor(input_f_i, 134.0);
        input_f_i.setPrefWidth(92);
        TextField input_node2 = new TextField();
        AnchorPane.setTopAnchor(input_node2, 455.0);
        AnchorPane.setRightAnchor(input_node2, 229.0);
        input_node2.setPrefWidth(69);

        //Применяем дизайн к сцене
        DesignManager.set_create_construction(root, panel, tableView,
                                              btn_show_rod, btn_return, constuction, loads, btn_add_rod,
                                              btn_delete_rod, btn_up_rod, btn_down_rod, input_num, input_L,
                                              input_A, input_E, input_B, dtlpView, dtlfView, dtsllView,
                                              btn_clear_all, input_prop, input_q_i,
                                              input_node1, input_f_i, input_node2, btn_add_prop, btn_remove_prop,
                                              btn_add_node1, btn_remove_node1, btn_add_node2, btn_remove_node2
        );

        root.getChildren().addAll(panel, tableView, btn_show_rod, btn_return,
                                  btn_add_rod, btn_delete_rod, btn_up_rod, btn_down_rod, constuction,
                                  loads, lbl_log, lbl_log2, input_num, input_L, input_A, input_E, input_B, dtlpView,
                                  dtlfView, dtsllView, btn_add_prop, btn_remove_prop, input_prop,
                                  btn_add_node1, btn_remove_node1, btn_add_node2, btn_remove_node2,
                                  input_q_i, input_node1, input_f_i, input_node2,
                                  btn_clear_all);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //Далее подгружаем существующую конструкцию
        load_construction(tableView, dtlpView, dtlfView, dtsllView);

        //События для кнопок
        btn_return.setOnAction(ActionEvent -> {
            save_all(tableView, dtlpView, dtlfView, dtsllView);
            project_panel();
        });

        btn_add_rod.setOnAction(ActionEvent -> {
            try {
                //Провераяем на правильность введенных данных
                ValidatorManager.validate_add_rod(lbl_log, tableView, input_num,
                                 input_L, input_A, input_E, input_B,dtlpView,lbl_log2);
                //Сохраняем все данные
                save_all(tableView, dtlpView, dtlfView, dtsllView);
            }
            catch (Exception e) {

            }
        });

        btn_up_rod.setOnAction(ActionEvent -> {
            if (!tableView.getItems().isEmpty()) {
                if (!tableView.getSelectionModel().isEmpty()) {
                    //Получаем выбранную строку
                    DataTableConstruction selectedItem = tableView.getSelectionModel().getSelectedItem();
                    //Узнаем индекс строки
                    int selected_index = tableView.getItems().indexOf(selectedItem);
                    if (selected_index == 0 && tableView.getItems().size() != 1) {
                        tableView.getItems().remove(selected_index);
                        tableView.getItems().add(tableView.getItems().size(), selectedItem);
                        tableView.getSelectionModel().clearSelection(selected_index);
                        tableView.getSelectionModel().select(tableView.getItems().size() - 1);
                        //Сохраняем все данные
                        save_all(tableView, dtlpView, dtlfView, dtsllView);
                    }
                    else if (tableView.getItems().size() != 1) {
                        tableView.getItems().remove(selected_index);
                        tableView.getItems().add(selected_index - 1, selectedItem);
                        tableView.getSelectionModel().select(selected_index - 1);
                        tableView.getSelectionModel().clearSelection(selected_index);
                        //Сохраняем все данные
                        save_all(tableView, dtlpView, dtlfView, dtsllView);
                    }
                }
            }
        });

        btn_down_rod.setOnAction(ActionEvent -> {
            if (!tableView.getItems().isEmpty()) {
                if (!tableView.getSelectionModel().isEmpty()) {
                    //Получаем выбранную строку
                    DataTableConstruction selectedItem = tableView.getSelectionModel().getSelectedItem();
                    //Узнаем индекс строки
                    int selected_index = tableView.getItems().indexOf(selectedItem);
                    if (selected_index == tableView.getItems().size() - 1 && tableView.getItems().size() != 1) {
                        tableView.getItems().remove(selected_index);
                        tableView.getItems().add(0, selectedItem);
                        tableView.getSelectionModel().clearSelection(selected_index);
                        tableView.getSelectionModel().select(0);
                        //Сохраняем все данные
                        save_all(tableView, dtlpView, dtlfView, dtsllView);
                    }
                    else if (tableView.getItems().size() != 1) {
                        tableView.getSelectionModel().clearSelection(selected_index);
                        tableView.getItems().remove(selected_index);
                        tableView.getItems().add(selected_index + 1, selectedItem);
                        tableView.getSelectionModel().select(selected_index + 1);
                        //Сохраняем все данные
                        save_all(tableView, dtlpView, dtlfView, dtsllView);
                    }
                }
            }
        });

        btn_delete_rod.setOnAction(ActionEvent -> {
            if (!tableView.getItems().isEmpty()) {
                if (!tableView.getSelectionModel().isEmpty()) {
                    //Получаем выбранную строку
                    DataTableConstruction selectedItem = tableView.getSelectionModel().getSelectedItem();
                    //Узнаем индекс строки
                    int selected_index = tableView.getItems().indexOf(selectedItem);
                    //Удаляем выбранный стержень в таблице
                    tableView.getItems().remove(selected_index);
                    lbl_log.setText("Стержень " + (selected_index + 1) + " убран");
                    //И ради безопасности очищаем все таблицы нагрузок
                    dtlfView.getItems().clear();
                    dtlpView.getItems().clear();
                    dtsllView.getItems().clear();
                    //Сохраняем все данные
                    save_all(tableView, dtlpView, dtlfView, dtsllView);
                    lbl_log2.setText("В целях безопасности, все данные о нагрузках очищены, так как изменена конструкция");
                }
            }
        });

        btn_add_prop.setOnAction(ActionEvent -> {
            ValidatorManager.validate_add_load(lbl_log2, dtlpView, tableView, input_prop);
            //Сохраняем все данные
            save_all(tableView, dtlpView, dtlfView, dtsllView);
            //Очищаем текстовое поле
            input_prop.clear();
        });

        btn_remove_prop.setOnAction(ActionEvent -> {
            if (!dtlpView.getItems().isEmpty()) {
                if (!dtlpView.getSelectionModel().isEmpty()) {
                    //Получаем выбранную строку
                    DataTableLoadsProp selectedItem = dtlpView.getSelectionModel().getSelectedItem();
                    //Узнаем индекс строки
                    int selected_index = dtlpView.getItems().indexOf(selectedItem);
                    //Удаляем выбранный узел
                    dtlpView.getItems().remove(selected_index);
                    lbl_log2.setText("Заделка в узле " + (selected_index + 1) + " убрана");
                    //Сохраняем все данные
                    save_all(tableView, dtlpView, dtlfView, dtsllView);
                }
            }
        });

        btn_add_node1.setOnAction(ActionEvent -> {
            ValidatorManager.validate_add_long_forces(lbl_log2, dtlfView, tableView, input_node1, input_q_i);
            //Сохраняем все данные
            save_all(tableView, dtlpView, dtlfView, dtsllView);
            //Очищаем текстовые поля
            input_node1.clear();
            input_q_i.clear();
        });

        btn_remove_node1.setOnAction(ActionEvent -> {
            if (!dtlfView.getItems().isEmpty()) {
                if (!dtlfView.getSelectionModel().isEmpty()) {
                    //Получаем выбранную строку
                    DataTableLongitudinalForces selectedItem = dtlfView.getSelectionModel().getSelectedItem();
                    //Узнаем индекс строки
                    int selected_index = dtlfView.getItems().indexOf(selectedItem);
                    //Удаляем выбранный узел
                    dtlfView.getItems().remove(selected_index);
                    lbl_log2.setText("Погонная нагрузка стержня " + (selected_index + 1) + " убрана");
                    //Сохраняем все данные
                    save_all(tableView, dtlpView, dtlfView, dtsllView);
                }
            }
        });

        btn_add_node2.setOnAction(ActionEvent -> {
            ValidatorManager.validate_add_static_loads(lbl_log2, dtsllView, tableView, input_node2, input_f_i);
            //Сохраняем все данные
            save_all(tableView, dtlpView, dtlfView, dtsllView);
            //Очищаем текстовые поля
            input_node2.clear();
            input_f_i.clear();
        });

        btn_remove_node2.setOnAction(ActionEvent -> {
            if (!dtsllView.getItems().isEmpty()) {
                if (!dtsllView.getSelectionModel().isEmpty()) {
                    //Получаем выбранную строку
                    DataTableStaticLinearLoads selectedItem = dtsllView.getSelectionModel().getSelectedItem();
                    //Узнаем индекс строки
                    int selected_index = dtsllView.getItems().indexOf(selectedItem);
                    //Удаляем выбранный узел
                    dtsllView.getItems().remove(selected_index);
                    lbl_log2.setText("Продольное усилие в узле " + (selected_index + 1) + " убрано");
                    //Сохраняем все данные
                    save_all(tableView, dtlpView, dtlfView, dtsllView);
                }
            }
        });

        btn_show_rod.setOnAction(ActionEvent -> {
            save_all(tableView, dtlpView, dtlfView, dtsllView);
            draw_construction();
        });

        btn_clear_all.setOnAction(ActionEvent -> {
            //Переход на экран подтверждения
            show_scene_clear_all(tableView,dtlfView,dtlpView,dtsllView,lbl_log2,lbl_log);
            
        });
    }

    private void show_scene_clear_all(TableView<DataTableConstruction> tableView,
                                      TableView<DataTableLongitudinalForces> dtlfView,
                                      TableView<DataTableLoadsProp> dtlpView,
                                      TableView<DataTableStaticLinearLoads> dtsllView,
                                      Label lbl_log2,Label lbl_log)
    {
        
        
        AnchorPane root = new AnchorPane();

        //Размещаем лого и задаем его расположение
        ImageView logo = new ImageView(im);
        logo.setPreserveRatio(true);
        logo.setFitHeight(100);
        AnchorPane.setTopAnchor(logo, 50.0);
        AnchorPane.setRightAnchor(logo, 10.0);
        
        Button panel = new Button("Очистка всех данных проекта...");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Нет, я передумал");
        Button btn2 = new Button("Да, очистить все");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(btn2, -5.0);

        AnchorPane.setBottomAnchor(btn1, 30.0);
        AnchorPane.setBottomAnchor(btn2, 0.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(btn2, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        
        Label label = new Label("Вы действительно хотите очистить все данные проекта?");
        label.setFont(new Font("Helvetica", 14));
        AnchorPane.setLeftAnchor(label, 50.0);
        AnchorPane.setBottomAnchor(label, 80.0);
        

        DesignManager.set_acces_scene(root, btn1, btn2, panel);

        root.getChildren().addAll(panel, btn1, btn2,label,logo);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            create_constrution();
        });

        btn2.setOnAction(ActionEvent -> {
            tableView.getItems().clear();
            dtlfView.getItems().clear();
            dtlpView.getItems().clear();
            dtsllView.getItems().clear();
            lbl_log2.setText("Все нагрузки очищены");
            lbl_log.setText("Все стержни убраны");
            save_all(tableView, dtlpView, dtlfView, dtsllView);
            create_constrution();
        });  
    }
    
    private void draw_construction() {
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Демонстрация конструкции");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Вернуться назад");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);

        AnchorPane.setBottomAnchor(btn1, 0.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);

        //Отрисовываем лейбл логов
        Label lbl_log = new Label("Конструкция успешно отрисована!");
        lbl_log.setFont(new Font("Helvetica", 14));
        AnchorPane.setLeftAnchor(lbl_log, 50.0);
        AnchorPane.setBottomAnchor(lbl_log, 50.0);
        Label lbl_descrip1 = new Label("");
        Label lbl_descrip2 = new Label("");
        Label lbl_descrip3 = new Label("");
        Label lbl_descrip4 = new Label("");
        AnchorPane.setLeftAnchor(lbl_descrip1, 80.0);
        AnchorPane.setBottomAnchor(lbl_descrip1, 150.0);
        AnchorPane.setLeftAnchor(lbl_descrip2, 80.0);
        AnchorPane.setBottomAnchor(lbl_descrip2, 100.0);
        AnchorPane.setLeftAnchor(lbl_descrip3, 480.0);
        AnchorPane.setBottomAnchor(lbl_descrip3, 150.0);
        AnchorPane.setLeftAnchor(lbl_descrip4, 480.0);
        AnchorPane.setBottomAnchor(lbl_descrip4, 100.0);
        lbl_descrip1.setFont(new Font("Helvetica", 20));
        lbl_descrip2.setFont(new Font("Helvetica", 20));
        lbl_descrip3.setFont(new Font("Helvetica", 20));
        lbl_descrip4.setFont(new Font("Helvetica", 20));

        ArrayList<Rectangle> rods = new ArrayList<>();
        ArrayList<Line> left_prop = new ArrayList<>();
        ArrayList<Line> right_prop = new ArrayList<>();
        ArrayList<Line> long_forces = new ArrayList<>();
        ArrayList<Line> static_forces = new ArrayList<>();
        ArrayList<Line> extra_lines = new ArrayList<>();
        ArrayList<Label> labels = new ArrayList<>();
        ArrayList<Rectangle> nums_nodes = new ArrayList<>();
        ArrayList<Circle> nums_rods = new ArrayList<>();
        ArrayList<Line> descript_lines = new ArrayList<>();
        
        DrawConstructionManager.draw(construction, lbl_log, rods, left_prop,
                                     right_prop, long_forces, static_forces,
                                     extra_lines,labels,nums_nodes,nums_rods,
                                     descript_lines,lbl_descrip1,lbl_descrip2,
                                     lbl_descrip3,lbl_descrip4);

        DesignManager.set_draw_construction(root, btn1, panel);

        root.getChildren().addAll(panel, btn1, lbl_log,lbl_descrip1,
                                  lbl_descrip2,lbl_descrip3,lbl_descrip4);
        extra_lines.stream().forEach(i -> root.getChildren().add(i));
        rods.stream().forEach(i -> root.getChildren().add(i));
        left_prop.stream().forEach(i -> root.getChildren().add(i));
        right_prop.stream().forEach(i -> root.getChildren().add(i));
        long_forces.stream().forEach(i -> root.getChildren().add(i));
        static_forces.stream().forEach(i -> root.getChildren().add(i));
        nums_nodes.stream().forEach(i -> root.getChildren().add(i));
        nums_rods.stream().forEach(i -> root.getChildren().add(i));
        labels.stream().forEach(i -> root.getChildren().add(i));
        descript_lines.stream().forEach(i -> root.getChildren().add(i));

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            create_constrution();
        });
    }

    private void save_all(TableView<DataTableConstruction> tableView,
                          TableView<DataTableLoadsProp> dtlpView,
                          TableView<DataTableLongitudinalForces> dtlfView,
                          TableView<DataTableStaticLinearLoads> dtsllView) {
        construction.setTableView(tableView.getItems());
        construction.setDtlpView(dtlpView.getItems());
        construction.setDtlfView(dtlfView.getItems());
        construction.setDtsllView(dtsllView.getItems());
        construction.setIsEdit(true);
    }

    private void load_construction(TableView<DataTableConstruction> tableView,
                                   TableView<DataTableLoadsProp> dtlpView,
                                   TableView<DataTableLongitudinalForces> dtlfView,
                                   TableView<DataTableStaticLinearLoads> dtsllView) {
        if (construction.getTableView() != null
                && construction.getDtlpView() != null
                && construction.getDtlfView() != null
                && construction.getDtsllView() != null) {
            tableView.setItems(construction.getTableView());
            dtlpView.setItems(construction.getDtlpView());
            dtlfView.setItems(construction.getDtlfView());
            dtsllView.setItems(construction.getDtsllView());
            save_all(tableView, dtlpView, dtlfView, dtsllView);
        }
    }

    
    private void calculate_construction()
    {
        
        Label log_label = new Label("Вектор перемещений успешно рассчитан!");
        log_label.setFont(new Font("Helvetica", 14));
        AnchorPane.setLeftAnchor(log_label, 50.0);
        AnchorPane.setBottomAnchor(log_label, 50.0);
        
        TextArea result_label = new TextArea();
        result_label.setFont(new Font("Consolas", 14));
        result_label.setWrapText(true);
        result_label.setEditable(false);
        AnchorPane.setLeftAnchor(result_label, 50.0);
        AnchorPane.setTopAnchor(result_label, 60.0);
        AnchorPane.setRightAnchor(result_label, 50.0);
        AnchorPane.setBottomAnchor(result_label, 80.0);
        
        
        
        Processor.calculate_delta(construction,log_label, result_label);
        
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Расчет вектора Дельта (Глобального вектора перемещений)");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Завершить");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);

        AnchorPane.setBottomAnchor(btn1, 0.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);

        DesignManager.set_calculate_construction(root, btn1,panel);

        root.getChildren().addAll(panel, btn1,log_label,result_label);

        scene.setRoot(root);
        primaryStage.setScene(scene);
        
        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            project_panel();
        });
    }

    
    private void show_result_table()
    {  
        AnchorPane root = new AnchorPane();
        
        Label lbl = new Label("Выберите стержень из списка");
        lbl.setAlignment(Pos.CENTER);
        lbl.setFont(new Font("Helvetica", 14));
        Label lbl2 = new Label("Выберите количество рассчитываемых отрезков в стержне");
        lbl2.setAlignment(Pos.CENTER);
        lbl2.setFont(new Font("Helvetica", 14));
        Label lbl3 = new Label("");
        lbl3.setAlignment(Pos.CENTER);
        lbl3.setFont(new Font("Helvetica", 14));
        
        Slider slider = new Slider(1.0, 100, 1.0);
        slider.setBlockIncrement(1.0);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setMajorTickUnit(10.0);
        slider.setMinorTickCount(9);
        slider.setSnapToTicks(true);
       
        ComboBox<String> rods_box = new ComboBox<>();
        if (construction.getTableView() != null) {
            if (construction.getTableView().size() > 0) {
                for (int i = 0; i < construction.getTableView().size(); i++) {
                    rods_box.getItems().add("Стержень " + construction.getTableView().get(i).getNum());
                }
            }
            else {
                lbl.setText("Конструкция не задана! Исправьте");
            }
        }
        else {
            lbl.setText("Конструкция не задана! Исправьте");
        }
        
        
        //Создаем пока пустую таблицу результатов
        //---------------------------------------------------------------------
        ObservableList<DataTableResult> dtresult = FXCollections.observableArrayList();

        TableView<DataTableResult> tableResult = new TableView<>();

        //Расположение таблицы конструкции
        AnchorPane.setLeftAnchor(tableResult, 50.0);
        AnchorPane.setTopAnchor(tableResult, 230.0);
        AnchorPane.setRightAnchor(tableResult, 50.0);

        tableResult.setMaxHeight(330.0);

        tableResult.setTableMenuButtonVisible(true);
        tableResult.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        //Далее задаем колонки
        TableColumn numCol = new TableColumn("№");
        TableColumn xCol = new TableColumn("x");
        TableColumn NxCol = new TableColumn("Nₓ");
        TableColumn UxCol = new TableColumn("Uₓ");
        TableColumn sigmaXCol = new TableColumn("σₓ");
        TableColumn sigmaCol = new TableColumn("[σ]");

        numCol.setCellValueFactory(new PropertyValueFactory("num"));
        xCol.setCellValueFactory(new PropertyValueFactory("x"));
        NxCol.setCellValueFactory(new PropertyValueFactory("Nx"));
        UxCol.setCellValueFactory(new PropertyValueFactory("Ux"));
        sigmaXCol.setCellValueFactory(new PropertyValueFactory("sigmaX"));
        sigmaCol.setCellValueFactory(new PropertyValueFactory("sigma"));

        numCol.setPrefWidth(50.0);
        xCol.setPrefWidth(140.0);
        NxCol.setPrefWidth(140.0);
        UxCol.setPrefWidth(140.0);
        sigmaXCol.setPrefWidth(140.0);
        sigmaCol.setPrefWidth(140.0);

        numCol.setResizable(true);
        xCol.setResizable(true);
        NxCol.setResizable(true);
        UxCol.setResizable(true);
        sigmaXCol.setResizable(true);
        sigmaCol.setResizable(true);
       
        tableResult.getColumns().addAll(numCol, xCol, NxCol, UxCol, sigmaXCol,sigmaCol);
        tableResult.setItems(dtresult);
        //---------------------------------------------------------------------
        
        Button panel = new Button("Результаты в табличном виде");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Вернуться назад");

        AnchorPane.setTopAnchor(rods_box, 80.0);
        AnchorPane.setTopAnchor(lbl, 50.0);
        AnchorPane.setTopAnchor(lbl2, 120.0);
        AnchorPane.setTopAnchor(slider, 150.0);
        AnchorPane.setTopAnchor(lbl3, 190.0);
        
        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(rods_box, 50.0);
        AnchorPane.setLeftAnchor(lbl, 50.0);
        AnchorPane.setLeftAnchor(lbl2, 50.0);
        AnchorPane.setLeftAnchor(slider, 50.0);
        AnchorPane.setLeftAnchor(lbl3, 50.0);

        AnchorPane.setBottomAnchor(btn1, 0.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(rods_box, 50.0);
        AnchorPane.setRightAnchor(lbl, 50.0);
        AnchorPane.setRightAnchor(lbl2, 50.0);
        AnchorPane.setRightAnchor(slider, 50.0);
        AnchorPane.setRightAnchor(lbl3, 50.0);

        DesignManager.set_result_table(root, btn1,panel,tableResult);

        root.getChildren().addAll(panel, btn1, rods_box,lbl,lbl2,slider,lbl3,tableResult);

        scene.setRoot(root);

        primaryStage.setScene(scene);
        
        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            project_panel();
        });
        rods_box.setOnAction(ActionEvent->{
            tableResult.getItems().clear();
            lbl3.setText(rods_box.getSelectionModel().getSelectedItem());
            //Рассчитываем для выбранного стержня результаты
            //И заполняем таблицу
            if(construction.getTableView()!=null)
            {
                //Сначала получаем номер стержня(порядковый)
                int num_rod = rods_box.getSelectionModel().getSelectedIndex();
                int name_rod = Integer.valueOf(rods_box.getSelectionModel().getSelectedItem().split(" ")[1]);
                //Получаем количество точек
                int num_points = (int) slider.getValue();
                //Получаем список x точек
                //Получаем длину стержня
                double Lp = construction.getTableView().get(num_rod).getL();
                //Сначала получаем длину шага
                double len_step = Lp / (double) num_points;
                if (construction.getDeltaVector() != null) {
                    double[] deltaVector = construction.getDeltaVector();
                    //Ищем нужный qp
                    double qp = 0.0;
                    for (int i = 0; i < construction.getDtlfView().size(); i++) {
                        if(construction.getDtlfView().get(i).get_nodeNum() == name_rod)
                        {
                            qp = construction.getDtlfView().get(i).get_forceNum();
                            break;
                        }
                    }
                    //Получаем модуль упругости стержня
                    double Ep = construction.getTableView().get(num_rod).getE();
                    //Получаем площадь поперечного сечения стержня
                    double Ap = construction.getTableView().get(num_rod).getA();
                    //Получаем допускаемое напряжение стержня
                    double Bp = construction.getTableView().get(num_rod).getB();
                    
                    //Временная отладка
                    System.out.println("d1:"+deltaVector[num_rod]);
                    System.out.println("d2:"+deltaVector[num_rod+1]);
                    System.out.println("LP:"+Lp);
                    System.out.println("qp:"+qp);
                    System.out.println("EP:"+Ep);
                    System.out.println("AP:"+Ap);
                    for (int i = 0; i <= num_points; i++) {
                        double x = len_step * i;
                        ArrayList<Double> values 
                                = Processor.calculateDisplacementMethod(deltaVector[num_rod],
                                                                      deltaVector[num_rod+1],
                                                                      x,Lp,qp,Ep,Ap);
                        
                        tableResult.getItems().add(new DataTableResult(i+1,
                                Math.round(x*100.0)/100.0,
                                Math.round(values.get(1)*10000.0)/10000.0,
                                Math.round(values.get(0)*10000.0)/10000.0,
                                Math.round(values.get(2)*10000.0)/10000.0,
                                Math.round(Bp*10000.0)/10000.0)); 
                    }
                }
                else {
                    lbl.setText("Дельта-вектор не рассчитан!");
                }
            }
            else
            {
                lbl.setText("Конструкция не задана! Исправьте");
            }
        });
         slider.setOnMouseReleased(ActionEvent->{
             tableResult.getItems().clear();
             if (rods_box.getSelectionModel().getSelectedItem() != null) {
                 lbl3.setText(rods_box.getSelectionModel().getSelectedItem());
                 //Рассчитываем для выбранного стержня результаты
                 //И заполняем таблицу
                 if (construction.getTableView() != null) 
                 {
                     //Сначала получаем номер стержня(порядковый)
                     int num_rod = rods_box.getSelectionModel().getSelectedIndex();
                     int name_rod = Integer.valueOf(rods_box.getSelectionModel().getSelectedItem().split(" ")[1]);
                     //Получаем количество точек
                     int num_points = (int) slider.getValue();
                     //Получаем список x точек
                     //Получаем длину стержня
                     double Lp = construction.getTableView().get(num_rod).getL();
                     //Сначала получаем длину шага
                     double len_step = Lp / (double) num_points;
                     if (construction.getDeltaVector() != null) {
                         double[] deltaVector = construction.getDeltaVector();
                         //Ищем нужный qp
                         double qp = 0.0;
                         for (int i = 0; i < construction.getDtlfView().size(); i++) {
                             if (construction.getDtlfView().get(i).get_nodeNum() == name_rod) {
                                 qp = construction.getDtlfView().get(i).get_forceNum();
                                 break;
                             }
                         }
                         //Получаем модуль упругости стержня
                         double Ep = construction.getTableView().get(num_rod).getE();
                         //Получаем площадь поперечного сечения стержня
                         double Ap = construction.getTableView().get(num_rod).getA();
                         //Получаем допускаемое напряжение стержня
                         double Bp = construction.getTableView().get(num_rod).getB();

                         //Временная отладка
                         System.out.println("d1:" + deltaVector[num_rod]);
                         System.out.println("d2:" + deltaVector[num_rod + 1]);
                         System.out.println("LP:" + Lp);
                         System.out.println("qp:" + qp);
                         System.out.println("EP:" + Ep);
                         System.out.println("AP:" + Ap);
                         for (int i = 0; i <= num_points; i++) {
                             double x = len_step * i;
                             ArrayList<Double> values
                                     = Processor.calculateDisplacementMethod(deltaVector[num_rod],
                                                                             deltaVector[num_rod + 1],
                                                                             x, Lp, qp, Ep, Ap);

                             tableResult.getItems().add(new DataTableResult(i + 1,
                                                                            Math.round(x * 100.0) / 100.0,
                                                                            Math.round(values.get(1) * 10000.0) / 10000.0,
                                                                            Math.round(values.get(0) * 10000.0) / 10000.0,
                                                                            Math.round(values.get(2) * 10000.0) / 10000.0,
                                                                            Math.round(Bp * 10000.0) / 10000.0));
                         }
                     }
                     else {
                         lbl.setText("Дельта-вектор не рассчитан!");
                     }
                 }
                 else {
                     lbl.setText("Конструкция не задана! Исправьте");
                 }
             }
        });
         slider.setOnKeyReleased(ActionEvent->{
            tableResult.getItems().clear();
            if (rods_box.getSelectionModel().getSelectedItem() != null) {
                 lbl3.setText(rods_box.getSelectionModel().getSelectedItem());
                 //Рассчитываем для выбранного стержня результаты
                 //И заполняем таблицу
                 if (construction.getTableView() != null) {
                     //Сначала получаем номер стержня(порядковый)
                     int num_rod = rods_box.getSelectionModel().getSelectedIndex();
                     int name_rod = Integer.valueOf(rods_box.getSelectionModel().getSelectedItem().split(" ")[1]);
                     //Получаем количество точек
                     int num_points = (int) slider.getValue();
                     //Получаем список x точек
                     //Получаем длину стержня
                     double Lp = construction.getTableView().get(num_rod).getL();
                     //Сначала получаем длину шага
                     double len_step = Lp / (double) num_points;
                     if (construction.getDeltaVector() != null) {
                         double[] deltaVector = construction.getDeltaVector();
                         //Ищем нужный qp
                         double qp = 0.0;
                         for (int i = 0; i < construction.getDtlfView().size(); i++) {
                             if (construction.getDtlfView().get(i).get_nodeNum() == name_rod) {
                                 qp = construction.getDtlfView().get(i).get_forceNum();
                                 break;
                             }
                         }
                         //Получаем модуль упругости стержня
                         double Ep = construction.getTableView().get(num_rod).getE();
                         //Получаем площадь поперечного сечения стержня
                         double Ap = construction.getTableView().get(num_rod).getA();
                         //Получаем допускаемое напряжение стержня
                         double Bp = construction.getTableView().get(num_rod).getB();

                         //Временная отладка
                         System.out.println("d1:" + deltaVector[num_rod]);
                         System.out.println("d2:" + deltaVector[num_rod + 1]);
                         System.out.println("LP:" + Lp);
                         System.out.println("qp:" + qp);
                         System.out.println("EP:" + Ep);
                         System.out.println("AP:" + Ap);
                         for (int i = 0; i <= num_points; i++) {
                             double x = len_step * i;
                             ArrayList<Double> values
                                     = Processor.calculateDisplacementMethod(deltaVector[num_rod],
                                                                             deltaVector[num_rod + 1],
                                                                             x, Lp, qp, Ep, Ap);

                             tableResult.getItems().add(new DataTableResult(i + 1,
                                                                            Math.round(x * 100.0) / 100.0,
                                                                            Math.round(values.get(1) * 10000.0) / 10000.0,
                                                                            Math.round(values.get(0) * 10000.0) / 10000.0,
                                                                            Math.round(values.get(2) * 10000.0) / 10000.0,
                                                                            Math.round(Bp * 10000.0) / 10000.0));
                         }
                     }
                     else {
                         lbl.setText("Дельта-вектор не рассчитан!");
                     }
                 }
                 else {
                     lbl.setText("Конструкция не задана! Исправьте");
                 }
             }
        });
    }
    
    private void show_to_delta_scene()
    {
        AnchorPane root = new AnchorPane();

        //Размещаем лого и задаем его расположение
        ImageView logo = new ImageView(im);
        logo.setPreserveRatio(true);
        logo.setFitHeight(100);
        AnchorPane.setTopAnchor(logo, 50.0);
        AnchorPane.setRightAnchor(logo, 10.0);
        
        Button panel = new Button("Внимание!");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Нет, я передумал");
        Button btn2 = new Button("Да, рассчитать");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(btn2, -5.0);

        AnchorPane.setBottomAnchor(btn1, 30.0);
        AnchorPane.setBottomAnchor(btn2, 0.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(btn2, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        
        Label label = new Label("ВНИМАНИЕ! Необходимо рассчитать вектор дельта. Рассчитать?");
        label.setFont(new Font("Helvetica", 14));
        AnchorPane.setLeftAnchor(label, 50.0);
        AnchorPane.setBottomAnchor(label, 80.0);
        
        DesignManager.set_acces_scene(root, btn1, btn2, panel);

        root.getChildren().addAll(panel, btn1, btn2,label,logo);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            project_panel();
        });

        btn2.setOnAction(ActionEvent -> {
            calculate_construction();
        });  
    }

    private void save_file(Label lbl)
    {
        //Перед сохранением преобразуем все данные конструкции в сериализуемый объект
        DataSerialize dataSerialize = new DataSerialize();
        if(construction.getTableView()!=null)
        {
            if(construction.getTableView().size()>0)
            {
                construction.getTableView().stream().forEach(i->{
                    ArrayList<Double> al = new ArrayList<>();
                    al.add((double)i.getNum());
                    al.add(i.getL());
                    al.add(i.getA());
                    al.add(i.getE());
                    al.add(i.getB());
                    dataSerialize.tableView.add(al);
                            });
                construction.getDtlpView().stream().forEach(i->{
                    ArrayList<Integer> al = new ArrayList<>();
                    al.add(i.get_nodeNum());
                    dataSerialize.tableLoads.add(al);
                });
                construction.getDtlfView().stream().forEach(i->{
                    ArrayList<Double> al = new ArrayList<>();
                    al.add((double)i.get_nodeNum());
                    al.add(i.get_forceNum());
                    dataSerialize.tableLongForces.add(al);
                });
                construction.getDtsllView().forEach(i->{
                    ArrayList<Double> al = new ArrayList<>();
                    al.add((double)i.get_num_rodNum());
                    al.add(i.get_sllNum());
                    dataSerialize.tableForces.add(al);
                });


                //Сохраняем все данные в файл
                try{
                   ObjectOutputStream dos 
                        = new ObjectOutputStream(new FileOutputStream(project_name + ".kpr"));
                            dos.writeObject(dataSerialize);
                            dos.flush();
                            dos.close(); 
                            lbl.setText("Файл успешно сохранен под именем "+project_name + ".kpr");
                }
                catch(IOException ex)
                {
                    lbl.setText("Файл не удалось сохранить");
                } 
            }
            else
            {
                lbl.setText("Конструкция не задана. Вы не можете сохранить проект");
            }
        }
        else
        {
            lbl.setText("Конструкция не задана. Вы не можете сохранить проект");
        }
    }

    private void draw_diagrams()
    {
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Демонстрация Nₓ эпюр");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Вернуться назад");
        
        Button btn_right = new Button("Uₓ\n\n\n\n\n\n\n\n>");
        btn_right.setWrapText(true);
        btn_right.setAlignment(Pos.TOP_CENTER);
        btn_right.setMaxWidth(50.0);
        btn_right.setMinWidth(50.0);
        Button btn_left = new Button("σₓ\n\n\n\n\n\n\n\n<");
        btn_left.setWrapText(true);
        btn_left.setAlignment(Pos.TOP_CENTER);
        btn_left.setMaxWidth(50.0);
        btn_left.setMinWidth(50.0);

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(btn_left, -5.0);

        AnchorPane.setBottomAnchor(btn1, 0.0);
        AnchorPane.setBottomAnchor(btn_left, 30.0);
        AnchorPane.setBottomAnchor(btn_right, 30.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(btn_right, -5.0);
        
        AnchorPane.setTopAnchor(btn_left, 40.0);
        AnchorPane.setTopAnchor(btn_right, 40.0);
        

        //Отрисовываем лейбл логов
        Label lbl_log = new Label("Конструкция успешно отрисована!");
        lbl_log.setFont(new Font("Helvetica", 14));
        AnchorPane.setLeftAnchor(lbl_log, 50.0);
        AnchorPane.setBottomAnchor(lbl_log, 50.0);
        Label lbl_descrip1 = new Label("");
        Label lbl_descrip2 = new Label("");
        Label lbl_descrip3 = new Label("");
        Label lbl_descrip4 = new Label("");
        AnchorPane.setLeftAnchor(lbl_descrip1, 80.0);
        AnchorPane.setBottomAnchor(lbl_descrip1, 150.0);
        AnchorPane.setLeftAnchor(lbl_descrip2, 80.0);
        AnchorPane.setBottomAnchor(lbl_descrip2, 100.0);
        AnchorPane.setLeftAnchor(lbl_descrip3, 480.0);
        AnchorPane.setBottomAnchor(lbl_descrip3, 150.0);
        AnchorPane.setLeftAnchor(lbl_descrip4, 480.0);
        AnchorPane.setBottomAnchor(lbl_descrip4, 100.0);
        lbl_descrip1.setFont(new Font("Helvetica", 20));
        lbl_descrip2.setFont(new Font("Helvetica", 20));
        lbl_descrip3.setFont(new Font("Helvetica", 20));
        lbl_descrip4.setFont(new Font("Helvetica", 20));

        ArrayList<Rectangle> rods = new ArrayList<>();
        ArrayList<Line> left_prop = new ArrayList<>();
        ArrayList<Line> right_prop = new ArrayList<>();
        ArrayList<Line> long_forces = new ArrayList<>();
        ArrayList<Line> static_forces = new ArrayList<>();
        ArrayList<Line> extra_lines = new ArrayList<>();
        ArrayList<Label> labels = new ArrayList<>();
        ArrayList<Rectangle> nums_nodes = new ArrayList<>();
        ArrayList<Circle> nums_rods = new ArrayList<>();
        ArrayList<Line> descript_lines = new ArrayList<>();
        
        ArrayList<ArrayList<Double>> nx_values = new ArrayList<>();
        
        if (construction.getTableView() != null) {
            for (int i = 0; i < construction.getTableView().size(); i++) {
                //Сначала получаем номер стержня(порядковый)
                int num_rod = i;
                int name_rod = construction.getTableView().get(i).getNum(); 

                //Получаем длину стержня
                double Lp = construction.getTableView().get(num_rod).getL();
                //Сначала получаем длину шага
                double len_step = (double) Lp / (double) 100;
                if (construction.getDeltaVector() != null) 
                {
                    double[] deltaVector = construction.getDeltaVector();
                    //Ищем нужный qp
                    double qp = 0.0;
                    for (int j = 0; j < construction.getDtlfView().size(); j++) {
                        if(construction.getDtlfView().get(j).get_nodeNum() == name_rod)
                        {
                            qp = construction.getDtlfView().get(j).get_forceNum();
                            break;
                        }
                    }
                    //Получаем модуль упругости стержня
                    double Ep = construction.getTableView().get(num_rod).getE();
                    //Получаем площадь поперечного сечения стержня
                    double Ap = construction.getTableView().get(num_rod).getA();
                    
                    ArrayList<Double> nx = new ArrayList<>();
                    ArrayList<Double> ux = new ArrayList<>();
                    ArrayList<Double> sigmax = new ArrayList<>();
                    
                    for (int j = 0; j <= 100; j++) {
                        double x = len_step * j;
                        ArrayList<Double> values 
                                = Processor.calculateDisplacementMethod(deltaVector[num_rod],
                                                                      deltaVector[num_rod+1],
                                                                      x,Lp,qp,Ep,Ap);
                        
                        nx.add(values.get(1));
                    }
                    
                    nx_values.add(nx);
                }
                else {
                    lbl_log.setText("Дельта-вектор не рассчитан!");
                }
                
            }
                 }
                 else {
                     lbl_log.setText("Конструкция не задана! Исправьте");
                 }
        
        
        
        DrawConstructionManager.draw_diagramm_1(construction, lbl_log, rods, left_prop,
                                     right_prop, long_forces, static_forces,
                                     extra_lines,labels,nums_nodes,nums_rods,
                                     descript_lines,lbl_descrip1,lbl_descrip2,
                                     lbl_descrip3,lbl_descrip4,nx_values);

        DesignManager.set_draw_diagram(root, btn1, panel,btn_left,btn_right);

        root.getChildren().addAll(panel, btn1, lbl_log,lbl_descrip1,
                                  lbl_descrip2,lbl_descrip3,lbl_descrip4,
                                  btn_left,btn_right);
        extra_lines.stream().forEach(i -> root.getChildren().add(i));
        rods.stream().forEach(i -> root.getChildren().add(i));
        left_prop.stream().forEach(i -> root.getChildren().add(i));
        right_prop.stream().forEach(i -> root.getChildren().add(i));
        long_forces.stream().forEach(i -> root.getChildren().add(i));
        static_forces.stream().forEach(i -> root.getChildren().add(i));
        nums_nodes.stream().forEach(i -> root.getChildren().add(i));
        nums_rods.stream().forEach(i -> root.getChildren().add(i));
        labels.stream().forEach(i -> root.getChildren().add(i));
        descript_lines.stream().forEach(i -> root.getChildren().add(i));

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            project_panel();
        });
        
        btn_right.setOnAction(ActionEvent -> {
            draw_diagrams_2();
        });
        
        btn_left.setOnAction(ActionEvent -> {
            draw_diagrams_3();
        });
    }

    private void draw_diagrams_2()
    {
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Демонстрация Uₓ эпюр");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Вернуться назад");
        
        Button btn_right = new Button("σₓ\n\n\n\n\n\n\n\n>");
        btn_right.setWrapText(true);
        btn_right.setAlignment(Pos.TOP_CENTER);
        btn_right.setMaxWidth(50.0);
        btn_right.setMinWidth(50.0);
        Button btn_left = new Button("Nₓ\n\n\n\n\n\n\n\n<");
        btn_left.setWrapText(true);
        btn_left.setAlignment(Pos.TOP_CENTER);
        btn_left.setMaxWidth(50.0);
        btn_left.setMinWidth(50.0);

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(btn_left, -5.0);

        AnchorPane.setBottomAnchor(btn1, 0.0);
        AnchorPane.setBottomAnchor(btn_left, 30.0);
        AnchorPane.setBottomAnchor(btn_right, 30.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(btn_right, -5.0);
        
        AnchorPane.setTopAnchor(btn_left, 40.0);
        AnchorPane.setTopAnchor(btn_right, 40.0);
        

        //Отрисовываем лейбл логов
        Label lbl_log = new Label("Конструкция успешно отрисована!");
        lbl_log.setFont(new Font("Helvetica", 14));
        AnchorPane.setLeftAnchor(lbl_log, 50.0);
        AnchorPane.setBottomAnchor(lbl_log, 50.0);
        Label lbl_descrip1 = new Label("");
        Label lbl_descrip2 = new Label("");
        Label lbl_descrip3 = new Label("");
        Label lbl_descrip4 = new Label("");
        AnchorPane.setLeftAnchor(lbl_descrip1, 80.0);
        AnchorPane.setBottomAnchor(lbl_descrip1, 150.0);
        AnchorPane.setLeftAnchor(lbl_descrip2, 80.0);
        AnchorPane.setBottomAnchor(lbl_descrip2, 100.0);
        AnchorPane.setLeftAnchor(lbl_descrip3, 480.0);
        AnchorPane.setBottomAnchor(lbl_descrip3, 150.0);
        AnchorPane.setLeftAnchor(lbl_descrip4, 480.0);
        AnchorPane.setBottomAnchor(lbl_descrip4, 100.0);
        lbl_descrip1.setFont(new Font("Helvetica", 20));
        lbl_descrip2.setFont(new Font("Helvetica", 20));
        lbl_descrip3.setFont(new Font("Helvetica", 20));
        lbl_descrip4.setFont(new Font("Helvetica", 20));

        ArrayList<Rectangle> rods = new ArrayList<>();
        ArrayList<Line> left_prop = new ArrayList<>();
        ArrayList<Line> right_prop = new ArrayList<>();
        ArrayList<Line> long_forces = new ArrayList<>();
        ArrayList<Line> static_forces = new ArrayList<>();
        ArrayList<Line> extra_lines = new ArrayList<>();
        ArrayList<Label> labels = new ArrayList<>();
        ArrayList<Rectangle> nums_nodes = new ArrayList<>();
        ArrayList<Circle> nums_rods = new ArrayList<>();
        ArrayList<Line> descript_lines = new ArrayList<>();
        
        ArrayList<ArrayList<Double>> ux_values = new ArrayList<>();

        
        if (construction.getTableView() != null) {
            for (int i = 0; i < construction.getTableView().size(); i++) {
                //Сначала получаем номер стержня(порядковый)
                int num_rod = i;
                int name_rod = construction.getTableView().get(i).getNum(); 

                //Получаем длину стержня
                double Lp = construction.getTableView().get(num_rod).getL();
                //Сначала получаем длину шага
                double len_step = (double) Lp / (double) 100;
                if (construction.getDeltaVector() != null) 
                {
                    double[] deltaVector = construction.getDeltaVector();
                    //Ищем нужный qp
                    double qp = 0.0;
                    for (int j = 0; j < construction.getDtlfView().size(); j++) {
                        if(construction.getDtlfView().get(j).get_nodeNum() == name_rod)
                        {
                            qp = construction.getDtlfView().get(j).get_forceNum();
                            break;
                        }
                    }
                    //Получаем модуль упругости стержня
                    double Ep = construction.getTableView().get(num_rod).getE();
                    //Получаем площадь поперечного сечения стержня
                    double Ap = construction.getTableView().get(num_rod).getA();
                    
                    ArrayList<Double> nx = new ArrayList<>();
                    ArrayList<Double> ux = new ArrayList<>();
                    ArrayList<Double> sigmax = new ArrayList<>();
                    
                    for (int j = 0; j <= 100; j++) {
                        double x = len_step * j;
                        ArrayList<Double> values 
                                = Processor.calculateDisplacementMethod(deltaVector[num_rod],
                                                                      deltaVector[num_rod+1],
                                                                      x,Lp,qp,Ep,Ap);
                        
                        ux.add(values.get(0));
                    }
                    
                    ux_values.add(ux);
                }
                else {
                    lbl_log.setText("Дельта-вектор не рассчитан!");
                }
                
            }
                 }
                 else {
                     lbl_log.setText("Конструкция не задана! Исправьте");
                 }
        
        
        
        DrawConstructionManager.draw_diagramm_2(construction, lbl_log, rods, left_prop,
                                     right_prop, long_forces, static_forces,
                                     extra_lines,labels,nums_nodes,nums_rods,
                                     descript_lines,lbl_descrip1,lbl_descrip2,
                                     lbl_descrip3,lbl_descrip4,ux_values);

        DesignManager.set_draw_diagram(root, btn1, panel,btn_left,btn_right);

        root.getChildren().addAll(panel, btn1, lbl_log,lbl_descrip1,
                                  lbl_descrip2,lbl_descrip3,lbl_descrip4,
                                  btn_left,btn_right);
        extra_lines.stream().forEach(i -> root.getChildren().add(i));
        rods.stream().forEach(i -> root.getChildren().add(i));
        left_prop.stream().forEach(i -> root.getChildren().add(i));
        right_prop.stream().forEach(i -> root.getChildren().add(i));
        long_forces.stream().forEach(i -> root.getChildren().add(i));
        static_forces.stream().forEach(i -> root.getChildren().add(i));
        nums_nodes.stream().forEach(i -> root.getChildren().add(i));
        nums_rods.stream().forEach(i -> root.getChildren().add(i));
        labels.stream().forEach(i -> root.getChildren().add(i));
        descript_lines.stream().forEach(i -> root.getChildren().add(i));

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            project_panel();
        });
        
        btn_left.setOnAction(ActionEvent -> {
            draw_diagrams();
        });
        
        btn_right.setOnAction(ActionEvent -> {
            draw_diagrams_3();
        });
    }
    
    private void draw_diagrams_3()
    {
        AnchorPane root = new AnchorPane();

        Button panel = new Button("Демонстрация σₓ эпюр");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Вернуться назад");
        
        Button btn_right = new Button("Nₓ\n\n\n\n\n\n\n\n>");
        btn_right.setWrapText(true);
        btn_right.setAlignment(Pos.TOP_CENTER);
        btn_right.setMaxWidth(50.0);
        btn_right.setMinWidth(50.0);
        Button btn_left = new Button("Uₓ\n\n\n\n\n\n\n\n<");
        btn_left.setWrapText(true);
        btn_left.setAlignment(Pos.TOP_CENTER);
        btn_left.setMaxWidth(50.0);
        btn_left.setMinWidth(50.0);

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(btn_left, -5.0);

        AnchorPane.setBottomAnchor(btn1, 0.0);
        AnchorPane.setBottomAnchor(btn_left, 30.0);
        AnchorPane.setBottomAnchor(btn_right, 30.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        AnchorPane.setRightAnchor(btn_right, -5.0);
        
        AnchorPane.setTopAnchor(btn_left, 40.0);
        AnchorPane.setTopAnchor(btn_right, 40.0);
        

        //Отрисовываем лейбл логов
        Label lbl_log = new Label("Конструкция успешно отрисована!");
        lbl_log.setFont(new Font("Helvetica", 14));
        AnchorPane.setLeftAnchor(lbl_log, 50.0);
        AnchorPane.setBottomAnchor(lbl_log, 50.0);
        Label lbl_descrip1 = new Label("");
        Label lbl_descrip2 = new Label("");
        Label lbl_descrip3 = new Label("");
        Label lbl_descrip4 = new Label("");
        AnchorPane.setLeftAnchor(lbl_descrip1, 80.0);
        AnchorPane.setBottomAnchor(lbl_descrip1, 150.0);
        AnchorPane.setLeftAnchor(lbl_descrip2, 80.0);
        AnchorPane.setBottomAnchor(lbl_descrip2, 100.0);
        AnchorPane.setLeftAnchor(lbl_descrip3, 480.0);
        AnchorPane.setBottomAnchor(lbl_descrip3, 150.0);
        AnchorPane.setLeftAnchor(lbl_descrip4, 480.0);
        AnchorPane.setBottomAnchor(lbl_descrip4, 100.0);
        lbl_descrip1.setFont(new Font("Helvetica", 20));
        lbl_descrip2.setFont(new Font("Helvetica", 20));
        lbl_descrip3.setFont(new Font("Helvetica", 20));
        lbl_descrip4.setFont(new Font("Helvetica", 20));

        ArrayList<Rectangle> rods = new ArrayList<>();
        ArrayList<Line> left_prop = new ArrayList<>();
        ArrayList<Line> right_prop = new ArrayList<>();
        ArrayList<Line> long_forces = new ArrayList<>();
        ArrayList<Line> static_forces = new ArrayList<>();
        ArrayList<Line> extra_lines = new ArrayList<>();
        ArrayList<Label> labels = new ArrayList<>();
        ArrayList<Rectangle> nums_nodes = new ArrayList<>();
        ArrayList<Circle> nums_rods = new ArrayList<>();
        ArrayList<Line> descript_lines = new ArrayList<>();
        
        ArrayList<ArrayList<Double>> sigmax_values = new ArrayList<>();

        
        if (construction.getTableView() != null) {
            for (int i = 0; i < construction.getTableView().size(); i++) {
                //Сначала получаем номер стержня(порядковый)
                int num_rod = i;
                int name_rod = construction.getTableView().get(i).getNum(); 

                //Получаем длину стержня
                double Lp = construction.getTableView().get(num_rod).getL();
                //Сначала получаем длину шага
                double len_step = (double) Lp / (double) 100;
                if (construction.getDeltaVector() != null) 
                {
                    double[] deltaVector = construction.getDeltaVector();
                    //Ищем нужный qp
                    double qp = 0.0;
                    for (int j = 0; j < construction.getDtlfView().size(); j++) {
                        if(construction.getDtlfView().get(j).get_nodeNum() == name_rod)
                        {
                            qp = construction.getDtlfView().get(j).get_forceNum();
                            break;
                        }
                    }
                    //Получаем модуль упругости стержня
                    double Ep = construction.getTableView().get(num_rod).getE();
                    //Получаем площадь поперечного сечения стержня
                    double Ap = construction.getTableView().get(num_rod).getA();
                    
                    ArrayList<Double> sigmax = new ArrayList<>();
                    
                    for (int j = 0; j <= 100; j++) {
                        double x = len_step * j;
                        ArrayList<Double> values 
                                = Processor.calculateDisplacementMethod(deltaVector[num_rod],
                                                                      deltaVector[num_rod+1],
                                                                      x,Lp,qp,Ep,Ap);
                        
                        sigmax.add(values.get(2));
                    }
                    
                    sigmax_values.add(sigmax);
                }
                else {
                    lbl_log.setText("Дельта-вектор не рассчитан!");
                }
                
            }
                 }
                 else {
                     lbl_log.setText("Конструкция не задана! Исправьте");
                 }
        
        
        
        DrawConstructionManager.draw_diagramm_3(construction, lbl_log, rods, left_prop,
                                     right_prop, long_forces, static_forces,
                                     extra_lines,labels,nums_nodes,nums_rods,
                                     descript_lines,lbl_descrip1,lbl_descrip2,
                                     lbl_descrip3,lbl_descrip4,sigmax_values);

        DesignManager.set_draw_diagram(root, btn1, panel,btn_left,btn_right);

        root.getChildren().addAll(panel, btn1, lbl_log,lbl_descrip1,
                                  lbl_descrip2,lbl_descrip3,lbl_descrip4,
                                  btn_left,btn_right);
        extra_lines.stream().forEach(i -> root.getChildren().add(i));
        rods.stream().forEach(i -> root.getChildren().add(i));
        left_prop.stream().forEach(i -> root.getChildren().add(i));
        right_prop.stream().forEach(i -> root.getChildren().add(i));
        long_forces.stream().forEach(i -> root.getChildren().add(i));
        static_forces.stream().forEach(i -> root.getChildren().add(i));
        nums_nodes.stream().forEach(i -> root.getChildren().add(i));
        nums_rods.stream().forEach(i -> root.getChildren().add(i));
        labels.stream().forEach(i -> root.getChildren().add(i));
        descript_lines.stream().forEach(i -> root.getChildren().add(i));

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            project_panel();
        });
        
        btn_left.setOnAction(ActionEvent -> {
            draw_diagrams_2();
        });
        
        btn_right.setOnAction(ActionEvent -> {
            draw_diagrams();
        });
    }

    private void show_acces_exit()
    {
        AnchorPane root = new AnchorPane();

        //Размещаем лого и задаем его расположение
        ImageView logo = new ImageView(im);
        logo.setPreserveRatio(true);
        logo.setFitHeight(100);
        AnchorPane.setTopAnchor(logo, 50.0);
        AnchorPane.setRightAnchor(logo, 10.0);
        
        Button panel = new Button("Подтверждение выхода");
        panel.setMinHeight(40.0);

        Button btn1 = new Button("Нет, я передумал");
        Button btn2 = new Button("Да, выйти в главное меню");

        AnchorPane.setLeftAnchor(panel, -5.0);
        AnchorPane.setLeftAnchor(btn1, -5.0);
        AnchorPane.setLeftAnchor(btn2, -5.0);

        AnchorPane.setBottomAnchor(btn1, 30.0);
        AnchorPane.setBottomAnchor(btn2, 0.0);

        AnchorPane.setRightAnchor(btn1, -5.0);
        AnchorPane.setRightAnchor(btn2, -5.0);
        AnchorPane.setRightAnchor(panel, -5.0);
        
        Label label = new Label("Все несохраненные данные будут удалены. Вы действительно хотите выйти?");
        label.setFont(new Font("Helvetica", 14));
        AnchorPane.setLeftAnchor(label, 50.0);
        AnchorPane.setBottomAnchor(label, 80.0);
        
        DesignManager.set_acces_scene(root, btn1, btn2, panel);

        root.getChildren().addAll(panel, btn1, btn2,label,logo);

        scene.setRoot(root);

        primaryStage.setScene(scene);

        //События для кнопок
        btn1.setOnAction(ActionEvent -> {
            project_panel();
        });

        btn2.setOnAction(ActionEvent -> {
            project_name = "";
            construction = null;
            construction = new Construction();
            create_main_scene();
        });  
    }

}
