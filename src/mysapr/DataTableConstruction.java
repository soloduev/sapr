package mysapr;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Igor Soloduev
 */

public class DataTableConstruction {
    
    //Класс представляет собой структуру таблицы, в которую задаем данные о 
    //стержнях и нагрузках
    
    private IntegerProperty num; //Номер узла, а также позиция стержня
    private DoubleProperty l_num; //Длина i-ого стержня
    private DoubleProperty a_num; //Площадь поперечного сечения стержня
    private DoubleProperty e_num; //Модуль упругости стержня(материал)
    private DoubleProperty b_num; //Допускаемое напряжение стержня

    public DataTableConstruction(int num, double l_num, double a_num,
            double e_num, double b_num) {
        this.num = new SimpleIntegerProperty(num);
        this.l_num = new SimpleDoubleProperty(l_num);
        this.a_num = new SimpleDoubleProperty(a_num);
        this.e_num = new SimpleDoubleProperty(e_num);
        this.b_num = new SimpleDoubleProperty(b_num);
    }
    
    public IntegerProperty numProperty(){return num;}
    public Integer getNum(){return num.getValue();}
    
    public DoubleProperty l_numProperty(){return l_num;}
    public double getL(){return l_num.getValue();}
    
    public DoubleProperty a_numProperty(){return a_num;}
    public double getA(){return a_num.getValue();}
    
    public DoubleProperty e_numProperty(){return e_num;}
    public double getE(){return e_num.getValue();}
    
    public DoubleProperty b_numProperty(){return b_num;}
    public double getB(){return b_num.getValue();}
    
}
