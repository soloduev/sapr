/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysapr;

/**
 *
 * @author Igor Soloduev
 */
public class Rod {
    
    public double l_rod;  //Длина стержня в метрах
    public double a_rod;  //Площадь поперечного сечения
    public double e_rod;  //Модуль упругости
    public double b_rod;  //Допускаемое напряжение
    public double q_rod;  //Статическая погонная нагрузка
}
