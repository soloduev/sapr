package mysapr;

import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;

/**
 *
 * @author Igor Soloduev
 */
public class DesignManager {

    public DesignManager() {
    }
    
    public static void set_main_scene(AnchorPane root,Button btn1,Button btn2,Button btn3,Button btn4,Button panel)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn2.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn3.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn4.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn2.setOpacity(0.7);
        btn3.setOpacity(0.7);
        btn4.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn2.setOnMouseEntered(ActionEvent ->{btn2.setOpacity(1);});
        btn2.setOnMouseExited(ActionEvent ->{btn2.setOpacity(0.7);});
        btn3.setOnMouseEntered(ActionEvent ->{btn3.setOpacity(1);});
        btn3.setOnMouseExited(ActionEvent ->{btn3.setOpacity(0.7);});
        btn4.setOnMouseEntered(ActionEvent ->{btn4.setOpacity(1);});
        btn4.setOnMouseExited(ActionEvent ->{btn4.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
        btn2.setFont(new Font("Helvetica", 14));
        btn3.setFont(new Font("Helvetica", 14));
        btn4.setFont(new Font("Helvetica", 14));
    }
    
    public static void set_info(AnchorPane root,Button btn1,Button panel)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
    }
    
    public static void set_start_programm(AnchorPane root,Button btn1,Button btn2,Button btn3,Button panel)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn2.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn3.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn2.setOpacity(0.7);
        btn3.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn2.setOnMouseEntered(ActionEvent ->{btn2.setOpacity(1);});
        btn2.setOnMouseExited(ActionEvent ->{btn2.setOpacity(0.7);});
        btn3.setOnMouseEntered(ActionEvent ->{btn3.setOpacity(1);});
        btn3.setOnMouseExited(ActionEvent ->{btn3.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
        btn2.setFont(new Font("Helvetica", 14));
        btn3.setFont(new Font("Helvetica", 14));
    }
    
    public static void set_create_project(AnchorPane root,Button btn1,Button btn2,Button text_enter_name,TextField name_task,Button panel)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn2.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        text_enter_name.setStyle("-fx-background-color: #000000;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn2.setOpacity(0.7);
        text_enter_name.setOpacity(0.5);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn2.setOnMouseEntered(ActionEvent ->{btn2.setOpacity(1);});
        btn2.setOnMouseExited(ActionEvent ->{btn2.setOpacity(0.7);});
        name_task.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        name_task.setOpacity(0.7);
        btn1.setFont(new Font("Helvetica", 14));
        btn2.setFont(new Font("Helvetica", 14));
        text_enter_name.setFont(new Font("Helvetica", 14));
    }

    public static void set_panel_project(AnchorPane root,Button panel,Button project_name,
            Button preprocessor,Button btn_enter_data,Button btn_show_constuction,
            Button processor,Button postprocessor,Button btn_exit,
            Button btn_calculate,
            Button btn_show_result_in_table,
            Button btn_show_result_in_epure)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        project_name.setFont(Font.font(16.0));
        preprocessor.setFont(Font.font(14.0));
        processor.setFont(Font.font(14.0));
        postprocessor.setFont(Font.font(14.0));
        btn_enter_data.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_show_constuction.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_calculate.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_show_result_in_table.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_show_result_in_epure.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_exit.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        project_name.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        preprocessor.setStyle("-fx-background-color: #000000;-fx-text-fill: #ffffff;");
        processor.setStyle("-fx-background-color: #000000;-fx-text-fill: #ffffff;");
        postprocessor.setStyle("-fx-background-color: #000000;-fx-text-fill: #ffffff;");
        preprocessor.setOpacity(0.5);
        processor.setOpacity(0.5);
        postprocessor.setOpacity(0.5);
        btn_enter_data.setOpacity(0.7);
        btn_show_constuction.setOpacity(0.7);
        btn_calculate.setOpacity(0.7);
        btn_show_result_in_table.setOpacity(0.7);
        btn_show_result_in_epure.setOpacity(0.7);
        btn_exit.setOpacity(0.7);
        btn_enter_data.setOnMouseEntered(ActionEvent ->{btn_enter_data.setOpacity(1);});
        btn_enter_data.setOnMouseExited(ActionEvent ->{btn_enter_data.setOpacity(0.7);});
        btn_show_constuction.setOnMouseEntered(ActionEvent ->{btn_show_constuction.setOpacity(1);});
        btn_show_constuction.setOnMouseExited(ActionEvent ->{btn_show_constuction.setOpacity(0.7);});
        btn_calculate.setOnMouseEntered(ActionEvent ->{btn_calculate.setOpacity(1);});
        btn_calculate.setOnMouseExited(ActionEvent ->{btn_calculate.setOpacity(0.7);});
        btn_show_result_in_table.setOnMouseEntered(ActionEvent ->{btn_show_result_in_table.setOpacity(1);});
        btn_show_result_in_table.setOnMouseExited(ActionEvent ->{btn_show_result_in_table.setOpacity(0.7);});
        btn_show_result_in_epure.setOnMouseEntered(ActionEvent ->{btn_show_result_in_epure.setOpacity(1);});
        btn_show_result_in_epure.setOnMouseExited(ActionEvent ->{btn_show_result_in_epure.setOpacity(0.7);});
        btn_exit.setOnMouseEntered(ActionEvent ->{btn_exit.setOpacity(1);});
        btn_exit.setOnMouseExited(ActionEvent ->{btn_exit.setOpacity(0.7);});
        preprocessor.setFont(new Font("Helvetica", 14));
        processor.setFont(new Font("Helvetica", 14));
        postprocessor.setFont(new Font("Helvetica", 14));
        btn_enter_data.setFont(new Font("Helvetica", 14));
        btn_show_constuction.setFont(new Font("Helvetica", 14));
        btn_calculate.setFont(new Font("Helvetica", 14));
        btn_exit.setFont(new Font("Helvetica", 14));
        btn_show_result_in_table.setFont(new Font("Helvetica", 14));
        btn_show_result_in_epure.setFont(new Font("Helvetica", 14));
    }

    public static void set_create_construction(AnchorPane root, Button panel,
            TableView tableView,Button btn_show_rod,Button btn_return,
            Button constuction,Button loads,Button btn_add_rod, 
            Button btn_delete_rod,Button btn_up_rod,Button btn_down_rod,
            TextField input_num,TextField input_L,TextField input_A,
            TextField input_E,TextField input_B,TableView dtlpView,
            TableView dtlfView,TableView dtsllView,
            Button btn_clear_all,TextField input_prop,
            TextField input_q_i,TextField input_node1,TextField input_f_i,
            TextField input_node2,Button btn_add_prop,Button btn_remove_prop,
            Button btn_add_node1,Button btn_remove_node1,Button btn_add_node2,
            Button btn_remove_node2)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        tableView.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_show_rod.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_return.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_show_rod.setOpacity(0.7);
        btn_return.setOpacity(0.7);
        btn_show_rod.setFont(new Font("Helvetica", 14));
        btn_return.setFont(new Font("Helvetica", 14));
        btn_show_rod.setOnMouseEntered(ActionEvent ->{btn_show_rod.setOpacity(1);});
        btn_show_rod.setOnMouseExited(ActionEvent ->{btn_show_rod.setOpacity(0.7);});
        btn_return.setOnMouseEntered(ActionEvent ->{btn_return.setOpacity(1);});
        btn_return.setOnMouseExited(ActionEvent ->{btn_return.setOpacity(0.7);});
        constuction.setFont(Font.font(14.0));
        loads.setFont(Font.font(14.0));
        constuction.setStyle("-fx-background-color: #000000;-fx-text-fill: #ffffff;");
        loads.setStyle("-fx-background-color: #000000;-fx-text-fill: #ffffff;");
        constuction.setFont(new Font("Helvetica", 14));
        loads.setFont(new Font("Helvetica", 14));
        constuction.setOpacity(0.5);
        loads.setOpacity(0.5);
        
        btn_add_rod.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_add_rod.setOpacity(0.7);
        btn_add_rod.setFont(new Font("Helvetica", 14));
        btn_add_rod.setOnMouseEntered(ActionEvent ->{btn_add_rod.setOpacity(1);});
        btn_add_rod.setOnMouseExited(ActionEvent ->{btn_add_rod.setOpacity(0.7);});
        
        btn_delete_rod.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_delete_rod.setOpacity(0.7);
        btn_delete_rod.setFont(new Font("Helvetica", 14));
        btn_delete_rod.setOnMouseEntered(ActionEvent ->{btn_delete_rod.setOpacity(1);});
        btn_delete_rod.setOnMouseExited(ActionEvent ->{btn_delete_rod.setOpacity(0.7);});
        
        btn_up_rod.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_up_rod.setOpacity(0.7);
        btn_up_rod.setFont(new Font("Helvetica", 14));
        btn_up_rod.setOnMouseEntered(ActionEvent ->{btn_up_rod.setOpacity(1);});
        btn_up_rod.setOnMouseExited(ActionEvent ->{btn_up_rod.setOpacity(0.7);});
        
        btn_down_rod.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_down_rod.setOpacity(0.7);
        btn_down_rod.setFont(new Font("Helvetica", 14));
        btn_down_rod.setOnMouseEntered(ActionEvent ->{btn_down_rod.setOpacity(1);});
        btn_down_rod.setOnMouseExited(ActionEvent ->{btn_down_rod.setOpacity(0.7);});
        
        btn_add_prop.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_add_prop.setOpacity(0.7);
        btn_add_prop.setFont(new Font("Helvetica", 14));
        btn_add_prop.setOnMouseEntered(ActionEvent ->{btn_add_prop.setOpacity(1);});
        btn_add_prop.setOnMouseExited(ActionEvent ->{btn_add_prop.setOpacity(0.7);});
        
        btn_remove_prop.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_remove_prop.setOpacity(0.7);
        btn_remove_prop.setFont(new Font("Helvetica", 14));
        btn_remove_prop.setOnMouseEntered(ActionEvent ->{btn_remove_prop.setOpacity(1);});
        btn_remove_prop.setOnMouseExited(ActionEvent ->{btn_remove_prop.setOpacity(0.7);});
        
        btn_add_node1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_add_node1.setOpacity(0.7);
        btn_add_node1.setFont(new Font("Helvetica", 14));
        btn_add_node1.setOnMouseEntered(ActionEvent ->{btn_add_node1.setOpacity(1);});
        btn_add_node1.setOnMouseExited(ActionEvent ->{btn_add_node1.setOpacity(0.7);});
        
        btn_remove_node1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_remove_node1.setOpacity(0.7);
        btn_remove_node1.setFont(new Font("Helvetica", 14));
        btn_remove_node1.setOnMouseEntered(ActionEvent ->{btn_remove_node1.setOpacity(1);});
        btn_remove_node1.setOnMouseExited(ActionEvent ->{btn_remove_node1.setOpacity(0.7);});
        
        btn_add_node2.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_add_node2.setOpacity(0.7);
        btn_add_node2.setFont(new Font("Helvetica", 14));
        btn_add_node2.setOnMouseEntered(ActionEvent ->{btn_add_node2.setOpacity(1);});
        btn_add_node2.setOnMouseExited(ActionEvent ->{btn_add_node2.setOpacity(0.7);});
        
        btn_remove_node2.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_remove_node2.setOpacity(0.7);
        btn_remove_node2.setFont(new Font("Helvetica", 14));
        btn_remove_node2.setOnMouseEntered(ActionEvent ->{btn_remove_node2.setOpacity(1);});
        btn_remove_node2.setOnMouseExited(ActionEvent ->{btn_remove_node2.setOpacity(0.7);});
        
        input_num.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_num.setOpacity(0.7);
        input_num.setFont(new Font("Helvetica", 14));
        
        input_L.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_L.setOpacity(0.7);
        input_L.setFont(new Font("Helvetica", 14));
        
        input_A.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_A.setOpacity(0.7);
        input_A.setFont(new Font("Helvetica", 14));
        
        input_E.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_E.setOpacity(0.7);
        input_E.setFont(new Font("Helvetica", 14));
        
        input_B.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_B.setOpacity(0.7);
        input_B.setFont(new Font("Helvetica", 14));
        
        input_prop.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_prop.setOpacity(0.7);
        input_prop.setFont(new Font("Helvetica", 14));
        
        input_q_i.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_q_i.setOpacity(0.7);
        input_q_i.setFont(new Font("Helvetica", 14));
        
        input_node1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_node1.setOpacity(0.7);
        input_node1.setFont(new Font("Helvetica", 14));
        
        input_f_i.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_f_i.setOpacity(0.7);
        input_f_i.setFont(new Font("Helvetica", 14));
        
        input_node2.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        input_node2.setOpacity(0.7);
        input_node2.setFont(new Font("Helvetica", 14));
        
        dtlpView.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        dtlfView.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        dtsllView.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        
        btn_clear_all.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn_clear_all.setOpacity(0.7);
        btn_clear_all.setFont(new Font("Helvetica", 14));
        btn_clear_all.setOnMouseEntered(ActionEvent ->{btn_clear_all.setOpacity(1);});
        btn_clear_all.setOnMouseExited(ActionEvent ->{btn_clear_all.setOpacity(0.7);});
        
    }

    
    public static void set_journal(AnchorPane root,Button btn1,Button btn2,Button btn3,Button panel)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn2.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn3.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn2.setOpacity(0.7);
        btn3.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn2.setOnMouseEntered(ActionEvent ->{btn2.setOpacity(1);});
        btn2.setOnMouseExited(ActionEvent ->{btn2.setOpacity(0.7);});
        btn3.setOnMouseEntered(ActionEvent ->{btn3.setOpacity(1);});
        btn3.setOnMouseExited(ActionEvent ->{btn3.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
        btn2.setFont(new Font("Helvetica", 14));
        btn3.setFont(new Font("Helvetica", 14));
    }

    public static void set_acces_scene(AnchorPane root,Button btn1,Button btn2,Button panel)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        btn2.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn2.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn2.setOnMouseEntered(ActionEvent ->{btn2.setOpacity(1);});
        btn2.setOnMouseExited(ActionEvent ->{btn2.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
        btn2.setFont(new Font("Helvetica", 14));
    }
    
    public static void set_draw_construction(AnchorPane root,Button btn1,Button panel)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
    }
    
    public static void set_draw_diagram(AnchorPane root,Button btn1,Button panel, Button left, Button right)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
        left.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        left.setOpacity(0.7);
        left.setOnMouseEntered(ActionEvent ->{left.setOpacity(1);});
        left.setOnMouseExited(ActionEvent ->{left.setOpacity(0.7);});
        left.setFont(new Font("Helvetica", 20));
        right.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        right.setOpacity(0.7);
        right.setOnMouseEntered(ActionEvent ->{right.setOpacity(1);});
        right.setOnMouseExited(ActionEvent ->{right.setOpacity(0.7);});
        right.setFont(new Font("Helvetica", 20));
    }
    
    public static void set_calculate_construction(AnchorPane root,Button btn1,Button panel)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
    }

    public static void set_result_table(AnchorPane root,Button btn1,Button panel,TableView tableView)
    {
        root.setStyle("-fx-background-color: #93afee;");
        panel.setFont(Font.font(16.0));
        btn1.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
        panel.setStyle("-fx-background-color: #9e1b1b;-fx-text-fill: #ffffff;");
        btn1.setOpacity(0.7);
        btn1.setOnMouseEntered(ActionEvent ->{btn1.setOpacity(1);});
        btn1.setOnMouseExited(ActionEvent ->{btn1.setOpacity(0.7);});
        btn1.setFont(new Font("Helvetica", 14));
        tableView.setStyle("-fx-background-color: #4d6cb1;-fx-text-fill: #ffffff;");
    }
}
