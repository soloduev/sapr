/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysapr;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Igor
 */
public class DataSerialize implements Serializable{
    public ArrayList<ArrayList<Double>> tableView;
    public ArrayList<ArrayList<Integer>> tableLoads;
    public ArrayList<ArrayList<Double>> tableLongForces;
    public ArrayList<ArrayList<Double>> tableForces;

    public DataSerialize() {
        tableView = new ArrayList<>();
        tableLoads = new ArrayList<>();
        tableLongForces = new ArrayList<>();
        tableForces = new ArrayList<>();
    }
}
