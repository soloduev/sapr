/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysapr;

import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

/**
 *
 * @author Igor
 */
public class DrawConstructionManager {

    public static void draw(Construction construction,
                            Label lbl_log,
                            ArrayList<Rectangle> rods,
                            ArrayList<Line> left_prop,
                            ArrayList<Line> right_prop,
                            ArrayList<Line> long_forces,
                            ArrayList<Line> static_forces,
                            ArrayList<Line> extra_lines,
                            ArrayList<Label> labels,
                            ArrayList<Rectangle> nums_nodes,
                            ArrayList<Circle> nums_rods,
                            ArrayList<Line> descript_lines,
                            Label lbl_descrip1,
                            Label lbl_descrip2,
                            Label lbl_descrip3,
                            Label lbl_descrip4) {
        //Здесь отрисовываем конструкцию

        //Сначала определяем пропорцию длины каждого стержня
        //720 -изначальная длина конструкции
        int num_props = 0; //Количество заделок
        int num_long_forces = 0; //Количество погонных нагрузок
        int num_static_forces = 0; //Количество погонных нагрузок
        Double global_len_i = 0.0;
        Double global_h_i = 0.0;

        if (construction != null) {
            if (construction.getTableView() != null) {
                //Определяем количество стержней
                Integer num_rods = construction.getTableView().size();
                if (num_rods > 0) {
                    //Далее, проходим по порядку все стержни, и в зависимости от их
                    //Длины, определяем их длину в соотношении друг с другом
                    //Сначала определяем длину всех стержней
                    Double sum_length = 0.0;
                    for (int i = 0; i < num_rods; i++) {
                        sum_length += construction.getTableView().get(i).getL();
                    }
                    //Определяем единичную длину
                    Double len_identity = 720.0 / sum_length;

                    //Теперь высоту
                    Double sum_h = 0.0;
                    for (int i = 0; i < num_rods; i++) {
                        sum_h += construction.getTableView().get(i).getA();
                    }
                    //Определяем единичную высоту
                    Double h_identity = 200.0 / sum_h;

                    Double draw_position_x = 80.0; //Позиция отрисовки по x;
                    Double draw_position_y = 200.0; //Позиция отрисовки по y;
                    //Теперь отрисовываем в правильном соотношении линии
                    for (int i = 0; i < num_rods; i++) {
                        //Получаем номер стержня
                        Integer id_rod = construction.getTableView().get(i).getNum();
                        //Далее, отрисовываем стержни
                        //получаем настоящую длину стержня
                        double real_l = construction.getTableView().get(i).getL();
                        //Получаем площадь попер. сечения
                        double real_a = construction.getTableView().get(i).getA();                  
                        //Длина i-ого стержня
                        Double len_i = len_identity * real_l;
                        global_len_i = len_i;
                        //Высота i-ого стержня
                        Double h_i = h_identity * real_a;
                        global_h_i = h_i;

                        Rectangle rectangle = new Rectangle(draw_position_x, draw_position_y - (h_i / 2),
                                                            len_i, h_i);

                        rectangle.setFill(Color.KHAKI);
                        rectangle.setStroke(Color.BLACK);
                        
                        
                        ArrayList<Double> force_q = new ArrayList<>();
                        force_q.add(0.0);
                        for (int j = 0; j < construction.getDtlfView().size(); j++) {
                            if(construction.getDtlfView().get(j).get_nodeNum() == id_rod)
                            {
                                force_q.set(0, construction.getDtlfView().get(j).get_forceNum());
                                break;
                            }
                        }
                        
                        rectangle.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            rectangle.setFill(Color.AQUA);
                            lbl_descrip1.setText("N:  " + id_rod);
                            lbl_descrip2.setText("L:  " + real_l);
                            lbl_descrip3.setText("A:  " + real_a);
                            lbl_descrip4.setText("q_i:  " + force_q.get(0));
                        });
                        
                        rectangle.setOnMouseExited(ActionEvent -> {
                            lbl_descrip1.setText("");
                            lbl_descrip2.setText("");
                            lbl_descrip3.setText("");
                            lbl_descrip4.setText("");
                            rectangle.setFill(Color.KHAKI);
                        });
                        rods.add(rectangle);

                        //Теперь отрисовываем погонные нагрузки в этом стержне
                        //Отрисовываем погонные нагрузки
                        num_long_forces = construction.getDtlfView().size();
                        for (int j = 0; j < num_long_forces; j++) {
                            //Получаем номер стержня, в котором приложена сила
                            Integer node = construction.getDtlfView().get(j).get_nodeNum();
                            //Получаем значение силы в данном стержне
                            Double force = construction.getDtlfView().get(j).get_forceNum();
                            //Теперь, если номера стержней совпадают, прорисовываем в нем нагрузку
                            if (node == construction.getTableView().get(i).getNum()) {
                                Line line = new Line(draw_position_x, draw_position_y,
                                                     draw_position_x + len_i, draw_position_y);
                                line.setStroke(Color.BROWN);
                                //Проверяем знак нагрузки
                                if (force > 0) {
                                    //Рисуем стрелки вправо
                                    for (int k = (int) (double) draw_position_x;
                                         k < (int) (double) (draw_position_x + len_i); k += 8) {
                                        Line line1 = new Line(k, draw_position_y - 5, k + 5, draw_position_y);
                                        line1.setStroke(Color.BROWN);
                                        line1.setStrokeWidth(0.5);
                                        Line line2 = new Line(k, draw_position_y + 5, k + 5, draw_position_y);
                                        line2.setStroke(Color.BROWN);
                                        line2.setStrokeWidth(0.5);
                                        long_forces.add(line1);
                                        long_forces.add(line2);
                                    }
                                }
                                else {
                                    //Рисуем стрелки влево
                                    for (int k = (int) (double) draw_position_x;
                                         k < (int) (double) (draw_position_x + len_i); k += 8) {
                                        Line line1 = new Line(k + 5, draw_position_y - 5, k, draw_position_y);
                                        line1.setStroke(Color.BROWN);
                                        line1.setStrokeWidth(0.5);
                                        Line line2 = new Line(k + 5, draw_position_y + 5, k, draw_position_y);
                                        line2.setStroke(Color.BROWN);
                                        line2.setStrokeWidth(0.5);
                                        long_forces.add(line1);
                                        long_forces.add(line2);
                                    }
                                }
                                long_forces.add(line);
                            }
                        }

                        //Теперь отрисовываем статистические усилия
                        num_static_forces = construction.getDtsllView().size();
                        //Индексы узлов, в которых отрисованы силы
                        ArrayList<Integer> indexes_draw = new ArrayList<>();
                        for (int j = 0; j < num_static_forces; j++) {
                            //Получаем номер узла, в котором приложена сила
                            Integer node = construction.getDtsllView().get(j).get_num_rodNum();
                            //Получаем значение силы в данном узле
                            Double force = construction.getDtsllView().get(j).get_sllNum();
                            //Проверяем, не отрисовали ли в этом узле силу

                            long count = indexes_draw.stream().filter(elem -> elem == node).count();

                            if (node == i + 1 && count == 0) {
                                //Рисуем стрелку для левой части стержня
                                //Проверяем знак силы
                                if (force > 0) {
                                    //Рисуем стрелку слева стержня, внутри, направленную вправо
                                    Line line1 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + (len_i / 3), draw_position_y);
                                    Line line2 = new Line(draw_position_x + (len_i / 3), draw_position_y,
                                                          draw_position_x + (len_i / 3) - 10, draw_position_y - 5);
                                    Line line3 = new Line(draw_position_x + (len_i / 3), draw_position_y,
                                                          draw_position_x + (len_i / 3) - 10, draw_position_y + 5);
                                    line1.setStrokeWidth(4.0);
                                    line1.setStroke(Color.BROWN);
                                    line2.setStrokeWidth(4.0);
                                    line2.setStroke(Color.BROWN);
                                    line3.setStrokeWidth(4.0);
                                    line3.setStroke(Color.BROWN);
                                    //Если стрелка помещается, то рисуем
                                    if(h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    indexes_draw.add(node);
                                    
                                    //Отрисовываем лейбл статистического усилия
                                    Label label_f = new Label(construction.getDtsllView().get(j).get_sllNum() + "F");
                                    label_f.setLayoutX((draw_position_x+len_i/3)+10.0);
                                    label_f.setLayoutY(draw_position_y-9.0);
                                    label_f.setFont(new Font("Consolas", 15));
                                    if(len_i>100.0)
                                        labels.add(label_f);
                                    else 
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    
                                }
                                else {
                                    //Рисуем стрелку слева стержня, внутри, направленную влево
                                    Line line1 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + (len_i / 3), draw_position_y);
                                    Line line2 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + 3 + 10, draw_position_y - 5);
                                    Line line3 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + 3 + 10, draw_position_y + 5);
                                    line1.setStrokeWidth(4.0);
                                    line1.setStroke(Color.BROWN);
                                    line2.setStrokeWidth(4.0);
                                    line2.setStroke(Color.BROWN);
                                    line3.setStrokeWidth(4.0);
                                    line3.setStroke(Color.BROWN);
                                    //Если стрелка помещается, то рисуем
                                    if(h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    
                                    indexes_draw.add(node);
                                }
                            }
                        }
                        
                        //Теперь отрисовываем вспомогательные линии,
                        //номера стержней и номера узлов
                        //Отрисовываем линию в конце каждого стержня
                        Line end_line = new Line(draw_position_x+len_i, 200.0, draw_position_x+len_i, 400.0);
                        end_line.setStroke(Color.BLACK);
                        end_line.setStrokeWidth(0.5);
                        end_line.getStrokeDashArray().addAll(5.0,10.0);
                        extra_lines.add(end_line);
                        
                        //Отрисовывем стрелки под каждыйм стержнем
                        //Сама линия
                        Line down_line = new Line(draw_position_x, 400.0, draw_position_x+len_i, 400.0);
                        down_line.setStroke(Color.BLACK);
                        down_line.setStrokeWidth(0.5);
                        extra_lines.add(down_line);
                        //Левая стрелка
                        Line left_line1 = new Line(draw_position_x, 400.0, draw_position_x+10, 397.0);
                        left_line1.setStroke(Color.BLACK);
                        left_line1.setStrokeWidth(0.5);
                        extra_lines.add(left_line1);
                        Line left_line2 = new Line(draw_position_x, 400.0, draw_position_x+10, 403.0);
                        left_line2.setStroke(Color.BLACK);
                        left_line2.setStrokeWidth(0.5);
                        extra_lines.add(left_line2);
                        //Правая стрелка
                        Line right_line1 = new Line(draw_position_x+len_i, 400.0, draw_position_x+len_i-10, 397.0);
                        right_line1.setStroke(Color.BLACK);
                        right_line1.setStrokeWidth(0.5);
                        extra_lines.add(right_line1);
                        Line right_line2 = new Line(draw_position_x+len_i, 400.0, draw_position_x+len_i-10, 403.0);
                        right_line2.setStroke(Color.BLACK);
                        right_line2.setStrokeWidth(0.5);
                        extra_lines.add(right_line2);
                        
                        //Теперь отрисовываем лейбл L для каждого стержня
                        if(len_i>=60.0)
                        {
                            Label label_l = new Label(construction.getTableView().get(i).getL() + "L");
                            label_l.setLayoutX((draw_position_x+len_i/2)-15.0);
                            label_l.setLayoutY(383.0);
                            label_l.setFont(new Font("Consolas", 15));
                            labels.add(label_l);
                        }
                        
                        if(len_i<60.0)
                        {
                            Label label_l = new Label(construction.getTableView().get(i).getL() + "L");
                            label_l.setLayoutX((draw_position_x+len_i/2)-15.0);
                            label_l.setLayoutY(383.0);
                            label_l.setFont(new Font("Consolas", 12));
                            labels.add(label_l);
                        }
                        
                        if(len_i<30.0)
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        
                        //Если длина стержня больше 35, то отрисовываем номер узла
                        if(len_i>30.0)
                        {
                            //Отрисовывем прямоугольники для узлов
                            Rectangle num_node = new Rectangle(draw_position_x+len_i-10.0, 340.0, 20.0, 20.0);
                            num_node.setFill(Color.LAVENDER);
                            num_node.setStroke(Color.BLACK);
                            nums_nodes.add(num_node);
                            //Отрисовываем номер узла
                            Label label_num_node = new Label(""+(i+2));
                            if(i+2>9)
                                label_num_node.setLayoutX(draw_position_x+len_i-8.0);
                            else
                                label_num_node.setLayoutX(draw_position_x+len_i-4.0);
                            label_num_node.setLayoutY(342.0);
                            label_num_node.setFont(new Font("Consolas", 15));
                            labels.add(label_num_node);
                        }
                        else
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        
                        
                        
                        
                        //Отрисовываем круги для номеров стержней
                        Circle num_rod = new Circle(draw_position_x + (len_i / 2), 75.0, 12.0);
                        num_rod.setFill(Color.LAVENDER);
                        num_rod.setStroke(Color.BLACK);
                        
                        //Отрисовываем линию от круга к стержню
                        Line line_to_rod = new Line(draw_position_x + (len_i / 2),
                                75.0, draw_position_x + (len_i / 2), draw_position_y);
                        line_to_rod.setStroke(Color.BLACK);
                        line_to_rod.setStrokeWidth(0.5);
                        line_to_rod.getStrokeDashArray().addAll(5.0,10.0);
                        line_to_rod.setOpacity(0.0);
                        extra_lines.add(line_to_rod);
                        
                        num_rod.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            num_rod.setFill(Color.AQUA);
                            lbl_descrip1.setText("N:  " + id_rod);
                            lbl_descrip2.setText("L:  " + real_l);
                            lbl_descrip3.setText("A:  " + real_a);
                            lbl_descrip4.setText("q_i:  " + force_q.get(0));
                            line_to_rod.setOpacity(1.0);
                        });
                        
                        num_rod.setOnMouseExited(ActionEvent -> {
                            lbl_descrip1.setText("");
                            lbl_descrip2.setText("");
                            lbl_descrip3.setText("");
                            lbl_descrip4.setText("");
                            num_rod.setFill(Color.LAVENDER);
                            line_to_rod.setOpacity(0.0);
                        });
                        nums_rods.add(num_rod);
                        //Отрисовываем номер узла
                        Label label_num_rod = new Label("" + (construction.getTableView().get(i).getNum()));
                        if (construction.getTableView().get(i).getNum() > 9) {
                            label_num_rod.setLayoutX(draw_position_x + (len_i / 2) - 6.0);
                        }
                        else {
                            label_num_rod.setLayoutX(draw_position_x + (len_i / 2) - 3.0);
                        }
                        label_num_rod.setLayoutY(68.0);
                        label_num_rod.setFont(new Font("Consolas", 10));
                        label_num_rod.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            num_rod.setFill(Color.AQUA);
                            lbl_descrip1.setText("N:  " + id_rod);
                            lbl_descrip2.setText("L:  " + real_l);
                            lbl_descrip3.setText("A:  " + real_a);
                            lbl_descrip4.setText("q_i:  " + force_q.get(0));
                            line_to_rod.setOpacity(1.0);
                        });
                        
                        label_num_rod.setOnMouseExited(ActionEvent -> {
                            lbl_descrip1.setText("");
                            lbl_descrip2.setText("");
                            lbl_descrip3.setText("");
                            lbl_descrip4.setText("");
                            num_rod.setFill(Color.LAVENDER);
                            line_to_rod.setOpacity(0.0);
                        });
                        labels.add(label_num_rod);
                        
                        
                        
                        
                        
                        //Если длина стержня больше 80, то отрисовываем дескритор стержня
                        if(len_i>60.0)
                        {
                            //Отрисовываем линии для каждого из стержней
                            Line descript_line1 = new Line(draw_position_x+(len_i/5),
                                    draw_position_y-(h_i/3), draw_position_x+(len_i/4),
                                    draw_position_y-(h_i/2)-10.0);
                            Line descript_line2 = new Line(draw_position_x+(len_i/4),
                                    draw_position_y-(h_i/2)-10.0, draw_position_x+(len_i/4)+len_i/3,
                                    draw_position_y-(h_i/2)-10.0);
                            descript_line1.setStroke(Color.BLACK);
                            descript_line1.setStrokeWidth(0.5);
                            descript_line2.setStroke(Color.BLACK);
                            descript_line2.setStrokeWidth(0.5);
                            Circle descript_circle = new Circle(draw_position_x+(len_i/5), draw_position_y-(h_i/3), 2.0);
                            descript_circle.setFill(Color.BLACK);
                            descript_lines.add(descript_line1);
                            descript_lines.add(descript_line2);
                            nums_rods.add(descript_circle);
                            //Теперь отрисовываем показатели каждого из стержней
                            Label descript_label = new Label("E, "+construction.getTableView().get(i).getA()+"A");
                            descript_label.setLayoutX(draw_position_x+(len_i/4)+3.0);
                            descript_label.setLayoutY(draw_position_y-(h_i/2)-25.0);
                            descript_label.setFont(new Font("Consolas", 12));
                            labels.add(descript_label);
                            
                        }
                        else
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        draw_position_x += len_i;
                    }
                        //Отрисовываем первую линию
                        Line first_line = new Line(80.0, 200.0, 80.0, 400.0);
                        first_line.setStroke(Color.BLACK);
                        first_line.setStrokeWidth(0.5);
                        first_line.getStrokeDashArray().addAll(5.0,10.0);
                        extra_lines.add(first_line);
                        
                        //Отрисовываем прямоугольник первого узла
                        Rectangle num_node = new Rectangle(70.0, 340.0, 20.0, 20.0);
                        num_node.setFill(Color.LAVENDER);
                        num_node.setStroke(Color.BLACK);
                        nums_nodes.add(num_node);
                        //Отрисовываем номер первого узла
                        Label label_num_node = new Label(""+1);
                        label_num_node.setLayoutX(76.0);
                        label_num_node.setLayoutY(342.0);
                        label_num_node.setFont(new Font("Consolas", 15));
                        labels.add(label_num_node);


                    //Теперь отдельно для последнего узла, прорисовываем статическое усилие
                    for (int j = 0; j < num_static_forces; j++) {
                        //Получаем номер узла, в котором приложена сила
                        Integer node = construction.getDtsllView().get(j).get_num_rodNum();
                        //Получаем значение силы в данном узле
                        Double force = construction.getDtsllView().get(j).get_sllNum();

                        if (node == construction.getTableView().size() + 1) {
                                //Рисуем стрелку для правой части стержня
                            //Проверяем знак силы
                            if (force > 0) {
                                //Рисуем стрелку справа стержня, внутри, направленную вправо
                                Line line1 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - (global_len_i / 3), draw_position_y);
                                Line line2 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - 3 - 10, draw_position_y - 5);
                                Line line3 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - 3 - 10, draw_position_y + 5);
                                line1.setStrokeWidth(4.0);
                                line1.setStroke(Color.BROWN);
                                line2.setStrokeWidth(4.0);
                                line2.setStroke(Color.BROWN);
                                line3.setStrokeWidth(4.0);
                                line3.setStroke(Color.BROWN);
                                //Если стрелка помещается, то рисуем
                                    if(global_h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                            }
                            else {
                                //Рисуем стрелку справа стержня, внутри, направленную влево
                                Line line1 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - (global_len_i / 3), draw_position_y);
                                Line line2 = new Line(800.0 - (global_len_i / 3), draw_position_y,
                                                      800.0 - (global_len_i / 3) + 10, draw_position_y - 5);
                                Line line3 = new Line(800.0 - (global_len_i / 3), draw_position_y,
                                                      800.0 - (global_len_i / 3) + 10, draw_position_y + 5);
                                line1.setStrokeWidth(4.0);
                                line1.setStroke(Color.BROWN);
                                line2.setStrokeWidth(4.0);
                                line2.setStroke(Color.BROWN);
                                line3.setStrokeWidth(4.0);
                                line3.setStroke(Color.BROWN);
                                //Если стрелка помещается, то рисуем
                                    if(global_h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                            }
                        }
                    }

                    //Отрисовываем заделки
                    num_props = construction.getDtlpView().size();
                    if (num_props == 0) {
                        lbl_log.setText("ВНИМАНИЕ! Конструкция не закреплена! Исправьте!");
                    }
                    else {
                        if (num_props == 1) {
                            if (construction.getDtlpView().get(0).get_nodeNum() == 1) {
                                //Отрисовываем левую заделку
                                Line line1 = new Line(80, 150.0, 80.0, 250.0);
                                line1.setStrokeWidth(3.0);
                                line1.setStroke(Color.BROWN);
                                //Теперь штрихи заделки
                                for (int i = 150; i <= 250; i += 10) {
                                    Line line_str = new Line(70, i + 5, 80.0, i);
                                    line_str.setStroke(Color.BROWN);
                                    left_prop.add(line_str);
                                }
                                left_prop.add(line1);
                            }
                            else {
                                //Отрисовываем правую заделку
                                Line line1 = new Line(800.0, 150.0, 800.0, 250.0);
                                line1.setStrokeWidth(3.0);
                                line1.setStroke(Color.BROWN);
                                //Теперь штрихи заделки
                                for (int i = 150; i <= 250; i += 10) {
                                    Line line_str = new Line(810.0, i - 5, 800.0, i);
                                    line_str.setStroke(Color.BROWN);
                                    right_prop.add(line_str);
                                }
                                right_prop.add(line1);
                            }
                        }
                        else {

                            //Отрисовываем левую заделку
                            Line line1 = new Line(80, 150.0, 80.0, 250.0);
                            line1.setStrokeWidth(3.0);
                            line1.setStroke(Color.BROWN);
                            left_prop.add(line1);
                            //Теперь штрихи заделки
                            for (int i = 150; i <= 250; i += 10) {
                                Line line_str = new Line(70, i + 5, 80.0, i);
                                line_str.setStroke(Color.BROWN);
                                left_prop.add(line_str);
                            }

                            //Отрисовываем правую заделку
                            Line line2 = new Line(800.0, 150.0, 800.0, 250.0);
                            line2.setStrokeWidth(3.0);
                            line2.setStroke(Color.BROWN);
                            //Теперь штрихи заделки
                            for (int i = 150; i <= 250; i += 10) {
                                Line line_str = new Line(810.0, i - 5, 800.0, i);
                                line_str.setStroke(Color.BROWN);
                                right_prop.add(line_str);
                            }
                            right_prop.add(line2);
                        }
                    }
                }
                else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
            }else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
        }else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
    }


    public static void draw_diagramm_1(Construction construction,
                            Label lbl_log,
                            ArrayList<Rectangle> rods,
                            ArrayList<Line> left_prop,
                            ArrayList<Line> right_prop,
                            ArrayList<Line> long_forces,
                            ArrayList<Line> static_forces,
                            ArrayList<Line> extra_lines,
                            ArrayList<Label> labels,
                            ArrayList<Rectangle> nums_nodes,
                            ArrayList<Circle> nums_rods,
                            ArrayList<Line> descript_lines,
                            Label lbl_descrip1,
                            Label lbl_descrip2,
                            Label lbl_descrip3,
                            Label lbl_descrip4,
                            ArrayList<ArrayList<Double>> nx_values)
    {
         //Здесь отрисовываем конструкцию с эпюрой 1

        //Сначала определяем пропорцию длины каждого стержня
        //720 -изначальная длина конструкции
        int num_props = 0; //Количество заделок
        int num_long_forces = 0; //Количество погонных нагрузок
        int num_static_forces = 0; //Количество погонных нагрузок
        Double global_len_i = 0.0;
        Double global_h_i = 0.0;

        if (construction != null) {
            if (construction.getTableView() != null) {
                //Определяем количество стержней
                Integer num_rods = construction.getTableView().size();
                if (num_rods > 0) {
                    //Далее, проходим по порядку все стержни, и в зависимости от их
                    //Длины, определяем их длину в соотношении друг с другом
                    //Сначала определяем длину всех стержней
                    Double sum_length = 0.0;
                    for (int i = 0; i < num_rods; i++) {
                        sum_length += construction.getTableView().get(i).getL();
                    }
                    //Определяем единичную длину
                    Double len_identity = 720.0 / sum_length;

                    //Теперь высоту
                    Double sum_h = 0.0;
                    for (int i = 0; i < num_rods; i++) {
                        sum_h += construction.getTableView().get(i).getA();
                    }
                    //Определяем единичную высоту
                    Double h_identity = 200.0 / sum_h;

                    Double draw_position_x = 80.0; //Позиция отрисовки по x;
                    Double draw_position_y = 200.0; //Позиция отрисовки по y;
                    //Теперь отрисовываем в правильном соотношении линии
                    for (int i = 0; i < num_rods; i++) {
                        //Получаем номер стержня
                        Integer id_rod = construction.getTableView().get(i).getNum();
                        //Далее, отрисовываем стержни
                        //получаем настоящую длину стержня
                        double real_l = construction.getTableView().get(i).getL();
                        //Получаем площадь попер. сечения
                        double real_a = construction.getTableView().get(i).getA();                  
                        //Длина i-ого стержня
                        Double len_i = len_identity * real_l;
                        global_len_i = len_i;
                        //Высота i-ого стержня
                        Double h_i = h_identity * real_a;
                        global_h_i = h_i;

                        Rectangle rectangle = new Rectangle(draw_position_x, draw_position_y - (h_i / 2),
                                                            len_i, h_i);

                        rectangle.setFill(Color.KHAKI);
                        rectangle.setStroke(Color.BLACK);
                        
                        
                        ArrayList<Double> force_q = new ArrayList<>();
                        force_q.add(0.0);
                        for (int j = 0; j < construction.getDtlfView().size(); j++) {
                            if(construction.getDtlfView().get(j).get_nodeNum() == id_rod)
                            {
                                force_q.set(0, construction.getDtlfView().get(j).get_forceNum());
                                break;
                            }
                        }
                        
                        rectangle.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            rectangle.setFill(Color.AQUA);
                        });
                        
                        rectangle.setOnMouseExited(ActionEvent -> {
                            rectangle.setFill(Color.KHAKI);
                        });
                        rods.add(rectangle);

                        //Теперь отрисовываем погонные нагрузки в этом стержне
                        //Отрисовываем погонные нагрузки
                        num_long_forces = construction.getDtlfView().size();
                        for (int j = 0; j < num_long_forces; j++) {
                            //Получаем номер стержня, в котором приложена сила
                            Integer node = construction.getDtlfView().get(j).get_nodeNum();
                            //Получаем значение силы в данном стержне
                            Double force = construction.getDtlfView().get(j).get_forceNum();
                            //Теперь, если номера стержней совпадают, прорисовываем в нем нагрузку
                            if (node == construction.getTableView().get(i).getNum()) {
                                Line line = new Line(draw_position_x, draw_position_y,
                                                     draw_position_x + len_i, draw_position_y);
                                line.setStroke(Color.BROWN);
                                //Проверяем знак нагрузки
                                if (force > 0) {
                                    //Рисуем стрелки вправо
                                    for (int k = (int) (double) draw_position_x;
                                         k < (int) (double) (draw_position_x + len_i); k += 8) {
                                        Line line1 = new Line(k, draw_position_y - 5, k + 5, draw_position_y);
                                        line1.setStroke(Color.BROWN);
                                        line1.setStrokeWidth(0.5);
                                        Line line2 = new Line(k, draw_position_y + 5, k + 5, draw_position_y);
                                        line2.setStroke(Color.BROWN);
                                        line2.setStrokeWidth(0.5);
                                        long_forces.add(line1);
                                        long_forces.add(line2);
                                    }
                                }
                                else {
                                    //Рисуем стрелки влево
                                    for (int k = (int) (double) draw_position_x;
                                         k < (int) (double) (draw_position_x + len_i); k += 8) {
                                        Line line1 = new Line(k + 5, draw_position_y - 5, k, draw_position_y);
                                        line1.setStroke(Color.BROWN);
                                        line1.setStrokeWidth(0.5);
                                        Line line2 = new Line(k + 5, draw_position_y + 5, k, draw_position_y);
                                        line2.setStroke(Color.BROWN);
                                        line2.setStrokeWidth(0.5);
                                        long_forces.add(line1);
                                        long_forces.add(line2);
                                    }
                                }
                                long_forces.add(line);
                            }
                        }

                        //Теперь отрисовываем статистические усилия
                        num_static_forces = construction.getDtsllView().size();
                        //Индексы узлов, в которых отрисованы силы
                        ArrayList<Integer> indexes_draw = new ArrayList<>();
                        for (int j = 0; j < num_static_forces; j++) {
                            //Получаем номер узла, в котором приложена сила
                            Integer node = construction.getDtsllView().get(j).get_num_rodNum();
                            //Получаем значение силы в данном узле
                            Double force = construction.getDtsllView().get(j).get_sllNum();
                            //Проверяем, не отрисовали ли в этом узле силу

                            long count = indexes_draw.stream().filter(elem -> elem == node).count();

                            if (node == i + 1 && count == 0) {
                                //Рисуем стрелку для левой части стержня
                                //Проверяем знак силы
                                if (force > 0) {
                                    //Рисуем стрелку слева стержня, внутри, направленную вправо
                                    Line line1 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + (len_i / 3), draw_position_y);
                                    Line line2 = new Line(draw_position_x + (len_i / 3), draw_position_y,
                                                          draw_position_x + (len_i / 3) - 10, draw_position_y - 5);
                                    Line line3 = new Line(draw_position_x + (len_i / 3), draw_position_y,
                                                          draw_position_x + (len_i / 3) - 10, draw_position_y + 5);
                                    line1.setStrokeWidth(4.0);
                                    line1.setStroke(Color.BROWN);
                                    line2.setStrokeWidth(4.0);
                                    line2.setStroke(Color.BROWN);
                                    line3.setStrokeWidth(4.0);
                                    line3.setStroke(Color.BROWN);
                                    //Если стрелка помещается, то рисуем
                                    if(h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    indexes_draw.add(node);
                                    
                                    //Отрисовываем лейбл статистического усилия
                                    Label label_f = new Label(construction.getDtsllView().get(j).get_sllNum() + "F");
                                    label_f.setLayoutX((draw_position_x+len_i/3)+10.0);
                                    label_f.setLayoutY(draw_position_y-9.0);
                                    label_f.setFont(new Font("Consolas", 15));
                                    if(len_i>100.0)
                                        labels.add(label_f);
                                    else 
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    
                                }
                                else {
                                    //Рисуем стрелку слева стержня, внутри, направленную влево
                                    Line line1 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + (len_i / 3), draw_position_y);
                                    Line line2 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + 3 + 10, draw_position_y - 5);
                                    Line line3 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + 3 + 10, draw_position_y + 5);
                                    line1.setStrokeWidth(4.0);
                                    line1.setStroke(Color.BROWN);
                                    line2.setStrokeWidth(4.0);
                                    line2.setStroke(Color.BROWN);
                                    line3.setStrokeWidth(4.0);
                                    line3.setStroke(Color.BROWN);
                                    //Если стрелка помещается, то рисуем
                                    if(h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    
                                    indexes_draw.add(node);
                                }
                            }
                        }
                        
                        //Теперь отрисовываем вспомогательные линии,
                        //номера стержней и номера узлов
                        //Отрисовываем линию в конце каждого стержня
                        Line end_line = new Line(draw_position_x+len_i, 200.0, draw_position_x+len_i, 550.0);
                        end_line.setStroke(Color.BLACK);
                        end_line.setStrokeWidth(0.5);
                        end_line.getStrokeDashArray().addAll(5.0,10.0);
                        extra_lines.add(end_line);
                        
                        //Отрисовывем стрелки под каждыйм стержнем
                        //Сама линия
                        Line down_line = new Line(draw_position_x, 450.0, draw_position_x+len_i, 450.0);
                        down_line.setStroke(Color.BLACK);
                        down_line.setStrokeWidth(0.5);
                        extra_lines.add(down_line);
                        
                        //Отрисовываем эпюру Nx
                        //=====================================================
                        //Рисуем только тогда, когда длина nx_values == 100
                        //и стержней больше 1
                        if (nx_values.size() <= i) {
                              System.out.println("nx_values size:"+nx_values.size());
                        }
                        else if (nx_values.get(i).size() != 101) {
                              System.out.println("nx_values "+i+" size:" + nx_values.get(i).size());
                        }
                        else 
                        {
                            //Сначала получаем длину шага
                            double len_step = (double) 1 / (double) 100;
                            System.out.println("lenstep:"+len_step);

                            //И ищем максимум в минусе  и максимум в плюсе 
                            double maxUp = 0;
                            double maxDown = 0;
                            for (int n = 0; n < num_rods; n++) {
                                for (int j = 0; j < 101; j++) {
                                    double nx = nx_values.get(n).get(j);
                                    if (nx > 0) {
                                        if (nx > maxUp) {
                                            maxUp = nx;
                                        }
                                    }
                                    else if (nx < 0) {
                                        if (nx < maxDown) {
                                            maxDown = nx;
                                        }
                                    }

                                }
                            }
                            System.out.println("maxUp " + i + " = " + maxUp);
                            System.out.println("maxDown " + i + " = " + maxDown);

                            //Получаем сумму Nx максимумов
                            double sumMax = Math.abs(maxUp) + Math.abs(maxDown);
                            System.out.println("maxSumm " + i + " = " + sumMax);
                            //Получаем длину шага для отрисовки(в пикселях)
                            double pix_step = (double) 75 / sumMax;

                            //Проходим по 100 точкам
                            for (int j = 1; j < 101; j++) {
                                //Получаем первую точку
                                double x1 = len_step * (j - 1);
                                double nx1 = nx_values.get(i).get(j - 1);
                                //Получаем y координату
                                double y_coord1 = nx1 * pix_step;
                                //Получаем вторую точку
                                double x2 = len_step * j;
                                double nx2 = nx_values.get(i).get(j);
                                //Получаем y координату
                                double y_coord2 = nx2 * pix_step;
                                //Зная все это, мы можем отрисовать линию по этим двум точкам
                                double firstX = draw_position_x + (len_i * x1);
                                double firstY;
                                if (y_coord1 >= 0) {
                                    firstY = 450.0 - y_coord1;
                                }
                                else {
                                    firstY = 450.0 + (-y_coord1);
                                }
                                double secondX = draw_position_x + (len_i * x2);
                                double secondY;
                                if (y_coord2 >= 0) {
                                    secondY = 450.0 - y_coord2;
                                }
                                else {
                                    secondY = 450.0 + (-y_coord2);
                                }

                                Line line = new Line(firstX, firstY, secondX, secondY);
                                line.setStroke(Color.RED);
                                line.setStrokeWidth(0.5);
                                extra_lines.add(line);
                                
                                Line line2 = new Line(firstX, firstY, firstX, 450.0);
                                line2.setStroke(Color.RED);
                                line2.setStrokeWidth(0.2);
                                extra_lines.add(line2);
                                
                                
                            }
                            //Отрисовываем выноски
                            //Левая часть стержня
                            Line lineFirst = new Line(draw_position_x, 450.0, draw_position_x+10.0, 380.0);
                            lineFirst.setStroke(Color.BLACK);
                            lineFirst.setStrokeWidth(0.4);
                            extra_lines.add(lineFirst);
                            
                            Line lineSecond = new Line(draw_position_x+10.0, 380.0, draw_position_x+40.0, 380.0);
                            lineSecond.setStroke(Color.BLACK);
                            lineSecond.setStrokeWidth(0.4);
                            extra_lines.add(lineSecond);
                            
                            Label lbl = new Label(Math.abs(nx_values.get(i).get(0)) + "qL");
                            lbl.setLayoutX(draw_position_x+10.0);
                            lbl.setLayoutY(365.0);
                            lbl.setFont(new Font("Consolas", 12));
                            labels.add(lbl);
                            
                            //Правая часть стержня
                            Line lineFirst2 = new Line(draw_position_x+len_i, 450.0, draw_position_x+len_i-10.0, 520.0);
                            lineFirst2.setStroke(Color.BLACK);
                            lineFirst2.setStrokeWidth(0.4);
                            extra_lines.add(lineFirst2);
                            
                            Line lineSecond2 = new Line(draw_position_x+len_i-10.0, 520.0, draw_position_x+len_i-40.0, 520.0);
                            lineSecond2.setStroke(Color.BLACK);
                            lineSecond2.setStrokeWidth(0.4);
                            extra_lines.add(lineSecond2);
                            
                            Label lbl2 = new Label(Math.abs(nx_values.get(i).get(100)) + "qL");
                            lbl2.setLayoutX(draw_position_x+len_i-50.0);
                            lbl2.setLayoutY(522.0);
                            lbl2.setFont(new Font("Consolas", 12));
                            labels.add(lbl2);    
                        }
                        //=====================================================
                        
                        
                        
                        if(len_i<30.0)
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");

                        //Если длина стержня больше 35, то отрисовываем номер узла
                        if(len_i>30.0)
                        {
                            //Отрисовывем прямоугольники для узлов
                            Rectangle num_node = new Rectangle(draw_position_x+len_i-10.0, 340.0, 20.0, 20.0);
                            num_node.setFill(Color.LAVENDER);
                            num_node.setStroke(Color.BLACK);
                            nums_nodes.add(num_node);
                            //Отрисовываем номер узла
                            Label label_num_node = new Label(""+(i+2));
                            if(i+2>9)
                                label_num_node.setLayoutX(draw_position_x+len_i-8.0);
                            else
                                label_num_node.setLayoutX(draw_position_x+len_i-4.0);
                            label_num_node.setLayoutY(342.0);
                            label_num_node.setFont(new Font("Consolas", 15));
                            labels.add(label_num_node);
                        }
                        else
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        
                        
                        
                        
                        //Отрисовываем круги для номеров стержней
                        Circle num_rod = new Circle(draw_position_x + (len_i / 2), 75.0, 12.0);
                        num_rod.setFill(Color.LAVENDER);
                        num_rod.setStroke(Color.BLACK);
                        
                        //Отрисовываем линию от круга к стержню
                        Line line_to_rod = new Line(draw_position_x + (len_i / 2),
                                75.0, draw_position_x + (len_i / 2), draw_position_y);
                        line_to_rod.setStroke(Color.BLACK);
                        line_to_rod.setStrokeWidth(0.5);
                        line_to_rod.getStrokeDashArray().addAll(5.0,10.0);
                        line_to_rod.setOpacity(0.0);
                        extra_lines.add(line_to_rod);
                        
                        num_rod.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            num_rod.setFill(Color.AQUA);
                            line_to_rod.setOpacity(1.0);
                        });
                        
                        num_rod.setOnMouseExited(ActionEvent -> {
                            num_rod.setFill(Color.LAVENDER);
                            line_to_rod.setOpacity(0.0);
                        });
                        nums_rods.add(num_rod);
                        //Отрисовываем номер узла
                        Label label_num_rod = new Label("" + (construction.getTableView().get(i).getNum()));
                        if (construction.getTableView().get(i).getNum() > 9) {
                            label_num_rod.setLayoutX(draw_position_x + (len_i / 2) - 6.0);
                        }
                        else {
                            label_num_rod.setLayoutX(draw_position_x + (len_i / 2) - 3.0);
                        }
                        label_num_rod.setLayoutY(68.0);
                        label_num_rod.setFont(new Font("Consolas", 10));
                        label_num_rod.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            num_rod.setFill(Color.AQUA);
                            line_to_rod.setOpacity(1.0);
                        });
                        
                        label_num_rod.setOnMouseExited(ActionEvent -> {
                            num_rod.setFill(Color.LAVENDER);
                            line_to_rod.setOpacity(0.0);
                        });
                        labels.add(label_num_rod);
                        
                        
                        //Если длина стержня больше 80, то отрисовываем дескритор стержня
                        if(len_i>60.0)
                        {
                            //Отрисовываем линии для каждого из стержней
                            Line descript_line1 = new Line(draw_position_x+(len_i/5),
                                    draw_position_y-(h_i/3), draw_position_x+(len_i/4),
                                    draw_position_y-(h_i/2)-10.0);
                            Line descript_line2 = new Line(draw_position_x+(len_i/4),
                                    draw_position_y-(h_i/2)-10.0, draw_position_x+(len_i/4)+len_i/3,
                                    draw_position_y-(h_i/2)-10.0);
                            descript_line1.setStroke(Color.BLACK);
                            descript_line1.setStrokeWidth(0.5);
                            descript_line2.setStroke(Color.BLACK);
                            descript_line2.setStrokeWidth(0.5);
                            Circle descript_circle = new Circle(draw_position_x+(len_i/5), draw_position_y-(h_i/3), 2.0);
                            descript_circle.setFill(Color.BLACK);
                            descript_lines.add(descript_line1);
                            descript_lines.add(descript_line2);
                            nums_rods.add(descript_circle);
                            //Теперь отрисовываем показатели каждого из стержней
                            Label descript_label = new Label("E, "+construction.getTableView().get(i).getA()+"A");
                            descript_label.setLayoutX(draw_position_x+(len_i/4)+3.0);
                            descript_label.setLayoutY(draw_position_y-(h_i/2)-25.0);
                            descript_label.setFont(new Font("Consolas", 12));
                            labels.add(descript_label);
                            
                        }
                        else
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        draw_position_x += len_i;
                    }
                        //Отрисовываем первую линию
                        Line first_line = new Line(80.0, 200.0, 80.0, 550.0);
                        first_line.setStroke(Color.BLACK);
                        first_line.setStrokeWidth(0.5);
                        first_line.getStrokeDashArray().addAll(5.0,10.0);
                        extra_lines.add(first_line);
                        
                        //Отрисовываем прямоугольник первого узла
                        Rectangle num_node = new Rectangle(70.0, 340.0, 20.0, 20.0);
                        num_node.setFill(Color.LAVENDER);
                        num_node.setStroke(Color.BLACK);
                        nums_nodes.add(num_node);
                        //Отрисовываем номер первого узла
                        Label label_num_node = new Label(""+1);
                        label_num_node.setLayoutX(76.0);
                        label_num_node.setLayoutY(342.0);
                        label_num_node.setFont(new Font("Consolas", 15));
                        labels.add(label_num_node);


                    //Теперь отдельно для последнего узла, прорисовываем статическое усилие
                    for (int j = 0; j < num_static_forces; j++) {
                        //Получаем номер узла, в котором приложена сила
                        Integer node = construction.getDtsllView().get(j).get_num_rodNum();
                        //Получаем значение силы в данном узле
                        Double force = construction.getDtsllView().get(j).get_sllNum();

                        if (node == construction.getTableView().size() + 1) {
                                //Рисуем стрелку для правой части стержня
                            //Проверяем знак силы
                            if (force > 0) {
                                //Рисуем стрелку справа стержня, внутри, направленную вправо
                                Line line1 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - (global_len_i / 3), draw_position_y);
                                Line line2 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - 3 - 10, draw_position_y - 5);
                                Line line3 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - 3 - 10, draw_position_y + 5);
                                line1.setStrokeWidth(4.0);
                                line1.setStroke(Color.BROWN);
                                line2.setStrokeWidth(4.0);
                                line2.setStroke(Color.BROWN);
                                line3.setStrokeWidth(4.0);
                                line3.setStroke(Color.BROWN);
                                //Если стрелка помещается, то рисуем
                                    if(global_h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                            }
                            else {
                                //Рисуем стрелку справа стержня, внутри, направленную влево
                                Line line1 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - (global_len_i / 3), draw_position_y);
                                Line line2 = new Line(800.0 - (global_len_i / 3), draw_position_y,
                                                      800.0 - (global_len_i / 3) + 10, draw_position_y - 5);
                                Line line3 = new Line(800.0 - (global_len_i / 3), draw_position_y,
                                                      800.0 - (global_len_i / 3) + 10, draw_position_y + 5);
                                line1.setStrokeWidth(4.0);
                                line1.setStroke(Color.BROWN);
                                line2.setStrokeWidth(4.0);
                                line2.setStroke(Color.BROWN);
                                line3.setStrokeWidth(4.0);
                                line3.setStroke(Color.BROWN);
                                //Если стрелка помещается, то рисуем
                                    if(global_h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                            }
                        }
                    }

                    //Отрисовываем заделки
                    num_props = construction.getDtlpView().size();
                    if (num_props == 0) {
                        lbl_log.setText("ВНИМАНИЕ! Конструкция не закреплена! Исправьте!");
                    }
                    else {
                        if (num_props == 1) {
                            if (construction.getDtlpView().get(0).get_nodeNum() == 1) {
                                //Отрисовываем левую заделку
                                Line line1 = new Line(80, 150.0, 80.0, 250.0);
                                line1.setStrokeWidth(3.0);
                                line1.setStroke(Color.BROWN);
                                //Теперь штрихи заделки
                                for (int i = 150; i <= 250; i += 10) {
                                    Line line_str = new Line(70, i + 5, 80.0, i);
                                    line_str.setStroke(Color.BROWN);
                                    left_prop.add(line_str);
                                }
                                left_prop.add(line1);
                            }
                            else {
                                //Отрисовываем правую заделку
                                Line line1 = new Line(800.0, 150.0, 800.0, 250.0);
                                line1.setStrokeWidth(3.0);
                                line1.setStroke(Color.BROWN);
                                //Теперь штрихи заделки
                                for (int i = 150; i <= 250; i += 10) {
                                    Line line_str = new Line(810.0, i - 5, 800.0, i);
                                    line_str.setStroke(Color.BROWN);
                                    right_prop.add(line_str);
                                }
                                right_prop.add(line1);
                            }
                        }
                        else {

                            //Отрисовываем левую заделку
                            Line line1 = new Line(80, 150.0, 80.0, 250.0);
                            line1.setStrokeWidth(3.0);
                            line1.setStroke(Color.BROWN);
                            left_prop.add(line1);
                            //Теперь штрихи заделки
                            for (int i = 150; i <= 250; i += 10) {
                                Line line_str = new Line(70, i + 5, 80.0, i);
                                line_str.setStroke(Color.BROWN);
                                left_prop.add(line_str);
                            }

                            //Отрисовываем правую заделку
                            Line line2 = new Line(800.0, 150.0, 800.0, 250.0);
                            line2.setStrokeWidth(3.0);
                            line2.setStroke(Color.BROWN);
                            //Теперь штрихи заделки
                            for (int i = 150; i <= 250; i += 10) {
                                Line line_str = new Line(810.0, i - 5, 800.0, i);
                                line_str.setStroke(Color.BROWN);
                                right_prop.add(line_str);
                            }
                            right_prop.add(line2);
                        }
                    }
                }
                else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
            }else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
        }else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
    }

    public static void draw_diagramm_2(Construction construction,
                            Label lbl_log,
                            ArrayList<Rectangle> rods,
                            ArrayList<Line> left_prop,
                            ArrayList<Line> right_prop,
                            ArrayList<Line> long_forces,
                            ArrayList<Line> static_forces,
                            ArrayList<Line> extra_lines,
                            ArrayList<Label> labels,
                            ArrayList<Rectangle> nums_nodes,
                            ArrayList<Circle> nums_rods,
                            ArrayList<Line> descript_lines,
                            Label lbl_descrip1,
                            Label lbl_descrip2,
                            Label lbl_descrip3,
                            Label lbl_descrip4,
                            ArrayList<ArrayList<Double>> ux_values)
    {
        //Здесь отрисовываем конструкцию с эпюрой 1

        //Сначала определяем пропорцию длины каждого стержня
        //720 -изначальная длина конструкции
        int num_props = 0; //Количество заделок
        int num_long_forces = 0; //Количество погонных нагрузок
        int num_static_forces = 0; //Количество погонных нагрузок
        Double global_len_i = 0.0;
        Double global_h_i = 0.0;

        if (construction != null) {
            if (construction.getTableView() != null) {
                //Определяем количество стержней
                Integer num_rods = construction.getTableView().size();
                if (num_rods > 0) {
                    //Далее, проходим по порядку все стержни, и в зависимости от их
                    //Длины, определяем их длину в соотношении друг с другом
                    //Сначала определяем длину всех стержней
                    Double sum_length = 0.0;
                    for (int i = 0; i < num_rods; i++) {
                        sum_length += construction.getTableView().get(i).getL();
                    }
                    //Определяем единичную длину
                    Double len_identity = 720.0 / sum_length;

                    //Теперь высоту
                    Double sum_h = 0.0;
                    for (int i = 0; i < num_rods; i++) {
                        sum_h += construction.getTableView().get(i).getA();
                    }
                    //Определяем единичную высоту
                    Double h_identity = 200.0 / sum_h;

                    Double draw_position_x = 80.0; //Позиция отрисовки по x;
                    Double draw_position_y = 200.0; //Позиция отрисовки по y;
                    //Теперь отрисовываем в правильном соотношении линии
                    for (int i = 0; i < num_rods; i++) {
                        //Получаем номер стержня
                        Integer id_rod = construction.getTableView().get(i).getNum();
                        //Далее, отрисовываем стержни
                        //получаем настоящую длину стержня
                        double real_l = construction.getTableView().get(i).getL();
                        //Получаем площадь попер. сечения
                        double real_a = construction.getTableView().get(i).getA();                  
                        //Длина i-ого стержня
                        Double len_i = len_identity * real_l;
                        global_len_i = len_i;
                        //Высота i-ого стержня
                        Double h_i = h_identity * real_a;
                        global_h_i = h_i;

                        Rectangle rectangle = new Rectangle(draw_position_x, draw_position_y - (h_i / 2),
                                                            len_i, h_i);

                        rectangle.setFill(Color.KHAKI);
                        rectangle.setStroke(Color.BLACK);
                        
                        
                        ArrayList<Double> force_q = new ArrayList<>();
                        force_q.add(0.0);
                        for (int j = 0; j < construction.getDtlfView().size(); j++) {
                            if(construction.getDtlfView().get(j).get_nodeNum() == id_rod)
                            {
                                force_q.set(0, construction.getDtlfView().get(j).get_forceNum());
                                break;
                            }
                        }
                        
                        rectangle.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            rectangle.setFill(Color.AQUA);
                        });
                        
                        rectangle.setOnMouseExited(ActionEvent -> {
                            rectangle.setFill(Color.KHAKI);
                        });
                        rods.add(rectangle);

                        //Теперь отрисовываем погонные нагрузки в этом стержне
                        //Отрисовываем погонные нагрузки
                        num_long_forces = construction.getDtlfView().size();
                        for (int j = 0; j < num_long_forces; j++) {
                            //Получаем номер стержня, в котором приложена сила
                            Integer node = construction.getDtlfView().get(j).get_nodeNum();
                            //Получаем значение силы в данном стержне
                            Double force = construction.getDtlfView().get(j).get_forceNum();
                            //Теперь, если номера стержней совпадают, прорисовываем в нем нагрузку
                            if (node == construction.getTableView().get(i).getNum()) {
                                Line line = new Line(draw_position_x, draw_position_y,
                                                     draw_position_x + len_i, draw_position_y);
                                line.setStroke(Color.BROWN);
                                //Проверяем знак нагрузки
                                if (force > 0) {
                                    //Рисуем стрелки вправо
                                    for (int k = (int) (double) draw_position_x;
                                         k < (int) (double) (draw_position_x + len_i); k += 8) {
                                        Line line1 = new Line(k, draw_position_y - 5, k + 5, draw_position_y);
                                        line1.setStroke(Color.BROWN);
                                        line1.setStrokeWidth(0.5);
                                        Line line2 = new Line(k, draw_position_y + 5, k + 5, draw_position_y);
                                        line2.setStroke(Color.BROWN);
                                        line2.setStrokeWidth(0.5);
                                        long_forces.add(line1);
                                        long_forces.add(line2);
                                    }
                                }
                                else {
                                    //Рисуем стрелки влево
                                    for (int k = (int) (double) draw_position_x;
                                         k < (int) (double) (draw_position_x + len_i); k += 8) {
                                        Line line1 = new Line(k + 5, draw_position_y - 5, k, draw_position_y);
                                        line1.setStroke(Color.BROWN);
                                        line1.setStrokeWidth(0.5);
                                        Line line2 = new Line(k + 5, draw_position_y + 5, k, draw_position_y);
                                        line2.setStroke(Color.BROWN);
                                        line2.setStrokeWidth(0.5);
                                        long_forces.add(line1);
                                        long_forces.add(line2);
                                    }
                                }
                                long_forces.add(line);
                            }
                        }

                        //Теперь отрисовываем статистические усилия
                        num_static_forces = construction.getDtsllView().size();
                        //Индексы узлов, в которых отрисованы силы
                        ArrayList<Integer> indexes_draw = new ArrayList<>();
                        for (int j = 0; j < num_static_forces; j++) {
                            //Получаем номер узла, в котором приложена сила
                            Integer node = construction.getDtsllView().get(j).get_num_rodNum();
                            //Получаем значение силы в данном узле
                            Double force = construction.getDtsllView().get(j).get_sllNum();
                            //Проверяем, не отрисовали ли в этом узле силу

                            long count = indexes_draw.stream().filter(elem -> elem == node).count();

                            if (node == i + 1 && count == 0) {
                                //Рисуем стрелку для левой части стержня
                                //Проверяем знак силы
                                if (force > 0) {
                                    //Рисуем стрелку слева стержня, внутри, направленную вправо
                                    Line line1 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + (len_i / 3), draw_position_y);
                                    Line line2 = new Line(draw_position_x + (len_i / 3), draw_position_y,
                                                          draw_position_x + (len_i / 3) - 10, draw_position_y - 5);
                                    Line line3 = new Line(draw_position_x + (len_i / 3), draw_position_y,
                                                          draw_position_x + (len_i / 3) - 10, draw_position_y + 5);
                                    line1.setStrokeWidth(4.0);
                                    line1.setStroke(Color.BROWN);
                                    line2.setStrokeWidth(4.0);
                                    line2.setStroke(Color.BROWN);
                                    line3.setStrokeWidth(4.0);
                                    line3.setStroke(Color.BROWN);
                                    //Если стрелка помещается, то рисуем
                                    if(h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    indexes_draw.add(node);
                                    
                                    //Отрисовываем лейбл статистического усилия
                                    Label label_f = new Label(construction.getDtsllView().get(j).get_sllNum() + "F");
                                    label_f.setLayoutX((draw_position_x+len_i/3)+10.0);
                                    label_f.setLayoutY(draw_position_y-9.0);
                                    label_f.setFont(new Font("Consolas", 15));
                                    if(len_i>100.0)
                                        labels.add(label_f);
                                    else 
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    
                                }
                                else {
                                    //Рисуем стрелку слева стержня, внутри, направленную влево
                                    Line line1 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + (len_i / 3), draw_position_y);
                                    Line line2 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + 3 + 10, draw_position_y - 5);
                                    Line line3 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + 3 + 10, draw_position_y + 5);
                                    line1.setStrokeWidth(4.0);
                                    line1.setStroke(Color.BROWN);
                                    line2.setStrokeWidth(4.0);
                                    line2.setStroke(Color.BROWN);
                                    line3.setStrokeWidth(4.0);
                                    line3.setStroke(Color.BROWN);
                                    //Если стрелка помещается, то рисуем
                                    if(h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    
                                    indexes_draw.add(node);
                                }
                            }
                        }
                        
                        //Теперь отрисовываем вспомогательные линии,
                        //номера стержней и номера узлов
                        //Отрисовываем линию в конце каждого стержня
                        Line end_line = new Line(draw_position_x+len_i, 200.0, draw_position_x+len_i, 550.0);
                        end_line.setStroke(Color.BLACK);
                        end_line.setStrokeWidth(0.5);
                        end_line.getStrokeDashArray().addAll(5.0,10.0);
                        extra_lines.add(end_line);
                        
                        //Отрисовывем стрелки под каждыйм стержнем
                        //Сама линия
                        Line down_line = new Line(draw_position_x, 450.0, draw_position_x+len_i, 450.0);
                        down_line.setStroke(Color.BLACK);
                        down_line.setStrokeWidth(0.5);
                        extra_lines.add(down_line);
                        
                        //Отрисовываем эпюру Ux
                        //=====================================================
                        //Рисуем только тогда, когда длина ux_values == 100
                        //и стержней больше 1
                        if (ux_values.size() <= i) {
                              System.out.println("nx_values size:"+ux_values.size());
                        }
                        else if (ux_values.get(i).size() != 101) {
                              System.out.println("nx_values "+i+" size:" + ux_values.get(i).size());
                        }
                        else 
                        {
                            //Сначала получаем длину шага
                            double len_step = (double) 1 / (double) 100;
                            System.out.println("lenstep:"+len_step);

                            //И ищем максимум в минусе  и максимум в плюсе 
                            double maxUp = 0;
                            double maxDown = 0;
                            for (int n = 0; n < num_rods; n++) {
                                for (int j = 0; j < 101; j++) {
                                    double ux = ux_values.get(n).get(j);
                                    if (ux > 0) {
                                        if (ux > maxUp) {
                                            maxUp = ux;
                                        }
                                    }
                                    else if (ux < 0) {
                                        if (ux < maxDown) {
                                            maxDown = ux;
                                        }
                                    }

                                }
                            }

                            //Получаем сумму Ux максимумов
                            double sumMax = Math.abs(maxUp) + Math.abs(maxDown);
                            //Получаем длину шага для отрисовки(в пикселях)
                            double pix_step = (double) 75 / sumMax;

                            //Проходим по 100 точкам
                            for (int j = 1; j < 101; j++) {
                                //Получаем первую точку
                                double x1 = len_step * (j - 1);
                                double nx1 = ux_values.get(i).get(j - 1);
                                //Получаем y координату
                                double y_coord1 = nx1 * pix_step;
                                //Получаем вторую точку
                                double x2 = len_step * j;
                                double nx2 = ux_values.get(i).get(j);
                                //Получаем y координату
                                double y_coord2 = nx2 * pix_step;
                                //Зная все это, мы можем отрисовать линию по этим двум точкам
                                double firstX = draw_position_x + (len_i * x1);
                                double firstY;
                                if (y_coord1 >= 0) {
                                    firstY = 450.0 - y_coord1;
                                }
                                else {
                                    firstY = 450.0 + (-y_coord1);
                                }
                                double secondX = draw_position_x + (len_i * x2);
                                double secondY;
                                if (y_coord2 >= 0) {
                                    secondY = 450.0 - y_coord2;
                                }
                                else {
                                    secondY = 450.0 + (-y_coord2);
                                }

                                Line line = new Line(firstX, firstY, secondX, secondY);
                                line.setStroke(Color.FORESTGREEN);
                                line.setStrokeWidth(0.5);
                                extra_lines.add(line);
                                
                                Line line2 = new Line(firstX, firstY, firstX, 450.0);
                                line2.setStroke(Color.FORESTGREEN);
                                line2.setStrokeWidth(0.2);
                                extra_lines.add(line2);
                            }
                            
                            //Отрисовываем выноски
                            //Левая часть стержня
                            Line lineFirst = new Line(draw_position_x, 450.0, draw_position_x+10.0, 380.0);
                            lineFirst.setStroke(Color.BLACK);
                            lineFirst.setStrokeWidth(0.4);
                            extra_lines.add(lineFirst);
                            
                            Line lineSecond = new Line(draw_position_x+10.0, 380.0, draw_position_x+40.0, 380.0);
                            lineSecond.setStroke(Color.BLACK);
                            lineSecond.setStrokeWidth(0.4);
                            extra_lines.add(lineSecond);
                            
                            Label lbl = new Label(Math.abs(ux_values.get(i).get(0)) + "qL/A");
                            lbl.setLayoutX(draw_position_x+5.0);
                            lbl.setLayoutY(365.0);
                            lbl.setFont(new Font("Consolas", 12));
                            labels.add(lbl);
                            
                            //Правая часть стержня
                            Line lineFirst2 = new Line(draw_position_x+len_i, 450.0, draw_position_x+len_i-10.0, 520.0);
                            lineFirst2.setStroke(Color.BLACK);
                            lineFirst2.setStrokeWidth(0.4);
                            extra_lines.add(lineFirst2);
                            
                            Line lineSecond2 = new Line(draw_position_x+len_i-10.0, 520.0, draw_position_x+len_i-40.0, 520.0);
                            lineSecond2.setStroke(Color.BLACK);
                            lineSecond2.setStrokeWidth(0.4);
                            extra_lines.add(lineSecond2);
                            
                            Label lbl2 = new Label(Math.abs(ux_values.get(i).get(100)) + "qL/A");
                            lbl2.setLayoutX(draw_position_x+len_i-55.0);
                            lbl2.setLayoutY(522.0);
                            lbl2.setFont(new Font("Consolas", 12));
                            labels.add(lbl2);  
                        }
                        //=====================================================
                        
                        
                        
                        if(len_i<30.0)
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");

                        //Если длина стержня больше 35, то отрисовываем номер узла
                        if(len_i>30.0)
                        {
                            //Отрисовывем прямоугольники для узлов
                            Rectangle num_node = new Rectangle(draw_position_x+len_i-10.0, 340.0, 20.0, 20.0);
                            num_node.setFill(Color.LAVENDER);
                            num_node.setStroke(Color.BLACK);
                            nums_nodes.add(num_node);
                            //Отрисовываем номер узла
                            Label label_num_node = new Label(""+(i+2));
                            if(i+2>9)
                                label_num_node.setLayoutX(draw_position_x+len_i-8.0);
                            else
                                label_num_node.setLayoutX(draw_position_x+len_i-4.0);
                            label_num_node.setLayoutY(342.0);
                            label_num_node.setFont(new Font("Consolas", 15));
                            labels.add(label_num_node);
                        }
                        else
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        
                        
                        
                        
                        //Отрисовываем круги для номеров стержней
                        Circle num_rod = new Circle(draw_position_x + (len_i / 2), 75.0, 12.0);
                        num_rod.setFill(Color.LAVENDER);
                        num_rod.setStroke(Color.BLACK);
                        
                        //Отрисовываем линию от круга к стержню
                        Line line_to_rod = new Line(draw_position_x + (len_i / 2),
                                75.0, draw_position_x + (len_i / 2), draw_position_y);
                        line_to_rod.setStroke(Color.BLACK);
                        line_to_rod.setStrokeWidth(0.5);
                        line_to_rod.getStrokeDashArray().addAll(5.0,10.0);
                        line_to_rod.setOpacity(0.0);
                        extra_lines.add(line_to_rod);
                        
                        num_rod.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            num_rod.setFill(Color.AQUA);
                            line_to_rod.setOpacity(1.0);
                        });
                        
                        num_rod.setOnMouseExited(ActionEvent -> {
                            num_rod.setFill(Color.LAVENDER);
                            line_to_rod.setOpacity(0.0);
                        });
                        nums_rods.add(num_rod);
                        //Отрисовываем номер узла
                        Label label_num_rod = new Label("" + (construction.getTableView().get(i).getNum()));
                        if (construction.getTableView().get(i).getNum() > 9) {
                            label_num_rod.setLayoutX(draw_position_x + (len_i / 2) - 6.0);
                        }
                        else {
                            label_num_rod.setLayoutX(draw_position_x + (len_i / 2) - 3.0);
                        }
                        label_num_rod.setLayoutY(68.0);
                        label_num_rod.setFont(new Font("Consolas", 10));
                        label_num_rod.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            num_rod.setFill(Color.AQUA);
                            line_to_rod.setOpacity(1.0);
                        });
                        
                        label_num_rod.setOnMouseExited(ActionEvent -> {
                            num_rod.setFill(Color.LAVENDER);
                            line_to_rod.setOpacity(0.0);
                        });
                        labels.add(label_num_rod);
                        
                        
                        //Если длина стержня больше 80, то отрисовываем дескритор стержня
                        if(len_i>60.0)
                        {
                            //Отрисовываем линии для каждого из стержней
                            Line descript_line1 = new Line(draw_position_x+(len_i/5),
                                    draw_position_y-(h_i/3), draw_position_x+(len_i/4),
                                    draw_position_y-(h_i/2)-10.0);
                            Line descript_line2 = new Line(draw_position_x+(len_i/4),
                                    draw_position_y-(h_i/2)-10.0, draw_position_x+(len_i/4)+len_i/3,
                                    draw_position_y-(h_i/2)-10.0);
                            descript_line1.setStroke(Color.BLACK);
                            descript_line1.setStrokeWidth(0.5);
                            descript_line2.setStroke(Color.BLACK);
                            descript_line2.setStrokeWidth(0.5);
                            Circle descript_circle = new Circle(draw_position_x+(len_i/5), draw_position_y-(h_i/3), 2.0);
                            descript_circle.setFill(Color.BLACK);
                            descript_lines.add(descript_line1);
                            descript_lines.add(descript_line2);
                            nums_rods.add(descript_circle);
                            //Теперь отрисовываем показатели каждого из стержней
                            Label descript_label = new Label("E, "+construction.getTableView().get(i).getA()+"A");
                            descript_label.setLayoutX(draw_position_x+(len_i/4)+3.0);
                            descript_label.setLayoutY(draw_position_y-(h_i/2)-25.0);
                            descript_label.setFont(new Font("Consolas", 12));
                            labels.add(descript_label);
                            
                        }
                        else
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        draw_position_x += len_i;
                    }
                        //Отрисовываем первую линию
                        Line first_line = new Line(80.0, 200.0, 80.0, 550.0);
                        first_line.setStroke(Color.BLACK);
                        first_line.setStrokeWidth(0.5);
                        first_line.getStrokeDashArray().addAll(5.0,10.0);
                        extra_lines.add(first_line);
                        
                        //Отрисовываем прямоугольник первого узла
                        Rectangle num_node = new Rectangle(70.0, 340.0, 20.0, 20.0);
                        num_node.setFill(Color.LAVENDER);
                        num_node.setStroke(Color.BLACK);
                        nums_nodes.add(num_node);
                        //Отрисовываем номер первого узла
                        Label label_num_node = new Label(""+1);
                        label_num_node.setLayoutX(76.0);
                        label_num_node.setLayoutY(342.0);
                        label_num_node.setFont(new Font("Consolas", 15));
                        labels.add(label_num_node);


                    //Теперь отдельно для последнего узла, прорисовываем статическое усилие
                    for (int j = 0; j < num_static_forces; j++) {
                        //Получаем номер узла, в котором приложена сила
                        Integer node = construction.getDtsllView().get(j).get_num_rodNum();
                        //Получаем значение силы в данном узле
                        Double force = construction.getDtsllView().get(j).get_sllNum();

                        if (node == construction.getTableView().size() + 1) {
                                //Рисуем стрелку для правой части стержня
                            //Проверяем знак силы
                            if (force > 0) {
                                //Рисуем стрелку справа стержня, внутри, направленную вправо
                                Line line1 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - (global_len_i / 3), draw_position_y);
                                Line line2 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - 3 - 10, draw_position_y - 5);
                                Line line3 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - 3 - 10, draw_position_y + 5);
                                line1.setStrokeWidth(4.0);
                                line1.setStroke(Color.BROWN);
                                line2.setStrokeWidth(4.0);
                                line2.setStroke(Color.BROWN);
                                line3.setStrokeWidth(4.0);
                                line3.setStroke(Color.BROWN);
                                //Если стрелка помещается, то рисуем
                                    if(global_h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                            }
                            else {
                                //Рисуем стрелку справа стержня, внутри, направленную влево
                                Line line1 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - (global_len_i / 3), draw_position_y);
                                Line line2 = new Line(800.0 - (global_len_i / 3), draw_position_y,
                                                      800.0 - (global_len_i / 3) + 10, draw_position_y - 5);
                                Line line3 = new Line(800.0 - (global_len_i / 3), draw_position_y,
                                                      800.0 - (global_len_i / 3) + 10, draw_position_y + 5);
                                line1.setStrokeWidth(4.0);
                                line1.setStroke(Color.BROWN);
                                line2.setStrokeWidth(4.0);
                                line2.setStroke(Color.BROWN);
                                line3.setStrokeWidth(4.0);
                                line3.setStroke(Color.BROWN);
                                //Если стрелка помещается, то рисуем
                                    if(global_h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                            }
                        }
                    }

                    //Отрисовываем заделки
                    num_props = construction.getDtlpView().size();
                    if (num_props == 0) {
                        lbl_log.setText("ВНИМАНИЕ! Конструкция не закреплена! Исправьте!");
                    }
                    else {
                        if (num_props == 1) {
                            if (construction.getDtlpView().get(0).get_nodeNum() == 1) {
                                //Отрисовываем левую заделку
                                Line line1 = new Line(80, 150.0, 80.0, 250.0);
                                line1.setStrokeWidth(3.0);
                                line1.setStroke(Color.BROWN);
                                //Теперь штрихи заделки
                                for (int i = 150; i <= 250; i += 10) {
                                    Line line_str = new Line(70, i + 5, 80.0, i);
                                    line_str.setStroke(Color.BROWN);
                                    left_prop.add(line_str);
                                }
                                left_prop.add(line1);
                            }
                            else {
                                //Отрисовываем правую заделку
                                Line line1 = new Line(800.0, 150.0, 800.0, 250.0);
                                line1.setStrokeWidth(3.0);
                                line1.setStroke(Color.BROWN);
                                //Теперь штрихи заделки
                                for (int i = 150; i <= 250; i += 10) {
                                    Line line_str = new Line(810.0, i - 5, 800.0, i);
                                    line_str.setStroke(Color.BROWN);
                                    right_prop.add(line_str);
                                }
                                right_prop.add(line1);
                            }
                        }
                        else {

                            //Отрисовываем левую заделку
                            Line line1 = new Line(80, 150.0, 80.0, 250.0);
                            line1.setStrokeWidth(3.0);
                            line1.setStroke(Color.BROWN);
                            left_prop.add(line1);
                            //Теперь штрихи заделки
                            for (int i = 150; i <= 250; i += 10) {
                                Line line_str = new Line(70, i + 5, 80.0, i);
                                line_str.setStroke(Color.BROWN);
                                left_prop.add(line_str);
                            }

                            //Отрисовываем правую заделку
                            Line line2 = new Line(800.0, 150.0, 800.0, 250.0);
                            line2.setStrokeWidth(3.0);
                            line2.setStroke(Color.BROWN);
                            //Теперь штрихи заделки
                            for (int i = 150; i <= 250; i += 10) {
                                Line line_str = new Line(810.0, i - 5, 800.0, i);
                                line_str.setStroke(Color.BROWN);
                                right_prop.add(line_str);
                            }
                            right_prop.add(line2);
                        }
                    }
                }
                else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
            }else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
        }else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
    }

    public static void draw_diagramm_3(Construction construction,
                            Label lbl_log,
                            ArrayList<Rectangle> rods,
                            ArrayList<Line> left_prop,
                            ArrayList<Line> right_prop,
                            ArrayList<Line> long_forces,
                            ArrayList<Line> static_forces,
                            ArrayList<Line> extra_lines,
                            ArrayList<Label> labels,
                            ArrayList<Rectangle> nums_nodes,
                            ArrayList<Circle> nums_rods,
                            ArrayList<Line> descript_lines,
                            Label lbl_descrip1,
                            Label lbl_descrip2,
                            Label lbl_descrip3,
                            Label lbl_descrip4,
                            ArrayList<ArrayList<Double>> sigmax_values)
    {
        //Здесь отрисовываем конструкцию с эпюрой 1

        //Сначала определяем пропорцию длины каждого стержня
        //720 -изначальная длина конструкции
        int num_props = 0; //Количество заделок
        int num_long_forces = 0; //Количество погонных нагрузок
        int num_static_forces = 0; //Количество погонных нагрузок
        Double global_len_i = 0.0;
        Double global_h_i = 0.0;

        if (construction != null) {
            if (construction.getTableView() != null) {
                //Определяем количество стержней
                Integer num_rods = construction.getTableView().size();
                if (num_rods > 0) {
                    //Далее, проходим по порядку все стержни, и в зависимости от их
                    //Длины, определяем их длину в соотношении друг с другом
                    //Сначала определяем длину всех стержней
                    Double sum_length = 0.0;
                    for (int i = 0; i < num_rods; i++) {
                        sum_length += construction.getTableView().get(i).getL();
                    }
                    //Определяем единичную длину
                    Double len_identity = 720.0 / sum_length;

                    //Теперь высоту
                    Double sum_h = 0.0;
                    for (int i = 0; i < num_rods; i++) {
                        sum_h += construction.getTableView().get(i).getA();
                    }
                    //Определяем единичную высоту
                    Double h_identity = 200.0 / sum_h;

                    Double draw_position_x = 80.0; //Позиция отрисовки по x;
                    Double draw_position_y = 200.0; //Позиция отрисовки по y;
                    //Теперь отрисовываем в правильном соотношении линии
                    for (int i = 0; i < num_rods; i++) {
                        //Получаем номер стержня
                        Integer id_rod = construction.getTableView().get(i).getNum();
                        //Далее, отрисовываем стержни
                        //получаем настоящую длину стержня
                        double real_l = construction.getTableView().get(i).getL();
                        //Получаем площадь попер. сечения
                        double real_a = construction.getTableView().get(i).getA();                  
                        //Длина i-ого стержня
                        Double len_i = len_identity * real_l;
                        global_len_i = len_i;
                        //Высота i-ого стержня
                        Double h_i = h_identity * real_a;
                        global_h_i = h_i;

                        Rectangle rectangle = new Rectangle(draw_position_x, draw_position_y - (h_i / 2),
                                                            len_i, h_i);

                        rectangle.setFill(Color.KHAKI);
                        rectangle.setStroke(Color.BLACK);
                        
                        
                        ArrayList<Double> force_q = new ArrayList<>();
                        force_q.add(0.0);
                        for (int j = 0; j < construction.getDtlfView().size(); j++) {
                            if(construction.getDtlfView().get(j).get_nodeNum() == id_rod)
                            {
                                force_q.set(0, construction.getDtlfView().get(j).get_forceNum());
                                break;
                            }
                        }
                        
                        rectangle.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            rectangle.setFill(Color.AQUA);
                        });
                        
                        rectangle.setOnMouseExited(ActionEvent -> {
                            rectangle.setFill(Color.KHAKI);
                        });
                        rods.add(rectangle);

                        //Теперь отрисовываем погонные нагрузки в этом стержне
                        //Отрисовываем погонные нагрузки
                        num_long_forces = construction.getDtlfView().size();
                        for (int j = 0; j < num_long_forces; j++) {
                            //Получаем номер стержня, в котором приложена сила
                            Integer node = construction.getDtlfView().get(j).get_nodeNum();
                            //Получаем значение силы в данном стержне
                            Double force = construction.getDtlfView().get(j).get_forceNum();
                            //Теперь, если номера стержней совпадают, прорисовываем в нем нагрузку
                            if (node == construction.getTableView().get(i).getNum()) {
                                Line line = new Line(draw_position_x, draw_position_y,
                                                     draw_position_x + len_i, draw_position_y);
                                line.setStroke(Color.BROWN);
                                //Проверяем знак нагрузки
                                if (force > 0) {
                                    //Рисуем стрелки вправо
                                    for (int k = (int) (double) draw_position_x;
                                         k < (int) (double) (draw_position_x + len_i); k += 8) {
                                        Line line1 = new Line(k, draw_position_y - 5, k + 5, draw_position_y);
                                        line1.setStroke(Color.BROWN);
                                        line1.setStrokeWidth(0.5);
                                        Line line2 = new Line(k, draw_position_y + 5, k + 5, draw_position_y);
                                        line2.setStroke(Color.BROWN);
                                        line2.setStrokeWidth(0.5);
                                        long_forces.add(line1);
                                        long_forces.add(line2);
                                    }
                                }
                                else {
                                    //Рисуем стрелки влево
                                    for (int k = (int) (double) draw_position_x;
                                         k < (int) (double) (draw_position_x + len_i); k += 8) {
                                        Line line1 = new Line(k + 5, draw_position_y - 5, k, draw_position_y);
                                        line1.setStroke(Color.BROWN);
                                        line1.setStrokeWidth(0.5);
                                        Line line2 = new Line(k + 5, draw_position_y + 5, k, draw_position_y);
                                        line2.setStroke(Color.BROWN);
                                        line2.setStrokeWidth(0.5);
                                        long_forces.add(line1);
                                        long_forces.add(line2);
                                    }
                                }
                                long_forces.add(line);
                            }
                        }

                        //Теперь отрисовываем статистические усилия
                        num_static_forces = construction.getDtsllView().size();
                        //Индексы узлов, в которых отрисованы силы
                        ArrayList<Integer> indexes_draw = new ArrayList<>();
                        for (int j = 0; j < num_static_forces; j++) {
                            //Получаем номер узла, в котором приложена сила
                            Integer node = construction.getDtsllView().get(j).get_num_rodNum();
                            //Получаем значение силы в данном узле
                            Double force = construction.getDtsllView().get(j).get_sllNum();
                            //Проверяем, не отрисовали ли в этом узле силу

                            long count = indexes_draw.stream().filter(elem -> elem == node).count();

                            if (node == i + 1 && count == 0) {
                                //Рисуем стрелку для левой части стержня
                                //Проверяем знак силы
                                if (force > 0) {
                                    //Рисуем стрелку слева стержня, внутри, направленную вправо
                                    Line line1 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + (len_i / 3), draw_position_y);
                                    Line line2 = new Line(draw_position_x + (len_i / 3), draw_position_y,
                                                          draw_position_x + (len_i / 3) - 10, draw_position_y - 5);
                                    Line line3 = new Line(draw_position_x + (len_i / 3), draw_position_y,
                                                          draw_position_x + (len_i / 3) - 10, draw_position_y + 5);
                                    line1.setStrokeWidth(4.0);
                                    line1.setStroke(Color.BROWN);
                                    line2.setStrokeWidth(4.0);
                                    line2.setStroke(Color.BROWN);
                                    line3.setStrokeWidth(4.0);
                                    line3.setStroke(Color.BROWN);
                                    //Если стрелка помещается, то рисуем
                                    if(h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    indexes_draw.add(node);
                                    
                                    //Отрисовываем лейбл статистического усилия
                                    Label label_f = new Label(construction.getDtsllView().get(j).get_sllNum() + "F");
                                    label_f.setLayoutX((draw_position_x+len_i/3)+10.0);
                                    label_f.setLayoutY(draw_position_y-9.0);
                                    label_f.setFont(new Font("Consolas", 15));
                                    if(len_i>100.0)
                                        labels.add(label_f);
                                    else 
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    
                                }
                                else {
                                    //Рисуем стрелку слева стержня, внутри, направленную влево
                                    Line line1 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + (len_i / 3), draw_position_y);
                                    Line line2 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + 3 + 10, draw_position_y - 5);
                                    Line line3 = new Line(draw_position_x + 3, draw_position_y,
                                                          draw_position_x + 3 + 10, draw_position_y + 5);
                                    line1.setStrokeWidth(4.0);
                                    line1.setStroke(Color.BROWN);
                                    line2.setStrokeWidth(4.0);
                                    line2.setStroke(Color.BROWN);
                                    line3.setStrokeWidth(4.0);
                                    line3.setStroke(Color.BROWN);
                                    //Если стрелка помещается, то рисуем
                                    if(h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                                    
                                    indexes_draw.add(node);
                                }
                            }
                        }
                        
                        //Теперь отрисовываем вспомогательные линии,
                        //номера стержней и номера узлов
                        //Отрисовываем линию в конце каждого стержня
                        Line end_line = new Line(draw_position_x+len_i, 200.0, draw_position_x+len_i, 550.0);
                        end_line.setStroke(Color.BLACK);
                        end_line.setStrokeWidth(0.5);
                        end_line.getStrokeDashArray().addAll(5.0,10.0);
                        extra_lines.add(end_line);
                        
                        //Отрисовывем стрелки под каждыйм стержнем
                        //Сама линия
                        Line down_line = new Line(draw_position_x, 450.0, draw_position_x+len_i, 450.0);
                        down_line.setStroke(Color.BLACK);
                        down_line.setStrokeWidth(0.5);
                        extra_lines.add(down_line);
                        
                        //Отрисовываем эпюру Ux
                        //=====================================================
                        //Рисуем только тогда, когда длина ux_values == 100
                        //и стержней больше 1
                        if (sigmax_values.size() <= i) {
                              System.out.println("nx_values size:"+sigmax_values.size());
                        }
                        else if (sigmax_values.get(i).size() != 101) {
                              System.out.println("nx_values "+i+" size:" + sigmax_values.get(i).size());
                        }
                        else 
                        {
                            //Сначала получаем длину шага
                            double len_step = (double) 1 / (double) 100;
                            System.out.println("lenstep:"+len_step);

                            //И ищем максимум в минусе  и максимум в плюсе 
                            double maxUp = 0;
                            double maxDown = 0;
                            for (int n = 0; n < num_rods; n++) {
                                for (int j = 0; j < 101; j++) {
                                    double sigmax = sigmax_values.get(n).get(j);
                                    if (sigmax > 0) {
                                        if (sigmax > maxUp) {
                                            maxUp = sigmax;
                                        }
                                    }
                                    else if (sigmax < 0) {
                                        if (sigmax < maxDown) {
                                            maxDown = sigmax;
                                        }
                                    }

                                }
                            }

                            //Получаем сумму Ux максимумов
                            double sumMax = Math.abs(maxUp) + Math.abs(maxDown);
                            //Получаем длину шага для отрисовки(в пикселях)
                            double pix_step = (double) 75 / sumMax;

                            //Проходим по 100 точкам
                            for (int j = 1; j < 101; j++) {
                                //Получаем первую точку
                                double x1 = len_step * (j - 1);
                                double nx1 = sigmax_values.get(i).get(j - 1);
                                //Получаем y координату
                                double y_coord1 = nx1 * pix_step;
                                //Получаем вторую точку
                                double x2 = len_step * j;
                                double nx2 = sigmax_values.get(i).get(j);
                                //Получаем y координату
                                double y_coord2 = nx2 * pix_step;
                                //Зная все это, мы можем отрисовать линию по этим двум точкам
                                double firstX = draw_position_x + (len_i * x1);
                                double firstY;
                                if (y_coord1 >= 0) {
                                    firstY = 450.0 - y_coord1;
                                }
                                else {
                                    firstY = 450.0 + (-y_coord1);
                                }
                                double secondX = draw_position_x + (len_i * x2);
                                double secondY;
                                if (y_coord2 >= 0) {
                                    secondY = 450.0 - y_coord2;
                                }
                                else {
                                    secondY = 450.0 + (-y_coord2);
                                }

                                Line line = new Line(firstX, firstY, secondX, secondY);
                                line.setStroke(Color.BLUEVIOLET);
                                line.setStrokeWidth(0.5);
                                extra_lines.add(line);
                                
                                Line line2 = new Line(firstX, firstY, firstX, 450.0);
                                line2.setStroke(Color.BLUEVIOLET);
                                line2.setStrokeWidth(0.2);
                                extra_lines.add(line2);
                            }
                            
                            //Отрисовываем выноски
                            //Левая часть стержня
                            Line lineFirst = new Line(draw_position_x, 450.0, draw_position_x+10.0, 380.0);
                            lineFirst.setStroke(Color.BLACK);
                            lineFirst.setStrokeWidth(0.4);
                            extra_lines.add(lineFirst);
                            
                            Line lineSecond = new Line(draw_position_x+10.0, 380.0, draw_position_x+40.0, 380.0);
                            lineSecond.setStroke(Color.BLACK);
                            lineSecond.setStrokeWidth(0.4);
                            extra_lines.add(lineSecond);
                            
                            Label lbl = new Label(Math.abs(sigmax_values.get(i).get(0)) + "qL^2/(EA)");
                            lbl.setLayoutX(draw_position_x);
                            lbl.setLayoutY(365.0);
                            lbl.setFont(new Font("Consolas", 12));
                            labels.add(lbl);
                            
                            //Правая часть стержня
                            Line lineFirst2 = new Line(draw_position_x+len_i, 450.0, draw_position_x+len_i-10.0, 520.0);
                            lineFirst2.setStroke(Color.BLACK);
                            lineFirst2.setStrokeWidth(0.4);
                            extra_lines.add(lineFirst2);
                            
                            Line lineSecond2 = new Line(draw_position_x+len_i-10.0, 520.0, draw_position_x+len_i-40.0, 520.0);
                            lineSecond2.setStroke(Color.BLACK);
                            lineSecond2.setStrokeWidth(0.4);
                            extra_lines.add(lineSecond2);
                            
                            Label lbl2 = new Label(Math.abs(sigmax_values.get(i).get(100)) + "qL^2/(EA)");
                            lbl2.setLayoutX(draw_position_x+len_i-60.0);
                            lbl2.setLayoutY(522.0);
                            lbl2.setFont(new Font("Consolas", 12));
                            labels.add(lbl2);  
                        }
                        //=====================================================
                        
                        
                        
                        if(len_i<30.0)
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");

                        //Если длина стержня больше 35, то отрисовываем номер узла
                        if(len_i>30.0)
                        {
                            //Отрисовывем прямоугольники для узлов
                            Rectangle num_node = new Rectangle(draw_position_x+len_i-10.0, 340.0, 20.0, 20.0);
                            num_node.setFill(Color.LAVENDER);
                            num_node.setStroke(Color.BLACK);
                            nums_nodes.add(num_node);
                            //Отрисовываем номер узла
                            Label label_num_node = new Label(""+(i+2));
                            if(i+2>9)
                                label_num_node.setLayoutX(draw_position_x+len_i-8.0);
                            else
                                label_num_node.setLayoutX(draw_position_x+len_i-4.0);
                            label_num_node.setLayoutY(342.0);
                            label_num_node.setFont(new Font("Consolas", 15));
                            labels.add(label_num_node);
                        }
                        else
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        
                        
                        
                        
                        //Отрисовываем круги для номеров стержней
                        Circle num_rod = new Circle(draw_position_x + (len_i / 2), 75.0, 12.0);
                        num_rod.setFill(Color.LAVENDER);
                        num_rod.setStroke(Color.BLACK);
                        
                        //Отрисовываем линию от круга к стержню
                        Line line_to_rod = new Line(draw_position_x + (len_i / 2),
                                75.0, draw_position_x + (len_i / 2), draw_position_y);
                        line_to_rod.setStroke(Color.BLACK);
                        line_to_rod.setStrokeWidth(0.5);
                        line_to_rod.getStrokeDashArray().addAll(5.0,10.0);
                        line_to_rod.setOpacity(0.0);
                        extra_lines.add(line_to_rod);
                        
                        num_rod.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            num_rod.setFill(Color.AQUA);
                            line_to_rod.setOpacity(1.0);
                        });
                        
                        num_rod.setOnMouseExited(ActionEvent -> {
                            num_rod.setFill(Color.LAVENDER);
                            line_to_rod.setOpacity(0.0);
                        });
                        nums_rods.add(num_rod);
                        //Отрисовываем номер узла
                        Label label_num_rod = new Label("" + (construction.getTableView().get(i).getNum()));
                        if (construction.getTableView().get(i).getNum() > 9) {
                            label_num_rod.setLayoutX(draw_position_x + (len_i / 2) - 6.0);
                        }
                        else {
                            label_num_rod.setLayoutX(draw_position_x + (len_i / 2) - 3.0);
                        }
                        label_num_rod.setLayoutY(68.0);
                        label_num_rod.setFont(new Font("Consolas", 10));
                        label_num_rod.setOnMouseEntered((MouseEvent ActionEvent) -> {
                            num_rod.setFill(Color.AQUA);
                            line_to_rod.setOpacity(1.0);
                        });
                        
                        label_num_rod.setOnMouseExited(ActionEvent -> {
                            num_rod.setFill(Color.LAVENDER);
                            line_to_rod.setOpacity(0.0);
                        });
                        labels.add(label_num_rod);
                        
                        
                        //Если длина стержня больше 80, то отрисовываем дескритор стержня
                        if(len_i>60.0)
                        {
                            //Отрисовываем линии для каждого из стержней
                            Line descript_line1 = new Line(draw_position_x+(len_i/5),
                                    draw_position_y-(h_i/3), draw_position_x+(len_i/4),
                                    draw_position_y-(h_i/2)-10.0);
                            Line descript_line2 = new Line(draw_position_x+(len_i/4),
                                    draw_position_y-(h_i/2)-10.0, draw_position_x+(len_i/4)+len_i/3,
                                    draw_position_y-(h_i/2)-10.0);
                            descript_line1.setStroke(Color.BLACK);
                            descript_line1.setStrokeWidth(0.5);
                            descript_line2.setStroke(Color.BLACK);
                            descript_line2.setStrokeWidth(0.5);
                            Circle descript_circle = new Circle(draw_position_x+(len_i/5), draw_position_y-(h_i/3), 2.0);
                            descript_circle.setFill(Color.BLACK);
                            descript_lines.add(descript_line1);
                            descript_lines.add(descript_line2);
                            nums_rods.add(descript_circle);
                            //Теперь отрисовываем показатели каждого из стержней
                            Label descript_label = new Label("E, "+construction.getTableView().get(i).getA()+"A");
                            descript_label.setLayoutX(draw_position_x+(len_i/4)+3.0);
                            descript_label.setLayoutY(draw_position_y-(h_i/2)-25.0);
                            descript_label.setFont(new Font("Consolas", 12));
                            labels.add(descript_label);
                            
                        }
                        else
                            lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                        
                        draw_position_x += len_i;
                    }
                        //Отрисовываем первую линию
                        Line first_line = new Line(80.0, 200.0, 80.0, 550.0);
                        first_line.setStroke(Color.BLACK);
                        first_line.setStrokeWidth(0.5);
                        first_line.getStrokeDashArray().addAll(5.0,10.0);
                        extra_lines.add(first_line);
                        
                        //Отрисовываем прямоугольник первого узла
                        Rectangle num_node = new Rectangle(70.0, 340.0, 20.0, 20.0);
                        num_node.setFill(Color.LAVENDER);
                        num_node.setStroke(Color.BLACK);
                        nums_nodes.add(num_node);
                        //Отрисовываем номер первого узла
                        Label label_num_node = new Label(""+1);
                        label_num_node.setLayoutX(76.0);
                        label_num_node.setLayoutY(342.0);
                        label_num_node.setFont(new Font("Consolas", 15));
                        labels.add(label_num_node);


                    //Теперь отдельно для последнего узла, прорисовываем статическое усилие
                    for (int j = 0; j < num_static_forces; j++) {
                        //Получаем номер узла, в котором приложена сила
                        Integer node = construction.getDtsllView().get(j).get_num_rodNum();
                        //Получаем значение силы в данном узле
                        Double force = construction.getDtsllView().get(j).get_sllNum();

                        if (node == construction.getTableView().size() + 1) {
                                //Рисуем стрелку для правой части стержня
                            //Проверяем знак силы
                            if (force > 0) {
                                //Рисуем стрелку справа стержня, внутри, направленную вправо
                                Line line1 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - (global_len_i / 3), draw_position_y);
                                Line line2 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - 3 - 10, draw_position_y - 5);
                                Line line3 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - 3 - 10, draw_position_y + 5);
                                line1.setStrokeWidth(4.0);
                                line1.setStroke(Color.BROWN);
                                line2.setStrokeWidth(4.0);
                                line2.setStroke(Color.BROWN);
                                line3.setStrokeWidth(4.0);
                                line3.setStroke(Color.BROWN);
                                //Если стрелка помещается, то рисуем
                                    if(global_h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                            }
                            else {
                                //Рисуем стрелку справа стержня, внутри, направленную влево
                                Line line1 = new Line(800.0 - 3, draw_position_y,
                                                      800.0 - (global_len_i / 3), draw_position_y);
                                Line line2 = new Line(800.0 - (global_len_i / 3), draw_position_y,
                                                      800.0 - (global_len_i / 3) + 10, draw_position_y - 5);
                                Line line3 = new Line(800.0 - (global_len_i / 3), draw_position_y,
                                                      800.0 - (global_len_i / 3) + 10, draw_position_y + 5);
                                line1.setStrokeWidth(4.0);
                                line1.setStroke(Color.BROWN);
                                line2.setStrokeWidth(4.0);
                                line2.setStroke(Color.BROWN);
                                line3.setStrokeWidth(4.0);
                                line3.setStroke(Color.BROWN);
                                //Если стрелка помещается, то рисуем
                                    if(global_h_i>15.0)
                                    {
                                        static_forces.add(line1);
                                        static_forces.add(line2);
                                        static_forces.add(line3);
                                    }
                                    else
                                        lbl_log.setText("Конструкция успешно отрисована! Но некоторые объекты не отрисованны из-за проблем масштаба.");
                            }
                        }
                    }

                    //Отрисовываем заделки
                    num_props = construction.getDtlpView().size();
                    if (num_props == 0) {
                        lbl_log.setText("ВНИМАНИЕ! Конструкция не закреплена! Исправьте!");
                    }
                    else {
                        if (num_props == 1) {
                            if (construction.getDtlpView().get(0).get_nodeNum() == 1) {
                                //Отрисовываем левую заделку
                                Line line1 = new Line(80, 150.0, 80.0, 250.0);
                                line1.setStrokeWidth(3.0);
                                line1.setStroke(Color.BROWN);
                                //Теперь штрихи заделки
                                for (int i = 150; i <= 250; i += 10) {
                                    Line line_str = new Line(70, i + 5, 80.0, i);
                                    line_str.setStroke(Color.BROWN);
                                    left_prop.add(line_str);
                                }
                                left_prop.add(line1);
                            }
                            else {
                                //Отрисовываем правую заделку
                                Line line1 = new Line(800.0, 150.0, 800.0, 250.0);
                                line1.setStrokeWidth(3.0);
                                line1.setStroke(Color.BROWN);
                                //Теперь штрихи заделки
                                for (int i = 150; i <= 250; i += 10) {
                                    Line line_str = new Line(810.0, i - 5, 800.0, i);
                                    line_str.setStroke(Color.BROWN);
                                    right_prop.add(line_str);
                                }
                                right_prop.add(line1);
                            }
                        }
                        else {

                            //Отрисовываем левую заделку
                            Line line1 = new Line(80, 150.0, 80.0, 250.0);
                            line1.setStrokeWidth(3.0);
                            line1.setStroke(Color.BROWN);
                            left_prop.add(line1);
                            //Теперь штрихи заделки
                            for (int i = 150; i <= 250; i += 10) {
                                Line line_str = new Line(70, i + 5, 80.0, i);
                                line_str.setStroke(Color.BROWN);
                                left_prop.add(line_str);
                            }

                            //Отрисовываем правую заделку
                            Line line2 = new Line(800.0, 150.0, 800.0, 250.0);
                            line2.setStrokeWidth(3.0);
                            line2.setStroke(Color.BROWN);
                            //Теперь штрихи заделки
                            for (int i = 150; i <= 250; i += 10) {
                                Line line_str = new Line(810.0, i - 5, 800.0, i);
                                line_str.setStroke(Color.BROWN);
                                right_prop.add(line_str);
                            }
                            right_prop.add(line2);
                        }
                    }
                }
                else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
            }else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
        }else {
                    lbl_log.setText("Не создано ни одного стержня! Исправьте!");
                }
    }
}
